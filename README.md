# Rune
Developed by Jay Gradon and Akram Darwazeh

***

## Project Definition
#### Project Goal

For the purpose of following/finding good software practices, such as documentation/commenting, quality software architecures following SOLID principles and design patterns, proper git workflow, and collaboration.

First C# project.

#### Concept
Rune is a dark-fantasy game centred on the concept of "runes" a magical syllabic alphabet where the clearer the user's intent is,
the more powerful the rune becomes - as such, a written rune is stronger than spoken, which is stronger than signed, which is 
stronger than thought.  Guilds such as the Mages Guilds of Fairy Tail are commonplace and provide a centre for Runemeisters 
such as the player, to take contracts and receive compensations, as well as developing their skills.

#### Inspirations
Rune is inspired by the anime adaptation of Hiro Mashima's manga, 'Fairy Tail', as well as the atmospheres of CD Projekt Red's
'Witcher' series and FromSoftware's 'Dark Souls' series.  

#### Design Plan
Rune is to initially be a sandbox game, where a centric quest hub, a Rune Guild in the main township will provide direction and 
structure to the players development in place of a main quest line.  
The majority of the game will use a D&D-esque dice roll system, with as many events randomised as possible to prevent a static
world and predictable gameplay.

***

## Development Guidelines
A wiki page should be made for each class detailing its responsibilities and its dependencies via a UML diagram.  
_Wiki pages are required after each 0.B iteration_  

Git commits should be made with every small collection of classes, eg. Worn, Armor, and Clothing, or an Armor set.  
_Merges to master should only be made after each 0.B iteration, from all previous 0.0C iterations_  

XML (///) documentations should be applied to every class, method, and primitive type field.  
_XML should be completed with every 0.0C iteration_  

A // REVIEW tag should be added in the first commit of any significant class or where any significant changes to a method, field, or class responsibility.  This is to ensure developers have an inherent understanding of the entire system and prevent bugs.  The // TODO tag should also be used rigorously to keep developers up-to-date on the direction of the project, and prevent the introduction of bugs through forgetfulness.

Unit tests are to be developed at the developers discretion.

***

## Learning Material
Feel free to add more links  
+ <a href="http://www.tutorialspoint.com/design_pattern/design_pattern_overview.htm" >Design Patterns Tutorial</a>  
+ <a href="http://smartbear.com/SmartBear/media/pdfs/best-kept-secrets-of-peer-code-review.pdf" >SmartBear Code Review Ebook</a>  

## Current Bugs
CURRENTLY UNTESTED
