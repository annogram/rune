﻿using System;
using System.Collections.Generic;
using System.Linq;

using Rune.Items.Containers;
using Rune.Items.Equipment.Accessories.Gear.Pouches;

namespace Rune.Items.Equipment.Accessories.Gear.Belts
{
    public abstract class Belt : Gear, IBelt
    {
        #region Const Defines
        new protected static readonly string ID = Gear.ID + "1";
        private const GearPos  EQUIP_POSITION = GearPos.WAIST;
        private const CarryPos CONTAINER_TYPE = CarryPos.BELT;
        #endregion

        #region Properties
        /// <summary> Total number of pouches that can be attached to the belt </summary>
        public int MaxPouchSlots { get; protected set; }
		public int PouchesCount  { get; protected set; }
        #endregion

        #region Fields
        /// <summary> The currently equipped pouches </summary>
        private IList<Pouch> Pouches;
        #endregion

        #region Constructors
        protected Belt() : base() {
            this.MaxPouchSlots = SetMaxPouchSlots();
            this.PouchesCount = 0;
            			
            Pouches = new List<Pouch>(MaxPouchSlots);
			PouchesCount = Pouches.Count;
        }
        #endregion

        #region Methods
        protected abstract int SetMaxPouchSlots();

        protected override GearPos SetEquipPosition() {
            return EQUIP_POSITION;
        }

        protected override CarryPos SetContainerType() {
            return CONTAINER_TYPE;
        }

        protected override void InitializeCommon() {
            List<string> names = new string[] { "belt" }.ToList();
            foreach (string s in names) {
                base.Common.Add(s.GetHashCode(), s);
            }
        }

        public bool EquipPouch(Pouch pouch) {
            if(Pouches.Count < MaxPouchSlots && !(pouch is NullPouch)) {
                Pouches.Add(pouch);
				PouchesCount = Pouches.Count;
                return true;
            }
            return false;
        }
		
		public Pouch DequipPouch(int index) {
			if((index >= 0) && (index < Pouches.Count)) {
				Pouch takenPouch = Pouches[index];
				Pouches.RemoveAt(index);
				PouchesCount = Pouches.Count;
				return takenPouch;
			}
			return new NullPouch();
		}
		
		public Pouch GetPouch(int index) {
			if((index >= 0) && (index < Pouches.Count)) {
				return Pouches[index];
			}
			return new NullPouch();
		}
        #endregion
    }
}
