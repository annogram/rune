﻿using System;
using System.Collections.Generic;
using Rune.Items.Containers;
using Rune.Items.Equipment.Accessories.Gear.Pouches;

namespace Rune.Items.Equipment.Accessories.Gear.Belts
{
    /// <summary>
    /// Defines the public API for a belt, a special type of container which can have other
    /// containers (pouches) equipped to it.
    /// </summary>
    public interface IBelt : IGear
    {
        #region Properties
		/// <summary> Max number of Pouches the belt can have equipped </summary>
        int MaxPouchSlots { get; }
		int PouchesCount  { get; }
        #endregion

        #region Methods
        /// <summary>
        /// Equip a pouch to the belt.  Will not equip if no spare pouch slots.
        /// </summary>
        /// <param name="pouch"> The pouch object to equip </param>
        /// <returns> True if equipped, else false </returns>
        bool EquipPouch(Pouch pouch);
		
		/// <summary>
		/// Dequips the pouch at the specified index from the belts
		/// equipped pouches.  The pouch is returned and removed
		/// from the equipped pouches.
		/// </summary>
        /// <param name="index"> The index's index </param>
        /// <returns> The pouch at the given index </returns>
		Pouch DequipPouch(int index);

		/// <summary>
		/// Gets the pouch at the specified index from the belts
		/// equipped pouches.
		/// </summary>
        /// <param name="index"> The index's index </param>
        /// <returns> The index at the given index </returns>
        Pouch GetPouch(int index);
        #endregion
    }
}
