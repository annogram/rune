﻿using System;
using Rune.Util;

namespace Rune.Items.Equipment.Accessories.Gear.Belts
{
    /// <summary>
    /// A null belt
    /// </summary>
    public class NullBelt : Belt
    {
        #region Const Defines
        new public static readonly string ID = Belt.ID + "0";
		#endregion
	
        #region Methods
        public override string ToSimpleName() {
            return "[I'M A REAL BELT]";
        }
		
		// Null items have no names
		protected override void InitializeCommon() { }
		
        protected override void InitializeUnique() { }

        protected override CarryPos SetCarryPosition() {
            return CarryPos.CANNOT_CARRY;
        }

        protected override int SetMaxItemSlots() {
            return 0;
        }

        protected override int SetMaxPouchSlots() {
            return 0;
        }

        protected override int SetMaxWeight() {
            return 0;
        }

        protected override QualityModifier SetQuality() {
            return QualityModifier.BROKEN;
        }

        protected override int SetValue() {
            return 0;
        }

        protected override int SetWeight() {
            return 0;
        }
		
		public override string GetDescription() {
            return "The very real belt looks vaguely transparent, as if its very existence in your hands is a woopsie.";
        }
        #endregion
    }
}
