﻿using System;
using Rune.Util;
using System.Collections.Generic;
using System.Linq;

namespace Rune.Items.Equipment.Accessories.Gear.Belts
{
    /// <summary>
    /// Defines properties for a ring belt; for all social tiers and of middling size. 
    /// http://www.larp-fashion.co.uk/media/image/mittelalter-ringguertel-lang-4cm-pp-10rng-g4-600_1.jpg
    /// </summary>
    public class RingBelt : Belt
    {
        #region Const Defines
        new public static readonly string ID = Belt.ID + "A";
        // Item Properties
        private const int             VALUE            = 15;
		private const QualityModifier DEFAULT_QUALITY  = QualityModifier.LOW;
		// Carry Properties
        private const int             WEIGHT           = 2;
        private const CarryPos        CARRY_POSITION   = CarryPos.ONE_HAND;
        // Container Properties
        private const int             ITEM_SLOTS       = 4;
        private const int             MAX_CARRY_WEIGHT = 30;
        // Belt Properties
        private const int             POUCH_SLOTS      = 2;
        #endregion

        #region Fields
        // Empty
        #endregion

        #region Constructors
        public RingBelt() : base() { }
        #endregion

        #region Methods
        public override string ToSimpleName() {
            return "Ring Belt";
        }

        protected override void InitializeUnique() {
            List<string> names = new string[] { "ringbelt" }.ToList();
            foreach (string s in names) {
                base.Unique.Add(s.GetHashCode(), s);
            }
        }

        protected override int SetMaxPouchSlots() {
            return POUCH_SLOTS;
        }

        protected override int SetWeight() {
            return WEIGHT;
        }

        protected override CarryPos SetCarryPosition() {
            return CARRY_POSITION;
        }

        protected override int SetMaxWeight() {
            return MAX_CARRY_WEIGHT;
        }

        protected override int SetMaxItemSlots() {
            return ITEM_SLOTS;
        }

        protected override int SetValue() {
            return VALUE;
        }

        protected override QualityModifier SetQuality() {
            return DEFAULT_QUALITY;
        }
		//TODO
        public override string GetDescription() {
            switch(this.Quality) {
				case(QualityModifier.BROKEN):
					return "";
				case(QualityModifier.LOW):
					return "";
				case(QualityModifier.NORMAL):
					return "";
				case(QualityModifier.HIGH):
					return "";
				case(QualityModifier.MYTHICAL):
					return "";
				default:
					// Quality is set to unsupported value, set to normal and goto appropriate case
					this.Quality = QualityModifier.NORMAL;
					goto case(QualityModifier.NORMAL);
			}
        }
        #endregion
    }
}
