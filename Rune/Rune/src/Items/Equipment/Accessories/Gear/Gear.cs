﻿using System;
using Rune.Items.Containers;

namespace Rune.Items.Equipment.Accessories.Gear
{
    /// <summary>
    /// 
    /// </summary>
    public abstract class Gear : Container, IGear
    {
        #region Const Defines
        new protected static readonly string ID = Container.ID + "1";
		#endregion
		
		#region Properties
		public GearPos  EquipPosition { get; protected set; }
		/// <summary> The weight of the item (kgs or equivalent) </summary>
        public int      Weight   { get; protected set; }
        public CarryPos CarryPosition { get; protected set; }
		#endregion
		
		#region Constructors
		protected Gear() : base() {
            this.EquipPosition = SetEquipPosition();
            this.Weight = SetWeight();
            this.CarryPosition = SetCarryPosition();
        }
        #endregion

        #region Methods
        protected abstract GearPos SetEquipPosition();

        protected abstract int SetWeight();

        protected abstract CarryPos SetCarryPosition();
        #endregion
    }
}
