﻿using System;

namespace Rune.Items.Equipment.Accessories.Gear
{
    /// <summary>
    /// An enum defining where a piece of gear is equipped.
    /// </summary>
    public enum GearPos
    {
        INVALID_POS = -1,
        BACK    = 0, // For equipping backpacks
        WAIST   = 1, // For equipping belts
        BELT    = 2, // For equipping pouchs
    }

    /// <summary>
    /// A class to define extension methods for enums relating to gear.
    /// </summary>
    public static class GearEnum
    {
		public static GearPos GetGearPosition(this string position) {
			switch(position) {
				case("BACK"):
					return GearPos.BACK;
				case("WAIST"):
					return GearPos.WAIST;
				case("BELT"):
					return GearPos.BELT;
				default:
					return GearPos.INVALID_POS;
			}
		}
    }
}
