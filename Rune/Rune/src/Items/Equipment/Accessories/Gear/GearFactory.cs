﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

using Rune.Util;

namespace Rune.Items.Equipment.Accessories.Gear
{
    public class GearFactory
    {
        #region Properties
		// Empty
		#endregion
		
		#region Constructors
		// Empty
		#endregion
		
		#region Methods
        public static IGear GetGear(string id) {
            foreach (Type v in Assembly.GetExecutingAssembly().GetTypes()
                     .Where(w => w.IsSubclassOf(typeof(Gear)) && !(w.IsAbstract))) 
            {
                Gear gear = Activator.CreateInstance(v) as Gear;
                if (id.Equals(v.GetField("ID").GetValue(gear))) return gear;
            }
            return new NullGear();
        }
        #endregion
    }
}
