﻿using System;
using Rune.Items.Containers;

namespace Rune.Items.Equipment.Accessories.Gear
{
    /// <summary>
    /// Defines the public API of Gear objects (equippable containers)
    /// </summary>
    public interface IGear : IContainer, IEquipment
    {
		#region Properties
		GearPos EquipPosition { get; }
		#endregion
		
		#region Methods
		// Empty
		#endregion		
    }
}
