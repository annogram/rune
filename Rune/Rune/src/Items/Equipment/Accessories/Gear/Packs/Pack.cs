﻿using System;
using System.Collections.Generic;
using System.Linq;
using Rune.Items.Containers;

namespace Rune.Items.Equipment.Accessories.Gear.Packs
{
    /// <summary>
    /// Defines the generic pack variables for an equippable backpack.
    /// </summary>
    public abstract class Pack : Gear
    {
        #region Const Defines
        new protected static readonly string ID = Gear.ID + "2";
        private const GearPos  EQUIP_POSITION = GearPos.BACK;
        private const CarryPos CONTAINER_TYPE = CarryPos.PACK;
        #endregion

        #region Properties
		// Empty
        #endregion

        #region Constructors
        // Empty
        #endregion

        #region Methods
        protected override void InitializeCommon() {
            List<string> names = new string[] { "pack" }.ToList();
            foreach (string s in names) {
                base.Common.Add(s.GetHashCode(), s);
            }
        }

        protected override GearPos SetEquipPosition() {
            return EQUIP_POSITION;
        }

        protected override CarryPos SetContainerType() {
            return CONTAINER_TYPE;
        }
        #endregion
    }
}
