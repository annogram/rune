﻿using System;
using Rune.Util;
using System.Collections.Generic;
using System.Linq;

namespace Rune.Items.Equipment.Accessories.Gear.Packs
{
    /// <summary>
    /// Defines properties for a satchel for peasants of small size.
    /// https://s-media-cache-ak0.pinimg.com/736x/5e/33/fd/5e33fde99965660d9b34258cd6c5f1c3.jpg
    /// </summary>
    public class Satchel : Pack
    {
        #region Const Defines
        new public static readonly string ID = Pack.ID + "A";
        // Item Properties
        private const int             VALUE            = 15;
		private const QualityModifier DEFAULT_QUALITY  = QualityModifier.LOW;
		// Carry Properties
        private const int             WEIGHT           = 2;
        private const CarryPos        CARRY_POSITION   = CarryPos.ONE_HAND;
        // Container Properties
        private const int             ITEM_SLOTS       = 4;
        private const int             MAX_CARRY_WEIGHT = 30;
        #endregion

        #region Fields
        // Empty
        #endregion

        #region Constructors
        public Satchel() : base() { }
        #endregion

        #region Methods
        public override string ToSimpleName() {
            return "Satchel";
        }

        protected override void InitializeUnique() {
            List<string> names = new string[] { "satchel" }.ToList();
            foreach (string s in names) {
                base.Unique.Add(s.GetHashCode(), s);
            }
        }

        protected override int SetWeight() {
            return WEIGHT;
        }

        protected override CarryPos SetCarryPosition() {
            return CARRY_POSITION;
        }

        protected override int SetMaxWeight() {
            return MAX_CARRY_WEIGHT;
        }

        protected override int SetMaxItemSlots() {
            return ITEM_SLOTS;
        }

        protected override int SetValue() {
            return VALUE;
        }

        protected override QualityModifier SetQuality() {
            return DEFAULT_QUALITY;
        }

        //TODO
        public override string GetDescription() {
            switch(this.Quality) {
				case(QualityModifier.BROKEN):
					return "";
				case(QualityModifier.LOW):
					return "";
				case(QualityModifier.NORMAL):
					return "";
				case(QualityModifier.HIGH):
					return "";
				case(QualityModifier.MYTHICAL):
					return "";
				default:
					// Quality is set to unsupported value, set to normal and goto appropriate case
					this.Quality = QualityModifier.NORMAL;
					goto case(QualityModifier.NORMAL);
			}
        }
        #endregion
    }
}
