﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Rune.Items.Equipment.Accessories.Gear.Pouches
{
    /// <summary>
    /// Defines properties for a drawstring pouch; for all social tiers (quality relative) and of small size. 
    /// https://s-media-cache-ak0.pinimg.com/236x/9b/96/0f/9b960f4e6f9b0e22a990df3f4b6d77f1.jpg
    /// </summary>
    public class DrawstringPouch : Pouch
    {
        #region Const Defines
        new public static readonly string ID = Pouch.ID + "A";
        // Item Properties
        private const int             VALUE            = 5;
		private const QualityModifier DEFAULT_QUALITY  = QualityModifier.LOW;
		// Carry Properties
        private const int             WEIGHT           = 1;
        private const CarryPos        CARRY_POSITION   = CarryPos.PACK;
        // Container Properties
        private const int             ITEM_SLOTS       = 1;
        private const int             MAX_CARRY_WEIGHT = 10;
        #endregion

        #region Fields
        // Empty
        #endregion

        #region Constructors
        public DrawstringPouch() : base() { }
        #endregion

        #region Methods
        public override string ToSimpleName() {
            return "Drawstring Pouch";
        }

        protected override void InitializeUnique() {
            List<string> names = new string[] { "drawstringpouch" }.ToList();
            foreach (string s in names) {
                base.Unique.Add(s.GetHashCode(), s);
            }
        }

        protected override int SetWeight() {
            return WEIGHT;
        }

        protected override CarryPos SetCarryPosition() {
            return CARRY_POSITION;
        }

        protected override int SetMaxWeight() {
            return MAX_CARRY_WEIGHT;
        }

        protected override int SetMaxItemSlots() {
            return ITEM_SLOTS;
        }

        protected override int SetValue() {
            return VALUE;
        }

        protected override QualityModifier SetQuality() {
            return DEFAULT_QUALITY;
        }

        //TODO
        public override string GetDescription() {
            switch(this.Quality) {
				case(QualityModifier.BROKEN):
					return "";
				case(QualityModifier.LOW):
					return "";
				case(QualityModifier.NORMAL):
					return "";
				case(QualityModifier.HIGH):
					return "";
				case(QualityModifier.MYTHICAL):
					return "";
				default:
					// Quality is set to unsupported value, set to normal and goto appropriate case
					this.Quality = QualityModifier.NORMAL;
					goto case(QualityModifier.NORMAL);
			}
        }
        #endregion
    }
}
