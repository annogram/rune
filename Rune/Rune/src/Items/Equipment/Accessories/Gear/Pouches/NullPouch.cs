﻿using System;

namespace Rune.Items.Equipment.Accessories.Gear.Pouches
{
    public class NullPouch : Pouch
    {
        #region Const Defines
        new public static readonly string ID = Pouch.ID + "0";
        #endregion

        #region Constructors
        public NullPouch() : base() { }
        #endregion

        #region Methods
        public override string ToSimpleName() {
            return "[I'M A REAL POUCH]";
        }
		
        protected override void InitializeUnique() { }

        protected override void InitializeCommon() { }

        protected override int SetWeight() {
            return 0;
        }

        protected override CarryPos SetCarryPosition() {
            return CarryPos.CANNOT_CARRY;
        }

        protected override int SetMaxWeight() {
            return 0;
        }

        protected override int SetMaxItemSlots() {
            return 0;
        }

        protected override int SetValue() {
            return 0;
        }

        protected override QualityModifier SetQuality() {
            return QualityModifier.BROKEN;
        }

        public override string GetDescription() {
            return "The very real pouch looks vaguely transparent, as if its very existence in your hands is a woopsie.";
        }
        #endregion
    }
}
