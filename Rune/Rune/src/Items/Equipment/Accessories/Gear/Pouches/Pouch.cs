﻿using System;
using System.Collections.Generic;
using System.Linq;
using Rune.Items.Containers;

namespace Rune.Items.Equipment.Accessories.Gear.Pouches
{
    public abstract class Pouch : Gear
    {
        #region Const Defines
        new public static readonly string ID = Gear.ID + "3";
        private const GearPos  EQUIP_POSITION = GearPos.BELT;
        private const CarryPos CONTAINER_TYPE = CarryPos.PACK;
        #endregion

        #region Properties
        // Empty
        #endregion

        #region Constructors
        // Empty
        #endregion

        #region Methods
        protected override void InitializeCommon() {
            List<string> names = new string[] { "pouch" }.ToList();
            foreach (string s in names) {
                base.Common.Add(s.GetHashCode(), s);
            }
        }

        protected override GearPos SetEquipPosition() {
            return EQUIP_POSITION;
        }

        protected override CarryPos SetContainerType() {
            return CONTAINER_TYPE;
        }
        #endregion
    }
}
