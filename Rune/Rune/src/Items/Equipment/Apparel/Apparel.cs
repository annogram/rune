﻿using System;

namespace Rune.Items.Equipment.Apparel
{
    /// <summary>
    /// 
    /// </summary>
    public abstract class Apparel : Equipment, IApparel
    {
        #region Const Defines
        new protected static readonly string ID = Equipment.ID + "2";
		private const int DAMAGE_SUB_PERCENT = 10;
		#endregion
		
        #region Properties
        public int Protection { get; protected set; }
        #endregion 

        #region Constructors
        protected Apparel() : base() {
            this.Protection = SetProtection();
        }
        #endregion 

        #region Methods
        protected abstract int SetProtection();
        #endregion 
    }
}
