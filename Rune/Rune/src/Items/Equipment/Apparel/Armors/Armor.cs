﻿using System;

using Rune.Util;

namespace Rune.Items.Equipment.Apparel.Armors
{
    public abstract class Armor : Apparel, IArmor
    {
        #region Const Defines
        new protected static readonly string ID = Apparel.ID + "1";
		#endregion
		
        #region Properties
        public ArmorPos EquipPosition { get; protected set; }
        public ArmorMaterial Material { get; protected set; }
        #endregion 

        #region Constructors
        protected Armor() : base() {
            this.EquipPosition = SetEquipPosition();
            this.Material = SetMaterial();
        }
        #endregion 

        #region Methods
        protected abstract ArmorPos SetEquipPosition();

        protected abstract ArmorMaterial SetMaterial();

        public override bool Equals(object obj) {
            if(obj.GetType() == this.GetType()) {
                IArmor armor = (IArmor)obj;
                if (armor.Quality == this.Quality && armor.Material == this.Material) {
                    return true;
                }
            }
            return false;
        }

        public override int GetHashCode() {
            return base.GetHashCode();
        }
		
		public virtual ArmorMaterial RollMaterial() {
			this.Material = RollMaterial(ArmorMaterial.MITHRIL, ArmorMaterial.PADDED);
			return Material;
		} 
		
		public virtual ArmorMaterial RollMaterial(ArmorMaterial lower, ArmorMaterial upper) {
			int roll = RuneChance.RollDiceExponential((int)(upper - lower)) + (int)lower; ;
			this.Material = (ArmorMaterial)roll;
			return Material;
		}
        #endregion 
    }
}
