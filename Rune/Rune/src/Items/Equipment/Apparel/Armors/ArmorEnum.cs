﻿using System;

namespace Rune.Items.Equipment.Apparel.Armors
{
    /// <summary>
    /// Defines where different types of armor may be equipped.
    /// </summary>
    public enum ArmorPos
    {
        INVALID_POS = -1,
        HEAD     = 0,
        TORSO    = 1,
        SHOULDER = 2,
        ARM      = 3,
        LEG      = 4,
		NUM_ARMOR_POS
    }

    /// <summary>
    /// Defines materials different kinds of armor can be made from.
    /// Acts as a modifier on defense with quality, alters description.
    /// </summary>
    public enum ArmorMaterial
    {
        PADDED,
        LEATHER,
		BRONZE,
        IRON,
        STEEL,
		MITHRIL,
		NUM_MATERIALS
    }

    /// <summary>
    /// A static class to provide extension methods for enums relating to armor.
    /// </summary>
    public static class ArmorEnum
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name=""></param>
        /// <param name="position"></param>
        /// <returns></returns>
        public static ArmorPos GetArmorPosition(this string position) {
            switch (position) {
                case ("HEAD"):
                    return ArmorPos.HEAD;
                case ("TORSO"):
                    return ArmorPos.TORSO;
                case ("SHOULDER"):
                    return ArmorPos.SHOULDER;
                case ("ARM"):
                    return ArmorPos.ARM;
                case ("LEG"):
                    return ArmorPos.LEG;
                default:
                    return ArmorPos.INVALID_POS;

            }
        }
    }
}
