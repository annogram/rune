﻿using Rune.Items.Equipment;
using Rune.Items.Equipment.Apparel.Armors;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Rune.Items.Equipment.Apparel.Armors 
{
    /// <summary>
    /// Used for creating concrete armor objects
    /// </summary>
    public abstract class ArmorFactory
	{
        public static IArmor GetArmor(string id) {
            foreach (Type v in Assembly.GetExecutingAssembly().GetTypes()
                     .Where(w => w.IsSubclassOf(typeof(Armor)) && !(w.IsAbstract)))
            {
                Armor armor = Activator.CreateInstance(v) as Armor;
                if (id.Equals(v.GetField("ID").GetValue(armor))) return armor;
            }
            return new NullArmor();
        }
	}
}
