﻿using Rune.Items.Equipment.Apparel.Armors;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rune.Items.Equipment.Apparel.Armors 
{
    /// <summary>
    /// Manager that handles currently equipped (being worn) armor.
	/// Handles equipping, dequipping, listing, and getting relevant 
	/// singular and aggregate values from equipped armor.
    /// </summary>
    public class ArmorManager : IArmorManager 
	{

        #region Properties
		// Empty
        #endregion

        #region Fields
		private NullArmor NullArmorInstance = new NullArmor();
        private IArmor[] equippedArmor;
        #endregion

        #region Constructors
        public ArmorManager() {
            equippedArmor = new IArmor[(int)ArmorPos.NUM_ARMOR_POS];
            // Change null reference to NullArmor objects
            for (int i = 0; i < (int)ArmorPos.NUM_ARMOR_POS; i++) {
				equippedArmor[i] = NullArmorInstance;
		    }
        }
        #endregion

        #region Methods
		public bool Equip(IArmor armor) {
			if((int)armor.EquipPosition >= 0 && (int)armor.EquipPosition < equippedArmor.Length && equippedArmor[(int)armor.EquipPosition] is NullArmor) {
				equippedArmor[(int)armor.EquipPosition] = armor;
				return true;
			}
			return false;
        }
		
        public IArmor Dequip(string position) {
            return Dequip(position.GetArmorPosition());
        }
		
		public IArmor Dequip(ArmorPos position) {
			if((int)position >= 0 && (int)position < equippedArmor.Length) {
				IArmor armor = equippedArmor[(int)position];
				equippedArmor[(int)position] = NullArmorInstance;
				return armor;
			}
			return NullArmorInstance;
        }

        public IArmor Dequip(IArmor armor) {
            int index = (int)armor.EquipPosition;
			// Only dequip if identical, otherwise can use Dequip(ArmorPos)
			// Check will break if index is invalid before predicate use of index
			if(index >= 0 && index < equippedArmor.Length && equippedArmor[index].Equals(armor)) {
				return Dequip(armor.EquipPosition);
			}
            return NullArmorInstance;
        }

		public IArmor Peek(string position) {
            ArmorPos enumPos = ArmorEnum.GetArmorPosition(position);
            return Peek(enumPos);
        }

        public IArmor Peek(ArmorPos position) {
            if ((int)position >= 0 && (int)position < equippedArmor.Length) {
                    return equippedArmor[(int)position];
            }
            return NullArmorInstance;
        }
		
		public bool HasEquip(IArmor armor) {
			int index = (int)armor.EquipPosition;
			if(index >= 0 && index < equippedArmor.Length) {
				return equippedArmor[(int)index].Equals(armor);
			}
			return false;
		}

        public bool IsPositionFull(string position) {
            return IsPositionFull(position.GetArmorPosition());
        }

		public bool IsPositionFull(ArmorPos position) {
			if((int)position >= 0 && (int)position < equippedArmor.Length && !(equippedArmor[(int)position] is NullArmor)) {
				return true;
			}
            return false;
		}
		
		public IEnumerable<IArmor> Find(string name) {
            foreach (IArmor armor in equippedArmor) {
                if (armor.HasName(name)) {
					yield return armor;
				}
            }
        }

        public IEnumerable<IArmor> ListEquips() {
            foreach (IArmor armor in equippedArmor) {
                if(!(armor is NullArmor)) {
					yield return armor;
				}
			}
        }
		
		public int GetTotalProtection() {
            return equippedArmor.Select(s => s.Protection).Aggregate((a, b) => a + b);
        }

        public int GetTotalWeight() {
            return equippedArmor.Select(s => s.Weight).Aggregate((a, b) => a + b);
        }
        #endregion
    }
}
