﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Rune.Items.Equipment.Apparel.Armors.Cuirasses
{
    /// <summary>
    /// Defines generic properties for Cuirasses (TORSO armor).
    /// </summary>
    public abstract class Cuirass : Armor
    {
        #region Const Defines
        new protected static readonly string ID = Armor.ID + "1";
        protected const ArmorPos EQUIP_POSITION = ArmorPos.TORSO;
        #endregion 

        #region Fields
        // Empty
        #endregion 

        #region Constructors
        // Empty
        #endregion 

        #region Methods
        protected override void InitializeCommon() {
            List<string> names = new string[] { "cuirass" }.ToList();
            foreach (string s in names) {
                base.Common.Add(s.GetHashCode(), s);
            }
        }

        protected override ArmorPos SetEquipPosition() {
            return EQUIP_POSITION;
        }
        #endregion 
    }
}

