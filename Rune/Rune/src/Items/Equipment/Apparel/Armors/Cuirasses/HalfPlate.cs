﻿using System;
using System.Collections.Generic;
using System.Linq;

using Rune.Util;

namespace Rune.Items.Equipment.Apparel.Armors.Cuirasses
{
    /// <summary>
    /// A simple hybrid armor of chainmail and small plate chestpiece.  Of middling defense for irregular troops, metal materials only.
    /// https://www.google.co.nz/imgres?imgurl=https://s-media-cache-ak0.pinimg.com/736x/13/cd/01/13cd01453f16ed26bcd0601037e2ab58.jpg&imgrefurl=https://www.pinterest.com/pin/380343131008504415/&h=904&w=600&tbnid=jE71ebbr9ow4UM:&docid=vmgGE5bx6fTGfM&ei=8HqsVuWSJsj8jwPyxI6oBQ&tbm=isch&ved=0ahUKEwjlyqi3ldHKAhVI_mMKHXKiA1U4ZBAzCBUoEjAS
    /// </summary>
    public class HalfPlate : Cuirass
    {
        #region Const Defines
        new public static readonly string ID = Cuirass.ID + "A";
        // Item Properties
        private const int VALUE = 60;
        private const QualityModifier DEFAULT_QUALITY = QualityModifier.NORMAL;
        // Equip Properties
        private const int WEIGHT = 10;
        private const CarryPos CARRY_POSITION = CarryPos.TWO_HAND;
        // Apparel Properties
        private const int PROTECTION = 15;
        // Armor Properties
        private const ArmorMaterial DEFAULT_MATERIAL = ArmorMaterial.STEEL;
        #endregion 

        #region Fields
        // Empty
        #endregion 

        #region Constructors
        // Empty
        #endregion 

        #region Methods
        public override string ToSimpleName() {
            return "Halfplate";
        }

        protected override void InitializeUnique() {
            List<string> names = new string[] { "halfplate", "halfplatearmor" }.ToList();
            foreach (string s in names) {
                base.Unique.Add(s.GetHashCode(), s);
            }
        }

        protected override void InitializeCommon() {
            base.InitializeCommon();
            List<string> names = new string[] { "plate", "platearmor" }.ToList();
            foreach (string s in names) {
                base.Common.Add(s.GetHashCode(), s);
            }
        }

        protected override ArmorMaterial SetMaterial() {
            return DEFAULT_MATERIAL;
        }

        protected override int SetProtection() {
            return PROTECTION;
        }

        protected override int SetWeight() {
            return WEIGHT;
        }

        protected override CarryPos SetCarryPosition() {
            return CARRY_POSITION;
        }

        protected override int SetValue() {
            return VALUE;
        }

        protected override QualityModifier SetQuality() {
            return DEFAULT_QUALITY;
        }

        public override ArmorMaterial RollMaterial() {
            this.Material = RollMaterial(ArmorMaterial.BRONZE, ArmorMaterial.MITHRIL);
            return Material;
        }

        public override ArmorMaterial RollMaterial(ArmorMaterial setLower, ArmorMaterial upper) {
            ArmorMaterial lower = setLower < ArmorMaterial.BRONZE ? ArmorMaterial.BRONZE : setLower;
            int roll = RuneChance.RollDiceExponential((int)(upper - lower), (double)((int)(upper - lower) / 100)) + (int)lower; ;
            this.Material = (ArmorMaterial)roll;
            return Material;
        }

        //TODO
        public override string GetDescription() {
            switch (this.Material) {
                case (ArmorMaterial.BRONZE):
                    switch (this.Quality) {
                        case (QualityModifier.BROKEN):
                            return "";
                        case (QualityModifier.LOW):
                            return "";
                        case (QualityModifier.NORMAL):
                            return "";
                        case (QualityModifier.HIGH):
                            return "";
                        case (QualityModifier.MYTHICAL):
                            return "";
                        default:
                            // Quality is set to unsupported value, set to normal and goto appropriate case
                            this.Quality = QualityModifier.NORMAL;
                            goto case (QualityModifier.NORMAL);
                    }
                case (ArmorMaterial.IRON):
                    switch (this.Quality) {
                        case (QualityModifier.BROKEN):
                            return "";
                        case (QualityModifier.LOW):
                            return "";
                        case (QualityModifier.NORMAL):
                            return "";
                        case (QualityModifier.HIGH):
                            return "";
                        case (QualityModifier.MYTHICAL):
                            return "";
                        default:
                            // Quality is set to unsupported value, set to normal and goto appropriate case
                            this.Quality = QualityModifier.NORMAL;
                            goto case (QualityModifier.NORMAL);
                    }
                case (ArmorMaterial.STEEL):
                    switch (this.Quality) {
                        case (QualityModifier.BROKEN):
                            return "";
                        case (QualityModifier.LOW):
                            return "";
                        case (QualityModifier.NORMAL):
                            return "";
                        case (QualityModifier.HIGH):
                            return "";
                        case (QualityModifier.MYTHICAL):
                            return "";
                        default:
                            // Quality is set to unsupported value, set to normal and goto appropriate case
                            this.Quality = QualityModifier.NORMAL;
                            goto case (QualityModifier.NORMAL);
                    }
                case (ArmorMaterial.MITHRIL):
                    switch (this.Quality) {
                        case (QualityModifier.BROKEN):
                            return "";
                        case (QualityModifier.LOW):
                            return "";
                        case (QualityModifier.NORMAL):
                            return "";
                        case (QualityModifier.HIGH):
                            return "";
                        case (QualityModifier.MYTHICAL):
                            return "";
                        default:
                            // Quality is set to unsupported value, set to normal and goto appropriate case
                            this.Quality = QualityModifier.NORMAL;
                            goto case (QualityModifier.NORMAL);
                    }
                default:
                    this.Material = ArmorMaterial.STEEL;
                    goto case (ArmorMaterial.STEEL);
            }
        }
        #endregion
    }
}