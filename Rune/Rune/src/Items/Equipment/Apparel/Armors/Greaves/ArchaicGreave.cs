﻿using System;
using System.Collections.Generic;
using System.Linq;

using Rune.Util;

namespace Rune.Items.Equipment.Apparel.Armors.Greaves
{
    /// <summary>
    /// A piece of simple rounded leg armor of limited protection.  Comes in all materials.
    /// https://s-media-cache-ak0.pinimg.com/236x/f2/fa/e1/f2fae1d3bd74aa1c4cc5ab2c92f64753.jpg
    /// </summary>
    class ArchaicGreave : Greave
    {
        #region Const Defines
        new public static readonly string ID = Greave.ID + "A";
        // Item Properties
        private const int             VALUE            = 25;
        private const QualityModifier DEFAULT_QUALITY  = QualityModifier.NORMAL;
        // Equip Properties
        private const int             WEIGHT           = 6;
        private const CarryPos        CARRY_POSITION   = CarryPos.ONE_HAND;
        // Apparel Properties
        private const int             PROTECTION       = 6;
        // Armor Properties
        private const ArmorMaterial   DEFAULT_MATERIAL = ArmorMaterial.STEEL;
        #endregion  

        #region Fields
        // Empty
        #endregion 

        #region Constructors
        // Empty
        #endregion 

        #region Methods
        public override string ToSimpleName() {
            return "Archaic Greaves";
        }

        protected override void InitializeUnique() {
            List<string> names = new string[] { "archaicgreave", "archaicgreaves" }.ToList();
            foreach (string s in names) {
                base.Unique.Add(s.GetHashCode(), s);
            }
        }

        protected override ArmorMaterial SetMaterial() {
            return DEFAULT_MATERIAL;
        }

        protected override int SetProtection() {
            return PROTECTION;
        }

        protected override int SetWeight() {
            return WEIGHT;
        }

        protected override CarryPos SetCarryPosition() {
            return CARRY_POSITION;
        }

        protected override int SetValue() {
            return WEIGHT;
        }

        protected override QualityModifier SetQuality() {
            return DEFAULT_QUALITY;
        }

        public override ArmorMaterial RollMaterial() {
            this.Material = RollMaterial(ArmorMaterial.LEATHER, ArmorMaterial.MITHRIL);
            return Material;
        }

        public override ArmorMaterial RollMaterial(ArmorMaterial setLower, ArmorMaterial upper) {
            ArmorMaterial lower = setLower < ArmorMaterial.LEATHER ? ArmorMaterial.BRONZE : setLower;
            int roll = RuneChance.RollDiceExponential((int)(upper - lower), (double)((int)(upper - lower) / 100)) + (int)lower; ;
            this.Material = (ArmorMaterial)roll;
            return Material;
        }

        //TODO
        public override string GetDescription() {
            switch (this.Material) {
                case (ArmorMaterial.LEATHER):
                    switch (this.Quality) {
                        case (QualityModifier.BROKEN):
                            return "";
                        case (QualityModifier.LOW):
                            return "";
                        case (QualityModifier.NORMAL):
                            return "";
                        case (QualityModifier.HIGH):
                            return "";
                        case (QualityModifier.MYTHICAL):
                            return "";
                        default:
                            // Quality is set to unsupported value, set to normal and goto appropriate case
                            this.Quality = QualityModifier.NORMAL;
                            goto case (QualityModifier.NORMAL);
                    }
                case (ArmorMaterial.BRONZE):
                    switch (this.Quality) {
                        case (QualityModifier.BROKEN):
                            return "";
                        case (QualityModifier.LOW):
                            return "";
                        case (QualityModifier.NORMAL):
                            return "";
                        case (QualityModifier.HIGH):
                            return "";
                        case (QualityModifier.MYTHICAL):
                            return "";
                        default:
                            // Quality is set to unsupported value, set to normal and goto appropriate case
                            this.Quality = QualityModifier.NORMAL;
                            goto case (QualityModifier.NORMAL);
                    }
                case (ArmorMaterial.IRON):
                    switch (this.Quality) {
                        case (QualityModifier.BROKEN):
                            return "";
                        case (QualityModifier.LOW):
                            return "";
                        case (QualityModifier.NORMAL):
                            return "";
                        case (QualityModifier.HIGH):
                            return "";
                        case (QualityModifier.MYTHICAL):
                            return "";
                        default:
                            // Quality is set to unsupported value, set to normal and goto appropriate case
                            this.Quality = QualityModifier.NORMAL;
                            goto case (QualityModifier.NORMAL);
                    }
                case (ArmorMaterial.STEEL):
                    switch (this.Quality) {
                        case (QualityModifier.BROKEN):
                            return "";
                        case (QualityModifier.LOW):
                            return "";
                        case (QualityModifier.NORMAL):
                            return "";
                        case (QualityModifier.HIGH):
                            return "";
                        case (QualityModifier.MYTHICAL):
                            return "";
                        default:
                            // Quality is set to unsupported value, set to normal and goto appropriate case
                            this.Quality = QualityModifier.NORMAL;
                            goto case (QualityModifier.NORMAL);
                    }
                case (ArmorMaterial.MITHRIL):
                    switch (this.Quality) {
                        case (QualityModifier.BROKEN):
                            return "";
                        case (QualityModifier.LOW):
                            return "";
                        case (QualityModifier.NORMAL):
                            return "";
                        case (QualityModifier.HIGH):
                            return "";
                        case (QualityModifier.MYTHICAL):
                            return "";
                        default:
                            // Quality is set to unsupported value, set to normal and goto appropriate case
                            this.Quality = QualityModifier.NORMAL;
                            goto case (QualityModifier.NORMAL);
                    }
                default:
                    this.Material = ArmorMaterial.STEEL;
                    goto case (ArmorMaterial.STEEL);
            }
        }
        #endregion
    }
}
