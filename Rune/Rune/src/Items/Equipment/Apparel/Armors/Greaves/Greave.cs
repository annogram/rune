﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Rune.Items.Equipment.Apparel.Armors.Greaves
{
    /// <summary>
    /// Defines generic properties for Greaves (LEG armor).
    /// </summary>
    public abstract class Greave : Armor
    {
        #region Const Defines
        new protected static readonly string ID = Armor.ID + "2";
        protected const ArmorPos EQUIP_POSITION = ArmorPos.LEG;
        #endregion  

        #region Fields
        // Empty
        #endregion 

        #region Constructors
        // Empty
        #endregion 

        #region Methods
        protected override void InitializeCommon() {
            List<string> names = new string[] { "greave", "greaves" }.ToList();
            foreach (string s in names) {
                base.Common.Add(s.GetHashCode(), s);
            }
        }

        protected override ArmorPos SetEquipPosition() {
            return EQUIP_POSITION;
        }
        #endregion 
    }
}
