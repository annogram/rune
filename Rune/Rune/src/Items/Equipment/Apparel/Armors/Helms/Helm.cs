﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Rune.Items.Equipment.Apparel.Armors.Helms
{
    /// <summary>
    /// Defines generic properties for helms (HEAD armor).
    /// </summary>
    public abstract class Helm : Armor
    {
        #region Const Defines
        new protected static readonly string ID = Armor.ID + "3";
        protected const ArmorPos EQUIP_POSITION = ArmorPos.HEAD;
        #endregion  

        #region Fields
        // Empty
        #endregion 

        #region Constructors
        // Empty
        #endregion 

        #region Methods
        protected override void InitializeCommon() {
            List<string> names = new string[] { "helm", "helmet" }.ToList();
            foreach (string s in names) {
                base.Common.Add(s.GetHashCode(), s);
            }
        }

        protected override ArmorPos SetEquipPosition() {
            return EQUIP_POSITION;
        }
        #endregion 
    }
}
