﻿using System;
using System.Collections.Generic;
using System.Linq;

using Rune.Util;

namespace Rune.Items.Equipment.Apparel.Armors.Helms
{
    /// <summary>
    /// A sallet helm for all social tiers above peasantry.  Of most materials.
    /// http://2.bp.blogspot.com/-kGC9w2FfM64/T7O1v1FQU_I/AAAAAAAAACk/xCV2gLrCmFI/s1600/Sallet1470-85German.jpg
    /// https://s-media-cache-ak0.pinimg.com/236x/1c/1d/b7/1c1db7dabcd7a8c80264cdf3c53836c2.jpg
    /// </summary>
    public class SalletHelm : Helm
    {
        #region Const Defines
        new public static readonly string ID = Helm.ID + "A";
        // Item Properties
        private const int             VALUE            = 25;
        private const QualityModifier DEFAULT_QUALITY  = QualityModifier.NORMAL;
        // Equip Properties
        private const int             WEIGHT           = 6;
        private const CarryPos        CARRY_POSITION   = CarryPos.ONE_HAND;
        // Apparel Properties
        private const int             PROTECTION       = 8;
        // Armor Properties
        private const ArmorMaterial   DEFAULT_MATERIAL = ArmorMaterial.STEEL;
        #endregion  

        #region Fields
        // Empty
        #endregion 

        #region Constructors
        // Empty
        #endregion

        #region Methods
        public override string ToSimpleName() {
            return "Sallet Helm";
        }

        protected override void InitializeUnique() {
            List<string> names = new string[] { "sallet", "sallethelm" }.ToList();
            foreach (string s in names) {
                base.Unique.Add(s.GetHashCode(), s);
            }
        }

        protected override ArmorMaterial SetMaterial() {
            return DEFAULT_MATERIAL;
        }

        protected override int SetProtection() {
            return PROTECTION;
        }

        protected override int SetWeight() {
            return WEIGHT;
        }

        protected override CarryPos SetCarryPosition() {
            return CARRY_POSITION;
        }

        protected override int SetValue() {
            return VALUE;
        }

        protected override QualityModifier SetQuality() {
            return DEFAULT_QUALITY;
        }

        public override ArmorMaterial RollMaterial() {
            this.Material = RollMaterial(ArmorMaterial.BRONZE, ArmorMaterial.MITHRIL);
            return Material;
        }

        public override ArmorMaterial RollMaterial(ArmorMaterial setLower, ArmorMaterial upper) {
            ArmorMaterial lower = setLower < ArmorMaterial.BRONZE ? ArmorMaterial.BRONZE : setLower;
            int roll = RuneChance.RollDiceExponential((int)(upper - lower), (double)((int)(upper - lower) / 100)) + (int)lower; ;
            this.Material = (ArmorMaterial)roll;
            return Material;
        }

        //TODO
        public override string GetDescription() {
            switch (this.Material) {
                case (ArmorMaterial.BRONZE):
                    switch (this.Quality) {
                        case (QualityModifier.BROKEN):
                            return "";
                        case (QualityModifier.LOW):
                            return "";
                        case (QualityModifier.NORMAL):
                            return "";
                        case (QualityModifier.HIGH):
                            return "";
                        case (QualityModifier.MYTHICAL):
                            return "";
                        default:
                            // Quality is set to unsupported value, set to normal and goto appropriate case
                            this.Quality = QualityModifier.NORMAL;
                            goto case (QualityModifier.NORMAL);
                    }
                case (ArmorMaterial.IRON):
                    switch (this.Quality) {
                        case (QualityModifier.BROKEN):
                            return "";
                        case (QualityModifier.LOW):
                            return "";
                        case (QualityModifier.NORMAL):
                            return "";
                        case (QualityModifier.HIGH):
                            return "";
                        case (QualityModifier.MYTHICAL):
                            return "";
                        default:
                            // Quality is set to unsupported value, set to normal and goto appropriate case
                            this.Quality = QualityModifier.NORMAL;
                            goto case (QualityModifier.NORMAL);
                    }
                case (ArmorMaterial.STEEL):
                    switch (this.Quality) {
                        case (QualityModifier.BROKEN):
                            return "";
                        case (QualityModifier.LOW):
                            return "";
                        case (QualityModifier.NORMAL):
                            return "";
                        case (QualityModifier.HIGH):
                            return "";
                        case (QualityModifier.MYTHICAL):
                            return "";
                        default:
                            // Quality is set to unsupported value, set to normal and goto appropriate case
                            this.Quality = QualityModifier.NORMAL;
                            goto case (QualityModifier.NORMAL);
                    }
                case (ArmorMaterial.MITHRIL):
                    switch (this.Quality) {
                        case (QualityModifier.BROKEN):
                            return "";
                        case (QualityModifier.LOW):
                            return "";
                        case (QualityModifier.NORMAL):
                            return "";
                        case (QualityModifier.HIGH):
                            return "";
                        case (QualityModifier.MYTHICAL):
                            return "";
                        default:
                            // Quality is set to unsupported value, set to normal and goto appropriate case
                            this.Quality = QualityModifier.NORMAL;
                            goto case (QualityModifier.NORMAL);
                    }
                default:
                    this.Material = ArmorMaterial.STEEL;
                    goto case (ArmorMaterial.STEEL);
            }
        }
        #endregion
    }
}
