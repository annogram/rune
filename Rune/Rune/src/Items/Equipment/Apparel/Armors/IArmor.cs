﻿using System;

namespace Rune.Items.Equipment.Apparel.Armors
{
    /// <summary>
    /// 
    /// </summary>
    public interface IArmor : IApparel
    {
        #region Properties
		ArmorPos      EquipPosition { get; }
        ArmorMaterial Material      { get; }
        #endregion

        #region Methods
        ArmorMaterial RollMaterial();
		
		ArmorMaterial RollMaterial(ArmorMaterial lower, ArmorMaterial upper);
        #endregion
    }
}
