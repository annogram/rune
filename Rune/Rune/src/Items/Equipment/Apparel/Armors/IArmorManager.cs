﻿using Rune.Items.Equipment;
using Rune.Items.Equipment.Apparel.Armors;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rune.Items.Equipment.Apparel.Armors 
{
    public interface IArmorManager : IEquipmentManager<IArmor> 
	{
		#region Properties
		// Empty
		#endregion
		
		#region Methods
        /// <summary>
        /// Calculates total physical damage resistance
        /// </summary>
        /// <returns>The total damage resistance as a percentage</returns>
        int GetTotalProtection();

        /// <summary>
        /// Unequips the armor from this position.
        /// </summary>
        /// <param name="position">The position to vacate</param>
        /// <returns>The piece of armor which was removed</returns>
        IArmor Dequip(ArmorPos position);

        /// <summary>
        /// Checks what peice of armor is currently in this position.
        /// </summary>
        /// <param name="position">The position to query</param>
        /// <returns>The armor that was in this position</returns>
        IArmor Peek(ArmorPos position);
		
		/// <summary>
        /// Returns whether the given position has equipped armor
        /// </summary>
        /// <param name="position"> Position to check for equipped armor </param>
        /// <returns> True if position has equipped armor, else false </returns>
        bool IsPositionFull(ArmorPos position);
		#endregion
    }
}
