﻿using Rune.Items.Equipment.Apparel.Armors;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rune.Items.Equipment.Apparel.Armors 
{
    public class NullArmor : Armor 
	{
        #region Const Defines
        new public static readonly string ID = Armor.ID + "0";
		#endregion
	
		#region Methods
        public override string ToSimpleName() {
            return "[I'M A REAL ARMOR]";
        }

        // Null objects have no names
        protected override void InitializeUnique() { }

        protected override CarryPos SetCarryPosition() {
            return CarryPos.CANNOT_CARRY;
        }

        protected override ArmorPos SetEquipPosition() {
            return ArmorPos.INVALID_POS;
        }

        protected override ArmorMaterial SetMaterial() {
            return ArmorMaterial.PADDED;
        }

        protected override int SetProtection() {
            return 0;
        }

        protected override QualityModifier SetQuality() {
            return QualityModifier.BROKEN;
        }

        protected override int SetValue() {
            return 0;
        }

        protected override int SetWeight() {
            return 0;
        }
		
		public override string GetDescription() {
            return "The very real armor looks vaguely transparent, as if its very existence in your hands is a woopsie.";
        }
		#endregion
    }
}
