﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Rune.Items.Equipment.Apparel.Armors.Pauldrons
{
    /// <summary>
    /// Defines generic properties for Pauldrons (SHOULDER armor).
    /// </summary>
    public abstract class Pauldron : Armor
    {
        #region Const Defines
        new protected static readonly string ID = Armor.ID + "4";
        protected const ArmorPos EQUIP_POSITION = ArmorPos.SHOULDER;
        #endregion  

        #region Fields
        // Empty
        #endregion 

        #region Constructors
        // Empty
        #endregion 

        #region Methods
        protected override void InitializeCommon() {
            List<string> names = new string[] { "pauldron", "pauldrons", "shoulder", "shoulders" }.ToList();
            foreach (string s in names) {
                base.Common.Add(s.GetHashCode(), s);
            }
        }

        protected override ArmorPos SetEquipPosition() {
            return EQUIP_POSITION;
        }
        #endregion 
    }
}
