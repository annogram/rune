﻿using Rune.Util;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Rune.Items.Equipment.Apparel.Armors.Vambraces
{
    /// <summary>
    /// A heavier vambrace with some protection for the elbow.  For use by soldiers/professionals.
    /// http://www.velvet-glove.co.uk/photos/full/vam-enc.jpg
    /// </summary>
    class EnclosedVambrace : Vambrace
    {
        #region Const Defines
        new public static readonly string ID = Vambrace.ID + "A";
        // Item Properties
        private const int             VALUE             = 15;
		private const QualityModifier DEFAULT_QUALITY   = QualityModifier.NORMAL;
		// Equip Properties
        private const int             WEIGHT            = 2;
        private const CarryPos        CARRY_POSITION    = CarryPos.ONE_HAND;
        // Apparel Properties
        private const int             PROTECTION 		= 5;
        // Armor Properties
        private const ArmorMaterial   DEFAULT_MATERIAL  = ArmorMaterial.STEEL;
        #endregion  

        #region Fields
        // Empty
        #endregion 

        #region Constructors
        // Empty
        #endregion

        #region Methods
        public override string ToSimpleName() {
            return "Enclosed Vambrace"; 
        }

        protected override void InitializeUnique() {
            List<string> names = new string[] { "enclosedbracer", "enclosedbracers", "enclosedvambrace", "enclosedvambraces" }.ToList();
            foreach (string s in names) {
                base.Unique.Add(s.GetHashCode(), s);
            }
        }

        protected override ArmorMaterial SetMaterial() {
            return DEFAULT_MATERIAL;
        }

        protected override int SetProtection() {
            return PROTECTION;
        }

        protected override int SetWeight() {
            return WEIGHT;
        }

        protected override CarryPos SetCarryPosition() {
            return CARRY_POSITION;
        }

        protected override int SetValue() {
            return VALUE;
        }

        protected override QualityModifier SetQuality() {
            return DEFAULT_QUALITY;
        }
		
		public override ArmorMaterial RollMaterial() {
			this.Material = RollMaterial(ArmorMaterial.LEATHER, ArmorMaterial.MITHRIL);
			return Material;
		} 
		
		public override ArmorMaterial RollMaterial(ArmorMaterial setLower, ArmorMaterial upper) {
			ArmorMaterial lower = setLower < ArmorMaterial.LEATHER ? ArmorMaterial.BRONZE : setLower;
            int roll = RuneChance.RollDiceExponential((int)(upper - lower), (double)((int)(upper - lower) / 100)) + (int)lower; ;
            this.Material = (ArmorMaterial)roll;
			return Material;
		}

        //TODO
        public override string GetDescription() {
            switch(this.Material) {
				case(ArmorMaterial.LEATHER):
					switch(this.Quality) {       
						case(QualityModifier.BROKEN):
							return "";
						case(QualityModifier.LOW):
							return "";
						case(QualityModifier.NORMAL):
							return "";
						case(QualityModifier.HIGH):
							return "";
						case(QualityModifier.MYTHICAL):
							return "";
						default:
							// Quality is set to unsupported value, set to normal and goto appropriate case
							this.Quality = QualityModifier.NORMAL;
							goto case(QualityModifier.NORMAL);
					}
				case(ArmorMaterial.BRONZE):
					switch(this.Quality) {       
						case(QualityModifier.BROKEN):
							return "";
						case(QualityModifier.LOW):
							return "";
						case(QualityModifier.NORMAL):
							return "";
						case(QualityModifier.HIGH):
							return "";
						case(QualityModifier.MYTHICAL):
							return "";
						default:
							// Quality is set to unsupported value, set to normal and goto appropriate case
							this.Quality = QualityModifier.NORMAL;
							goto case(QualityModifier.NORMAL);
					}
				case(ArmorMaterial.IRON):
					switch(this.Quality) {       
						case(QualityModifier.BROKEN):
							return "";
						case(QualityModifier.LOW):
							return "";
						case(QualityModifier.NORMAL):
							return "";
						case(QualityModifier.HIGH):
							return "";
						case(QualityModifier.MYTHICAL):
							return "";
						default:
							// Quality is set to unsupported value, set to normal and goto appropriate case
							this.Quality = QualityModifier.NORMAL;
							goto case(QualityModifier.NORMAL);
					}
				case(ArmorMaterial.STEEL):
					switch(this.Quality) {       
						case(QualityModifier.BROKEN):
							return "";
						case(QualityModifier.LOW):
							return "";
						case(QualityModifier.NORMAL):
							return "";
						case(QualityModifier.HIGH):
							return "";
						case(QualityModifier.MYTHICAL):
							return "";
						default:
							// Quality is set to unsupported value, set to normal and goto appropriate case
							this.Quality = QualityModifier.NORMAL;
							goto case(QualityModifier.NORMAL);
					}
				case(ArmorMaterial.MITHRIL):
					switch(this.Quality) {       
						case(QualityModifier.BROKEN):
							return "";
						case(QualityModifier.LOW):
							return "";
						case(QualityModifier.NORMAL):
							return "";
						case(QualityModifier.HIGH):
							return "";
						case(QualityModifier.MYTHICAL):
							return "";
						default:
							// Quality is set to unsupported value, set to normal and goto appropriate case
							this.Quality = QualityModifier.NORMAL;
							goto case(QualityModifier.NORMAL);
					}
				default:
					this.Material = ArmorMaterial.STEEL;
					goto case (ArmorMaterial.STEEL);
			}
        }
        #endregion
    }
}
