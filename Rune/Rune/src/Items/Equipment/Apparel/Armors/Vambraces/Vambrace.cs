﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Rune.Items.Equipment.Apparel.Armors.Vambraces
{
    /// <summary>
    /// Defines generic properties for Vambraces (ARM armor).
    /// </summary>
    public abstract class Vambrace : Armor
    {
        #region Const Defines
		new protected static readonly string ID = Armor.ID + "5";
        protected const ArmorPos EQUIP_POSITION = ArmorPos.ARM;
        #endregion  

        #region Fields
        // Empty
        #endregion 

        #region Constructors
        // Empty
        #endregion 

        #region Methods
        protected override void InitializeCommon() {
            List<string> names = new string[] { "vambrace", "vambraces", "bracer", "bracers" }.ToList();
            foreach (string s in names) {
                base.Common.Add(s.GetHashCode(), s);
            }
        }

        protected override ArmorPos SetEquipPosition() {
            return EQUIP_POSITION;
        }
        #endregion 
    }
}
