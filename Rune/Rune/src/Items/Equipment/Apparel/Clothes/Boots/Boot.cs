﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Rune.Items.Equipment.Apparel.Clothes.Boots
{
    /// <summary>
    /// Defines generic properties for boots (FOOT clothing)
    /// </summary>
    public abstract class Boot : Clothing
    {
        #region Const Defines
        new protected static readonly string ID = Clothing.ID + "1";
        protected const ClothingPos EQUIP_POSITION = ClothingPos.FOOT;
        #endregion  

        #region Fields
        // Empty
        #endregion 

        #region Constructors
        // Empty
        #endregion 

        #region Methods
        protected override void InitializeCommon() {
            List<string> names = new string[] { "boot", "boots", "pairofboots" }.ToList();
            foreach (string s in names) {
                base.Common.Add(s.GetHashCode(), s);
            }
        }

        protected override ClothingPos SetEquipPosition() {
            return EQUIP_POSITION;
        }
        #endregion 
    }
}
