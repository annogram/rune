﻿using System;
using System.Collections.Generic;
using System.Linq;

using Rune.Util;

namespace Rune.Items.Equipment.Apparel.Clothes.Boots
{
    /// <summary>
    /// A kneehigh boot for middle/high class.
    /// http://api.ning.com/files/GutZeHmFpMvKUciB*MTj9ju7UBzYf9EuEkovHq69hWGO7veqfNEu5S62Z69nJjpcfvCgKdf4Cu-Svt*9wabD3nraDx*sYMVd/VampireBoots_male.png
    /// https://www.costumesofnashua.com/CNWebSite105/Active905/Pages/Steampunk/Pics%20Steampunk/PLGotham105Brn2.jpg
    /// </summary>
    public class Jackboot : Boot
    {
        #region Const Defines
        new public static readonly string ID = Boot.ID + "A";
        // Item Properties
        private const int                   VALUE              = 25;
		private const QualityModifier       DEFAULT_QUALITY    = QualityModifier.NORMAL;
		// Equip Properties
        private const int                   WEIGHT             = 4;
        private const CarryPos              CARRY_POSITION     = CarryPos.PACK;
        // Apparel Properties
        private const int                   PROTECTION 	       = 2;
        // Clothing Properties
        private readonly AffectResistance[] AFFECT_RESISTANCES = new AffectResistance[] { };
        private const ClothingMaterial      DEFAULT_MATERIAL   = ClothingMaterial.LEATHER;
        #endregion  

        #region Fields
        // Empty
        #endregion 

        #region Constructors
        // Empty
        #endregion

        #region Methods
        public override string ToSimpleName() {
            return "Jackboot";
        }

        protected override void InitializeUnique() {
            List<string> names = new string[] { "jackboot", "jackboots" }.ToList();
            foreach (string s in names) {
                base.Unique.Add(s.GetHashCode(), s);
            }
        }

        protected override IEnumerable<AffectResistance> SetResistances() {
            return AFFECT_RESISTANCES;
        }

        public override ClothingMaterial SetMaterial() {
            return DEFAULT_MATERIAL;
        }

        protected override int SetProtection() {
            return PROTECTION;
        }

        protected override int SetWeight() {
            return WEIGHT;
        }

        protected override CarryPos SetCarryPosition() {
            return CARRY_POSITION;
        }

        protected override int SetValue() {
            return VALUE;
        }

        protected override QualityModifier SetQuality() {
            return DEFAULT_QUALITY;
        }
		
		public override ClothingMaterial RollMaterial() {
			return RollMaterial(ClothingMaterial.FUR, ClothingMaterial.LEATHER);
		}
		
		public override ClothingMaterial RollMaterial(ClothingMaterial setLower, ClothingMaterial upper) {
			ClothingMaterial lower = setLower < ClothingMaterial.FUR ? ClothingMaterial.FUR : setLower;
			int roll = RuneChance.RollDiceUniform((int)(upper - lower)) + (int)lower; ;
			this.Material = (ClothingMaterial)roll;
			return Material;
		}

        //TODO
        public override string GetDescription() {
            switch(this.Material) {
				case(ClothingMaterial.LEATHER):
					switch(this.Quality) {       
						case(QualityModifier.BROKEN):
							return "";
						case(QualityModifier.LOW):
							return "";
						case(QualityModifier.NORMAL):
							return "";
						case(QualityModifier.HIGH):
							return "";
						case(QualityModifier.MYTHICAL):
							return "";
						default:
							// Quality is set to unsupported value, set to normal and goto appropriate case
							this.Quality = QualityModifier.NORMAL;
							goto case(QualityModifier.NORMAL);
					}
				case(ClothingMaterial.FUR):
					switch(this.Quality) {       
						case(QualityModifier.BROKEN):
							return "";
						case(QualityModifier.LOW):
							return "";
						case(QualityModifier.NORMAL):
							return "";
						case(QualityModifier.HIGH):
							return "";
						case(QualityModifier.MYTHICAL):
							return "";
						default:
							// Quality is set to unsupported value, set to normal and goto appropriate case
							this.Quality = QualityModifier.NORMAL;
							goto case(QualityModifier.NORMAL);
					}
				default:
					this.Material = ClothingMaterial.LEATHER;
					goto case (ClothingMaterial.LEATHER);
			}
        }
        #endregion
    }
}
