﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Rune.Items.Equipment.Apparel.Clothes.Cloaks
{
    /// <summary>
    /// Defines generic properties for a Cloak (CLOAK/CAPE clothing).
    /// </summary>
    public abstract class Cloak : Clothing
    {
        #region Const Defines
        new protected static readonly string ID = Clothing.ID + "2";
        protected const ClothingPos EQUIP_POSITION = ClothingPos.CLOAK;
        #endregion  

        #region Fields
        // Empty
        #endregion 

        #region Constructors
        // Empty
        #endregion 

        #region Methods
        protected override void InitializeCommon() {
            List<string> names = new string[] { "cloak", "cape" }.ToList();
            foreach (string s in names) {
                base.Common.Add(s.GetHashCode(), s);
            }
        }

        protected override ClothingPos SetEquipPosition() {
            return EQUIP_POSITION;
        }
        #endregion 
    }
}
