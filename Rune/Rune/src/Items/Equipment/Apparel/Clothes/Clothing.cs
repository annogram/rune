﻿using System;
using System.Collections.Generic;

using Rune.Util;

namespace Rune.Items.Equipment.Apparel.Clothes
{
    /// <summary>
    /// Declares properties for clothing.
    /// </summary>
    public abstract class Clothing : Apparel, IClothing
    {
        #region Const Defines
        new protected static readonly string ID = Apparel.ID + "2";
		#endregion
		
        #region Properties
        public ClothingPos                   EquipPosition { get; protected set; }
        public IEnumerable<AffectResistance> Resistances   { get; protected set; }
        public ClothingMaterial              Material      { get; protected set; }
        #endregion 

        #region Constructors
        protected Clothing() : base() {
            this.EquipPosition = SetEquipPosition();
            this.Resistances = SetResistances();
            this.Material = SetMaterial();
        }
        #endregion 

        #region Methods
        protected abstract ClothingPos SetEquipPosition();

        protected abstract IEnumerable<AffectResistance> SetResistances();

        public abstract ClothingMaterial SetMaterial();

        public override bool Equals(object obj) {
            if (obj.GetType() == this.GetType()) {
                IClothing armor = (IClothing)obj;
                if (armor.Quality == this.Quality && armor.Material == this.Material) {
                    return true;
                }
            }
            return false;
        }

        public override int GetHashCode() {
            return base.GetHashCode();
        }
		
		public virtual ClothingMaterial RollMaterial() {
			return RollMaterial(ClothingMaterial.LEATHER, ClothingMaterial.HEMP);
		}
		
		public virtual ClothingMaterial RollMaterial(ClothingMaterial lower, ClothingMaterial upper) {
			int roll = RuneChance.RollDiceUniform((int)(upper - lower)) + (int)lower; ;
			this.Material = (ClothingMaterial)roll;
			return Material;
		}
        #endregion 
    }
}
