﻿using System;

namespace Rune.Items.Equipment.Apparel.Clothes {
    /// <summary>
    /// Designates where worn clothing may be equipped
    /// </summary> <remarks>
    /// Positions are unique; clothing with one position cannot
    /// be applied to another position.
    /// </remarks>
    /// 
    public enum ClothingPos
    {
        INVALID_POS = -1,
        CLOAK = 0,
        TORSO = 1,
        HAND  = 2,
        LEG   = 3,
        FOOT  = 4,
		NUM_CLOTHING_POS
    }

    public enum ClothingMaterial
    {
        HEMP,
        COTTON,
		WOOL,
		SILK,
		FUR,
        LEATHER,
		NUM_MATERIALS
    }

    /// <summary>
    /// The types of resistance an item of clothing can have
    /// to a particular character STATUSAFFECT
    /// </summary> <remarks>
    /// Should be the antithesis of a character STATUSAFFECT
    /// </remarks>>
    public enum AffectResistance {
        WARM,
        FILTER
    }

    public static class ClothingEnum
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name=""></param>
        /// <param name="position"></param>
        /// <returns></returns>
        public static ClothingPos GetClothingPosition(this string position) {
            switch(position) {
                case ("CLOAK"):
                    return ClothingPos.CLOAK;
                case ("TORSO"):
                    return ClothingPos.TORSO;
                case ("HAND"):
                    return ClothingPos.HAND;
                case ("LEG"):
                    return ClothingPos.LEG;
                case ("FOOT"):
                    return ClothingPos.FOOT;
                default:
                    return ClothingPos.INVALID_POS;

            }
        }
    }
}
