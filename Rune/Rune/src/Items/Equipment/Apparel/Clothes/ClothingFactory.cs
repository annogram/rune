﻿using Rune.Items.Equipment.Apparel.Clothes;
using Rune.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;


namespace Rune.Items.Equipment.Apparel.Clothes
{
    /// <summary>
	/// Used for creating concrete clothing objects
    /// </summary>
    public abstract class ClothingFactory
    {
         public static IClothing GetClothing(string id) {
            foreach (Type v in Assembly.GetExecutingAssembly().GetTypes()
                     .Where(w => w.IsSubclassOf(typeof(Clothing)) && !(w.IsAbstract))) 
            {
                Clothing clothing = Activator.CreateInstance(v) as Clothing;
                if (id.Equals(v.GetField("ID").GetValue(clothing))) return clothing;
            }
            return new NullClothing();
        }
    }
}
