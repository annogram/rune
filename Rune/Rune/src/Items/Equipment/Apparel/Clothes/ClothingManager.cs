﻿using Rune.Items.Equipment.Apparel.Clothes;
using Rune.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rune.Items.Equipment.Apparel.Clothes
{
    /// <summary>
    /// Manager that handles currently equipped (being worn) clothing.
	/// Handles equipping, dequipping, listing, and getting relevant 
	/// singular and aggregate values from equipped clothing.
    /// </summary>
    public class ClothingManager : IClothingManager
    {
        #region Properties
		// Empty
        #endregion

        #region Fields
		public NullClothing NullClothingInstance = new NullClothing();
        /// <summary> Array of Equipped Clothing; indices referred to by the ClothingPos enum </summary>
        private IClothing[] equippedClothes;
        #endregion

        #region Constructors
        public ClothingManager() {
            equippedClothes = new IClothing[(int)ClothingPos.NUM_CLOTHING_POS];
            // change null references to NullClothing objects.
            for (int i = 0; i < (int)ClothingPos.NUM_CLOTHING_POS; i++) {
                equippedClothes[i] = NullClothingInstance;
            }
        }
        #endregion

        #region Methods
        public bool Equip(IClothing clothing) {
			if((int)clothing.EquipPosition >= 0 && (int)clothing.EquipPosition < equippedClothes.Length && equippedClothes[(int)clothing.EquipPosition] is NullClothing) {
				equippedClothes[(int)clothing.EquipPosition] = clothing;
				return true;
			}
			return false;
        }

		public IClothing Dequip(string position) {
            return Dequip(position.GetClothingPosition());
        }
		
        public IClothing Dequip(ClothingPos position) {
            if ((int)position >= 0 && (int)position < equippedClothes.Length) {
                IClothing clothing = equippedClothes[(int)position];
                equippedClothes[(int)position] = NullClothingInstance;
                return clothing;
            }
            return NullClothingInstance;
        }

		// Find() should be used prior for guaranteed result
        public IClothing Dequip(IClothing clothing) {
            int index = (int)clothing.EquipPosition;
			// Only dequip if identical, otherwise can use Dequip(ClothingPos)
			if(index >= 0 && index < equippedClothes.Length && equippedClothes[index].Equals(clothing)) {
				return Dequip(clothing.EquipPosition);
			}
            return NullClothingInstance;
        }

        public IClothing Peek(string position) {
            return Peek(position.GetClothingPosition());
        }

        public IClothing Peek(ClothingPos position) {
            if ((int)position >= 0 && (int)position < equippedClothes.Length) {
                    return equippedClothes[(int)position];
            }
            return NullClothingInstance;
        }
		
		public bool HasEquip(IClothing clothing) {
			int index = (int)clothing.EquipPosition;
			if(index >= 0 && index < equippedClothes.Length) {
				return equippedClothes[index].Equals(clothing);
			}
			return false;
		}

        public bool IsPositionFull(string position) {
            return IsPositionFull(position.GetClothingPosition());
        }
		
		public bool IsPositionFull(ClothingPos position) {
			if((int)position >= 0 && (int)position < equippedClothes.Length && !(equippedClothes[(int)position] is NullClothing)) {
				return true;
			}
            return false;
		}
		
		public IEnumerable<IClothing> Find(string name) {
            foreach (IClothing clothing in equippedClothes) {
                if(clothing.HasName(name)) {
					yield return clothing;
				}
            }
        }
		
		public IEnumerable<IClothing> ListEquips() {
            foreach (IClothing clothing in equippedClothes) {
                if(!(clothing is NullClothing)) {
					yield return clothing;
				}
            }
        }

        public int GetTotalProtection() {
			// Aggregate clothing DamageResistance to total DamageResistance of previous clothing
            return equippedClothes.Select(s => s.Protection).Aggregate((a, b) => a + b);
        }

        public int GetTotalResistance(AffectResistance resistance) {
            int totalResistance = 0;
            for(int i = 0; i < equippedClothes.Length; i++) {
                if(equippedClothes[i].Resistances.Contains(resistance)) {
                    totalResistance++;
                }
            }
            return totalResistance;
        }

        public int GetTotalWeight() {
            return equippedClothes.Select(s => s.Weight).Aggregate((a, b) => a + b);
        }
        #endregion
    }
}
