﻿using System;
using System.Collections.Generic;
using System.Linq;

using Rune.Util;

namespace Rune.Items.Equipment.Apparel.Clothes.Gloves
{
    /// <summary>
    /// A full fingered glove of varying materials and quality.  All social tiers.
    /// 
    /// </summary>
    public class FullFingerGloves : Glove
    {
        #region Const Defines
		new public static readonly string ID = Glove.ID + "A";
        // Item Properties
        private const int                   VALUE              = 5;
		private const QualityModifier       DEFAULT_QUALITY    = QualityModifier.NORMAL;
		// Equip Properties
        private const int                   WEIGHT             = 2;
        private const CarryPos              CARRY_POSITION     = CarryPos.PACK;
        // Apparel Properties
        private const int                   PROTECTION         = 2;
        // Clothing Properties
        private readonly AffectResistance[] AFFECT_RESISTANCES = new AffectResistance[] { };
        private const ClothingMaterial      DEFAULT_MATERIAL   = ClothingMaterial.LEATHER;
        #endregion  

        #region Fields
        // Empty
        #endregion 

        #region Constructors
        // Empty
        #endregion

        #region Methods
        public override string ToSimpleName() {
            return "Full-Finger Gloves";
        }

        protected override void InitializeUnique() {
            List<string> names = new string[] { "full-fingergloves", "full-fingerglove", "fullfingerglove",
                "fullfingergloves", "fullfingeredglove", "fullfingeredgloves" }.ToList();
            foreach (string s in names) {
                base.Unique.Add(s.GetHashCode(), s);
            }
        }

        protected override IEnumerable<AffectResistance> SetResistances() {
            return AFFECT_RESISTANCES;
        }

        public override ClothingMaterial SetMaterial() {
            return DEFAULT_MATERIAL;
        }

        protected override int SetProtection() {
            return PROTECTION;
        }

        protected override int SetWeight() {
            return WEIGHT;
        }

        protected override CarryPos SetCarryPosition() {
            return CARRY_POSITION;
        }

        protected override int SetValue() {
            return VALUE;
        }

        protected override QualityModifier SetQuality() {
            return DEFAULT_QUALITY;
        }
		
		public override ClothingMaterial RollMaterial() {
			return RollMaterial(ClothingMaterial.HEMP, ClothingMaterial.LEATHER);
		}
		
		public override ClothingMaterial RollMaterial(ClothingMaterial lower, ClothingMaterial upper) {
			int roll = RuneChance.RollDiceUniform((int)(upper - lower)) + (int)lower; ;
			this.Material = (ClothingMaterial)roll;
			return Material;
		}

                //TODO
        public override string GetDescription() {
            switch(this.Material) {
				case(ClothingMaterial.HEMP):
					switch(this.Quality) {       
						case(QualityModifier.BROKEN):
							return "";
						case(QualityModifier.LOW):
							return "";
						case(QualityModifier.NORMAL):
							return "";
						case(QualityModifier.HIGH):
							return "";
						case(QualityModifier.MYTHICAL):
							return "";
						default:
							// Quality is set to unsupported value, set to normal and goto appropriate case
							this.Quality = QualityModifier.NORMAL;
							goto case(QualityModifier.NORMAL);
					}
				case(ClothingMaterial.COTTON):
					switch(this.Quality) {       
						case(QualityModifier.BROKEN):
							return "";
						case(QualityModifier.LOW):
							return "";
						case(QualityModifier.NORMAL):
							return "";
						case(QualityModifier.HIGH):
							return "";
						case(QualityModifier.MYTHICAL):
							return "";
						default:
							// Quality is set to unsupported value, set to normal and goto appropriate case
							this.Quality = QualityModifier.NORMAL;
							goto case(QualityModifier.NORMAL);
					}
				case(ClothingMaterial.WOOL):
					switch(this.Quality) {       
						case(QualityModifier.BROKEN):
							return "";
						case(QualityModifier.LOW):
							return "";
						case(QualityModifier.NORMAL):
							return "";
						case(QualityModifier.HIGH):
							return "";
						case(QualityModifier.MYTHICAL):
							return "";
						default:
							// Quality is set to unsupported value, set to normal and goto appropriate case
							this.Quality = QualityModifier.NORMAL;
							goto case(QualityModifier.NORMAL);
					}
    			case(ClothingMaterial.SILK):
					switch(this.Quality) {       
						case(QualityModifier.BROKEN):
							return "";
						case(QualityModifier.LOW):
							return "";
						case(QualityModifier.NORMAL):
							return "";
						case(QualityModifier.HIGH):
							return "";
						case(QualityModifier.MYTHICAL):
							return "";
						default:
							// Quality is set to unsupported value, set to normal and goto appropriate case
							this.Quality = QualityModifier.NORMAL;
							goto case(QualityModifier.NORMAL);
					}
				case(ClothingMaterial.FUR):
					switch(this.Quality) {       
						case(QualityModifier.BROKEN):
							return "";
						case(QualityModifier.LOW):
							return "";
						case(QualityModifier.NORMAL):
							return "";
						case(QualityModifier.HIGH):
							return "";
						case(QualityModifier.MYTHICAL):
							return "";
						default:
							// Quality is set to unsupported value, set to normal and goto appropriate case
							this.Quality = QualityModifier.NORMAL;
							goto case(QualityModifier.NORMAL);
					}
				case(ClothingMaterial.LEATHER):
					switch(this.Quality) {       
						case(QualityModifier.BROKEN):
							return "";
						case(QualityModifier.LOW):
							return "";
						case(QualityModifier.NORMAL):
							return "";
						case(QualityModifier.HIGH):
							return "";
						case(QualityModifier.MYTHICAL):
							return "";
						default:
							// Quality is set to unsupported value, set to normal and goto appropriate case
							this.Quality = QualityModifier.NORMAL;
							goto case(QualityModifier.NORMAL);
					}
				default:
					this.Material = ClothingMaterial.LEATHER;
					goto case (ClothingMaterial.LEATHER);
			}
        }
        #endregion
    }
}
