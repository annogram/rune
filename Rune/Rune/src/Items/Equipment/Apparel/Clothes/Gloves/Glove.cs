﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Rune.Items.Equipment.Apparel.Clothes.Gloves
{
    /// <summary>
    /// Defines generic properties for a glove (HAND clothing)
    /// </summary>
    public abstract class Glove : Clothing
    {
        #region Const Defines
        new protected static readonly string ID = Clothing.ID + "3";
        protected const ClothingPos EQUIP_POSITION = ClothingPos.HAND;
        #endregion  

        #region Fields
        // Empty
        #endregion 

        #region Constructors       
        // Empty
        #endregion 

        #region Methods
        protected override void InitializeCommon() {
            List<string> names = new string[] { "glove", "gloves" }.ToList();
            foreach (string s in names) {
                base.Common.Add(s.GetHashCode(), s);
            }
        }

        protected override ClothingPos SetEquipPosition() {
            return EQUIP_POSITION;
        }
        #endregion 
    }
}