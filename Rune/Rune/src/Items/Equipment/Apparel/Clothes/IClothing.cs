﻿using System;
using System.Collections.Generic;

namespace Rune.Items.Equipment.Apparel.Clothes {
    /// <summary>
    /// Defines properties required by clothing.
    /// </summary>
    public interface IClothing : IApparel {
        #region Properties
		ClothingPos                   EquipPosition { get; }
        IEnumerable<AffectResistance> Resistances   { get; }
        ClothingMaterial              Material      { get; }
        #endregion 

        #region Methods
        ClothingMaterial RollMaterial();
		
		ClothingMaterial RollMaterial(ClothingMaterial lower, ClothingMaterial upper);
        #endregion 
    }
}
