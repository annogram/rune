﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rune.Items.Equipment.Apparel.Clothes 
{
    public interface IClothingManager : IEquipmentManager<IClothing> 
	{
        #region Properties
		// Empty
		#endregion
		
		#region Methods
		/// <summary>
        /// Calculates the sum of all clothing Damage Resistance.
        /// </summary>
        /// <returns> Total Damage Resistance </returns>
        int GetTotalProtection();

        /// <summary>
        /// Calculates the number of items with a given resistance.
        /// </summary>
        /// <param name="resistance"> The resistance to check against. </param>
        /// <returns> Total number of items with the given resistance. </returns>
        int GetTotalResistance(AffectResistance resistance);

        /// <summary>
        /// "Dequips" or removes a piece of equipment from the specified position.
        /// </summary>
        /// <param name="position"> Position to remove equipment from </param>
        /// <returns> The removed IEquip </returns>
        IClothing Dequip(ClothingPos position);

        /// <summary>
        /// Returns the equipment equipped at the specified position.
        /// Does not dequip the equipment.
        /// </summary>
        /// <param name="position"> Position to peek at </param>
        /// <returns> Returns the equipment equipped at the specified position. </returns>
        IClothing Peek(ClothingPos position);
		
		/// <summary>
        /// Returns whether the given position has equipped clothing
        /// </summary>
        /// <param name="position"> Position to check for equipped clothing </param>
        /// <returns> True if position has equipped clothing, else false </returns>
        bool IsPositionFull(ClothingPos position);
		#endregion
    }
}
