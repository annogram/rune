﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Rune.Items.Equipment.Apparel.Clothes;

namespace Rune.Items.Equipment.Apparel.Clothes 
{
    public class NullClothing : Clothing 
	{
        #region Const Defines
        new public static readonly string ID = Clothing.ID + "0";
		#endregion		
		
		#region Methods
        public override ClothingMaterial SetMaterial() {
            throw new NotImplementedException();
        }

        public override string ToSimpleName() {
            return "[I AM A REAL CLOTHING]";
        }

        // Null items have no names
        protected override void InitializeCommon() { }

        protected override void InitializeUnique() { }

        protected override CarryPos SetCarryPosition() {
            return CarryPos.CANNOT_CARRY;
        }

        protected override ClothingPos SetEquipPosition() {
            return ClothingPos.INVALID_POS;
        }

        protected override int SetProtection() {
            return 0;
        }

        protected override QualityModifier SetQuality() {
            return QualityModifier.BROKEN;
        }

        protected override IEnumerable<AffectResistance> SetResistances() {
            return new AffectResistance[] { };
        }

        protected override int SetValue() {
            return 0;
        }

        protected override int SetWeight() {
            return 0;
        }
		
		public override string GetDescription() {
            return "The very real clothing looks vaguely transparent, as if its very existence in your hands is a woopsie.";
        }
		#endregion
    }
}
