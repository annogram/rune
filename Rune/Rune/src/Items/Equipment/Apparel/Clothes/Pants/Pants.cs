﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Rune.Items.Equipment.Apparel.Clothes.Pants
{
    /// <summary>
    /// 
    /// </summary>
    public abstract class Pants : Clothing
    {
        #region Const Defines
        new protected static readonly string ID = Clothing.ID + "4";
        protected const ClothingPos EQUIP_POSITION = ClothingPos.LEG;
        #endregion  

        #region Fields
        // Empty
        #endregion 

        #region Constructors
        // Empty
        #endregion 

        #region Methods
        protected override void InitializeCommon() {
            List<string> names = new string[] { "pant", "pants", "legging", "leggings" }.ToList();
            foreach (string s in names) {
                base.Common.Add(s.GetHashCode(), s);
            }
        }

        protected override ClothingPos SetEquipPosition() {
            return EQUIP_POSITION;
        }
        #endregion 
    }
}