﻿using System;
using System.Collections.Generic;
using System.Linq;

using Rune.Util;

namespace Rune.Items.Equipment.Apparel.Clothes.Shirts
{
    /// <summary>
    /// A loose shirt for labour and summer days.  Of all qualities and social tiers.
    /// http://www.larpinn.co.uk/images/C2-1606natur.jpg
    /// </summary>
    public class LooseShirt : Shirt
    {
        #region Const Defines
        new public static readonly string ID = Shirt.ID + "A";
        // Item Properties
        private const int                   VALUE              = 8;
		private const QualityModifier       DEFAULT_QUALITY    = QualityModifier.NORMAL;
		// Equip Properties
        private const int                   WEIGHT             = 1;
        private const CarryPos              CARRY_POSITION     = CarryPos.PACK;
        // Apparel Properties
        private const int                   PROTECTION 		   = 0;
        // Clothing Properties
        private readonly AffectResistance[] AFFECT_RESISTANCES = new AffectResistance[] { };
        private const ClothingMaterial      DEFAULT_MATERIAL   = ClothingMaterial.HEMP;
        #endregion  

        #region Fields
        // Empty
        #endregion 

        #region Constructors
        public LooseShirt() : base() { }
        #endregion

        #region Methods
        public override string ToSimpleName() {
            return "Loose Shirt";
        }

        protected override void InitializeUnique() {
            List<string> names = new string[] { "looseshirt" }.ToList();
            foreach (string s in names) {
                base.Unique.Add(s.GetHashCode(), s);
            }
        }

        protected override IEnumerable<AffectResistance> SetResistances() {
            return AFFECT_RESISTANCES;
        }

        public override ClothingMaterial SetMaterial() {
            return DEFAULT_MATERIAL;
        }

        protected override int SetProtection() {
            return PROTECTION;
        }

        protected override int SetWeight() {
            return WEIGHT;
        }

        protected override CarryPos SetCarryPosition() {
            return CARRY_POSITION;
        }

        protected override int SetValue() {
            return VALUE;
        }

        protected override QualityModifier SetQuality() {
            return DEFAULT_QUALITY;
        }
		
		public override ClothingMaterial RollMaterial() {
			return RollMaterial(ClothingMaterial.HEMP, ClothingMaterial.SILK);
		}
		
		public override ClothingMaterial RollMaterial(ClothingMaterial lower, ClothingMaterial setUpper) {
			ClothingMaterial upper = setUpper > ClothingMaterial.SILK ? ClothingMaterial.SILK : setUpper;
			int roll = RuneChance.RollDiceUniform((int)(upper - lower)) + (int)lower; ;
			this.Material = (ClothingMaterial)roll;
			return Material;
		}

                //TODO
        public override string GetDescription() {
            switch(this.Material) {
				case(ClothingMaterial.HEMP):
					switch(this.Quality) {       
						case(QualityModifier.BROKEN):
							return "";
						case(QualityModifier.LOW):
							return "";
						case(QualityModifier.NORMAL):
							return "";
						case(QualityModifier.HIGH):
							return "";
						case(QualityModifier.MYTHICAL):
							return "";
						default:
							// Quality is set to unsupported value, set to normal and goto appropriate case
							this.Quality = QualityModifier.NORMAL;
							goto case(QualityModifier.NORMAL);
					}
				case(ClothingMaterial.COTTON):
					switch(this.Quality) {       
						case(QualityModifier.BROKEN):
							return "";
						case(QualityModifier.LOW):
							return "";
						case(QualityModifier.NORMAL):
							return "";
						case(QualityModifier.HIGH):
							return "";
						case(QualityModifier.MYTHICAL):
							return "";
						default:
							// Quality is set to unsupported value, set to normal and goto appropriate case
							this.Quality = QualityModifier.NORMAL;
							goto case(QualityModifier.NORMAL);
					}
				case(ClothingMaterial.WOOL):
					switch(this.Quality) {       
						case(QualityModifier.BROKEN):
							return "";
						case(QualityModifier.LOW):
							return "";
						case(QualityModifier.NORMAL):
							return "";
						case(QualityModifier.HIGH):
							return "";
						case(QualityModifier.MYTHICAL):
							return "";
						default:
							// Quality is set to unsupported value, set to normal and goto appropriate case
							this.Quality = QualityModifier.NORMAL;
							goto case(QualityModifier.NORMAL);
					}
				case(ClothingMaterial.SILK):
					switch(this.Quality) {       
						case(QualityModifier.BROKEN):
							return "";
						case(QualityModifier.LOW):
							return "";
						case(QualityModifier.NORMAL):
							return "";
						case(QualityModifier.HIGH):
							return "";
						case(QualityModifier.MYTHICAL):
							return "";
						default:
							// Quality is set to unsupported value, set to normal and goto appropriate case
							this.Quality = QualityModifier.NORMAL;
							goto case(QualityModifier.NORMAL);
					}
				case(ClothingMaterial.FUR):
					switch(this.Quality) {       
						case(QualityModifier.BROKEN):
							return "";
						case(QualityModifier.LOW):
							return "";
						case(QualityModifier.NORMAL):
							return "";
						case(QualityModifier.HIGH):
							return "";
						case(QualityModifier.MYTHICAL):
							return "";
						default:
							// Quality is set to unsupported value, set to normal and goto appropriate case
							this.Quality = QualityModifier.NORMAL;
							goto case(QualityModifier.NORMAL);
					}
				case(ClothingMaterial.LEATHER):
					switch(this.Quality) {       
						case(QualityModifier.BROKEN):
							return "";
						case(QualityModifier.LOW):
							return "";
						case(QualityModifier.NORMAL):
							return "";
						case(QualityModifier.HIGH):
							return "";
						case(QualityModifier.MYTHICAL):
							return "";
						default:
							// Quality is set to unsupported value, set to normal and goto appropriate case
							this.Quality = QualityModifier.NORMAL;
							goto case(QualityModifier.NORMAL);
					}
				default:
					this.Material = ClothingMaterial.LEATHER;
					goto case (ClothingMaterial.LEATHER);
			}
        }
        #endregion
    }
}
