﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Rune.Items.Equipment.Apparel.Clothes.Shirts
{
    /// <summary>
    /// Defines generic properties for shirts (TORSO clothing)
    /// </summary>
    public abstract class Shirt : Clothing
    {
        #region Const Defines
        new protected static readonly string ID = Clothing.ID + "5";
        protected const ClothingPos EQUIP_POSIITON = ClothingPos.TORSO;
        #endregion  

        #region Fields
        // Empty
        #endregion 

        #region Constructors
        // Empty
        #endregion 

        #region Methods
        protected override void InitializeCommon() {
            List<string> names = new string[] { "shirt" }.ToList();
            foreach (string s in names) {
                base.Common.Add(s.GetHashCode(), s);
            }
        }

        protected override ClothingPos SetEquipPosition() {
            return EQUIP_POSIITON;
        }
        #endregion 
    }
}