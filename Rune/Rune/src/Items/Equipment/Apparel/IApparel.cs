﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rune.Items.Equipment.Apparel
{
    /// <summary>
    /// Defines properties required by apparel (worn equipment).
    /// </summary>
    public interface IApparel : IEquipment
    {
        #region Properties
        /// <summary> Parameter determining how well the apparel reduces damage </summary>
        int Protection  { get; }
        #endregion 

        #region Methods
        // Empty
        #endregion 
    }
}
