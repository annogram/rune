﻿using System;

namespace Rune.Items.Equipment
{
    /// <summary>
    /// Declares properties required by equippable items.
    /// </summary>
    public abstract class Equipment : Item, IEquipment
    {
        #region Const Defines
        new protected static readonly string ID = Item.ID + "1";
		#endregion
		
        #region Properties
        /// <summary> The weight of the item (kgs or equivalent) </summary>
        public int      Weight        { get; protected set; }
        public CarryPos CarryPosition { get; protected set; }
        #endregion 

        #region Constructors
        protected Equipment() : base() {
            this.Weight = SetWeight();
            this.CarryPosition = SetCarryPosition();
		}
        #endregion

        #region Methods
        protected abstract int SetWeight();

        protected abstract CarryPos SetCarryPosition();
        #endregion 
    }
}
