﻿using System;
using System.Collections.Generic;
using System.Linq;


using Rune.Util;
using Rune.Items;
using Rune.Items.Equipment.Accessories.Gear;
using Rune.Items.Equipment.Accessories.Gear.Packs;
using Rune.Items.Equipment.Accessories.Gear.Belts;
using Rune.Items.Equipment.Accessories.Gear.Pouches;
using Rune.Items.Equipment.Weapons;
using Rune.Items.Equipment.Apparel.Clothes;
using Rune.Items.Equipment.Apparel.Armors;

namespace Rune.Items.Equipment 
{
    public class EquipmentController : IEquipmentController
	{
		#region Properties
		public int EquippedWeight { get; protected set; }
		// Equipment Managers
        public IWeaponManager   WeaponManager   { get; protected set; }
        public IClothingManager ClothingManager { get; protected set; }
        public IArmorManager    ArmorManager    { get; protected set; }
		// Item Storage
		public Pack  EquippedPack { get; protected set; }
		public IBelt EquippedBelt { get; protected set; }
		#endregion
		
		#region Fields
		// Null Instances
		private NullPack NullPackInstance = new NullPack();
		private NullBelt NullBeltInstance = new NullBelt();
		#endregion
		
		#region Constructors
		/// <summary>
        /// This constructor creates a controller that uses all of the manager classes.
		/// Adds 'intelligence' lacking in managers (confirms, output dialogue, etc.)
        /// </summary>
        public EquipmentController() {
			// Init properties
			EquippedWeight = 0;
			EquippedPack = NullPackInstance;
			EquippedBelt = NullBeltInstance;
			
			// Create equipment managers
            this.WeaponManager = new WeaponManager();
            this.ArmorManager = new ArmorManager();
            this.ClothingManager = new ClothingManager();
        }
		#endregion
		
		#region Methods
		public bool Equip(IEquipment equip) {
			if (equip is IGear) {
				if (equip is Pack && EquippedPack is NullPack) {
					EquippedWeight += equip.Weight;
					EquippedPack = equip as Pack;
					return true;
				} else if (equip is IBelt && EquippedBelt is NullBelt) {
					EquippedWeight += equip.Weight;
					EquippedBelt = equip as IBelt;
					return true;
				} else if (equip is Pouch) {
					// Belt performs own validity checks
					if (EquippedBelt.EquipPouch(equip as Pouch)) {
						EquippedWeight += equip.Weight;
						return true;
					}
					return false;
				}
				// Unable to equip gear
				return false;
			} else if (equip is IWeapon) {
				if (WeaponManager.Equip(equip as IEquipableWeapon)) {
					EquippedWeight += equip.Weight;
					return true;
				}
				return false;
			} else if (equip is IArmor) {
				if (ArmorManager.Equip(equip as IArmor)) {
					EquippedWeight += equip.Weight;
					return true;
				}
				return false;
			} else if (equip is IClothing) {
				if (ClothingManager.Equip(equip as IClothing)) {
					EquippedWeight += equip.Weight;
					return true;
				}
				return false;
			}
			// Unable to find valid slot for equip
            return false;
       }
		
		public bool Equip(IEquipment equip, string position) {
			WeaponPos positionAsWeaponPos = position.GetWeaponPosition();
			if (positionAsWeaponPos != WeaponPos.INVALID_POS) {
				if (WeaponManager.Equip(equip as IEquipableWeapon, positionAsWeaponPos)) {
					EquippedWeight += equip.Weight;
					return true;
				}
				return false;
			} 
			// Weapon is only type with position options, just equip to default position
			return Equip(equip);			
		}
		
		public bool Equip(IEquipment equip, WeaponPos position) {
			if (equip is IWeapon) {
				if (WeaponManager.Equip(equip as IEquipableWeapon, position)) {
					EquippedWeight += equip.Weight;
					return true;
				}
				return false;
			}
			// Equip is not weapon
			return false;
		}
	
		public IEquipment Dequip(IEquipment equip) {
			IEquipment NullEquipInstance = new NullEquip();
			IEquipment takenEquip;
			if (equip is IGear) {
				if (equip is Pack && !(EquippedPack is NullPack)) {
					takenEquip = EquippedPack as IEquipment;
					EquippedWeight -= takenEquip.Weight;
					EquippedPack = NullPackInstance;
					return takenEquip;
				} else if (equip is IBelt && !(EquippedBelt is NullBelt)) {
					takenEquip = EquippedBelt as IEquipment;
					EquippedWeight -= takenEquip.Weight;
					EquippedBelt = NullBeltInstance;
					return takenEquip;
				} else if (equip is Pouch) {
					for(int i = 0; i < EquippedBelt.PouchesCount; i++) {
						if (EquippedBelt.GetPouch(i).Equals(equip)) {
							EquippedWeight -= EquippedBelt.GetPouch(i).Weight;
							return EquippedBelt.DequipPouch(i);
						}
					}
				}
				// Unable to find/dequip gear
				return NullEquipInstance;
			} else if (equip is IWeapon) {
				takenEquip = WeaponManager.Dequip(equip as IEquipableWeapon);
				if (!(takenEquip is NullWeapon)) {
					EquippedWeight -= takenEquip.Weight;
					return takenEquip;
				} 
			} else if (equip is IArmor) {
				takenEquip = ArmorManager.Dequip(equip as IArmor);
				if (!(takenEquip is NullArmor)) {
					EquippedWeight -= takenEquip.Weight;
					return takenEquip;
				} 
			} else if (equip is IClothing) {
				takenEquip = ClothingManager.Dequip(equip as IClothing);
				if (!(takenEquip is NullClothing)) {
					EquippedWeight -= takenEquip.Weight;
					return takenEquip;
				} 
			}
			// Unable to find equal equipment to dequip
            return NullEquipInstance;
		}
		
		public IEquipment Dequip(string position) {
			IEquipment NullEquipInstance = new NullEquip();
		
			GearPos positionAsGearPos = position.GetGearPosition();
			if (positionAsGearPos != GearPos.INVALID_POS) return Dequip(positionAsGearPos);
			
			WeaponPos positionAsWeaponPos = position.GetWeaponPosition();
			if (positionAsWeaponPos != WeaponPos.INVALID_POS) return Dequip(positionAsWeaponPos);
			
			ArmorPos positionAsArmorPos = position.GetArmorPosition();
			if (positionAsArmorPos != ArmorPos.INVALID_POS) return Dequip(positionAsArmorPos);
			
			ClothingPos positionAsClothingPos = position.GetClothingPosition();
			if (positionAsClothingPos != ClothingPos.INVALID_POS) return Dequip(positionAsClothingPos);
			
			// No valid position found
			return NullEquipInstance;
		}
		
		public IEquipment Dequip(WeaponPos position) {
			IEquipment NullEquipInstance = new NullEquip();
			IEquipment takenEquip;
			
			if (position != WeaponPos.INVALID_POS) {
				takenEquip = WeaponManager.Dequip(position);
				if (!(takenEquip is NullEquipWeapon)) {
					EquippedWeight -= takenEquip.Weight;
					return takenEquip;
				} 
			}
			return NullEquipInstance;
		}
		
		public IEquipment Dequip(ArmorPos position) {
			IEquipment NullEquipInstance = new NullEquip();
			IEquipment takenEquip;
			
			if (position != ArmorPos.INVALID_POS) {
				takenEquip = ArmorManager.Dequip(position);
				if (!(takenEquip is NullArmor)) {
					EquippedWeight -= takenEquip.Weight;
					return takenEquip;
				} 
			}
			return NullEquipInstance;
		}
		
		public IEquipment Dequip(ClothingPos position) {
			IEquipment NullEquipInstance = new NullEquip();
			IEquipment takenEquip;
			
			if (position != ClothingPos.INVALID_POS) {
				takenEquip = ClothingManager.Dequip(position);
				if (!(takenEquip is NullClothing)) {
					EquippedWeight -= takenEquip.Weight;
					return takenEquip;
				} 
			}
			return NullEquipInstance;
		}
		
		public IEquipment Dequip(GearPos position) {
			IEquipment NullEquipInstance = new NullEquip();
			IEquipment takenEquip;
			
			if(position != GearPos.INVALID_POS) {
				if(position == GearPos.BACK && !(EquippedPack is NullPack)) {
					takenEquip = EquippedPack as IEquipment;
					EquippedWeight -= takenEquip.Weight;
					EquippedPack = NullPackInstance;
					return takenEquip;
				} else if (position == GearPos.WAIST && !(EquippedBelt is NullBelt)) {
					takenEquip = EquippedBelt as IEquipment;
					EquippedWeight -= takenEquip.Weight;
					EquippedBelt = NullBeltInstance;
					return takenEquip;
				} else if (position == GearPos.BELT) {
					takenEquip = EquippedBelt.DequipPouch(0);
					if (!(takenEquip is NullPouch)) {
						EquippedWeight -= takenEquip.Weight;
						return takenEquip;
					}
				}
			}
			return NullEquipInstance;
		}

        public IEnumerable<IEquipment> FindEquip(string name) {
            IEnumerable<IEquipment> weaponItems = WeaponManager.Find(name);
            for (int i = 0; i < weaponItems.Count(); i++) {
                yield return weaponItems.ElementAt(i);
            }
            IEnumerable<IEquipment> armorItems = ArmorManager.Find(name);
            for (int i = 0; i < armorItems.Count(); i++) {
                yield return armorItems.ElementAt(i);
            }
            IEnumerable<IEquipment> clothingItems = ClothingManager.Find(name);
            for (int i = 0; i < clothingItems.Count(); i++) {
                yield return clothingItems.ElementAt(i);
            }
        }

        public bool HasEquip(string name) {
			IEnumerable<IEquipment> weaponItems = WeaponManager.Find(name);
			if (weaponItems.Count() > 0) {
				return true;
			}
			IEnumerable<IEquipment> armorItems = ArmorManager.Find(name);
			if (armorItems.Count() > 0) {
				return true;
			}
			IEnumerable<IEquipment> clothingItems = ClothingManager.Find(name);
			if (clothingItems.Count() > 0) {
				return true;
			}
			return false;
		}
		
		
		public bool HasEquip(IEquipment equip) {
			// Managers perform own is a checks.
			if (equip is IWeapon && WeaponManager.HasEquip(equip as IEquipableWeapon)) return true;
			if (equip is IArmor && ArmorManager.HasEquip(equip as IArmor)) return true;
			if (equip is IClothing && ClothingManager.HasEquip(equip as IClothing)) return true;
			return false;
		}
		
		
		public IWeapon Peek(WeaponPos position) {
			return WeaponManager.Peek(position);
		}
		
		
		public IArmor Peek(ArmorPos position) {
			return ArmorManager.Peek(position);
		}
		
		
		public IClothing Peek(ClothingPos position) {
			return ClothingManager.Peek(position);
		}
		
		public IEnumerable<IEquipment> ListEquips() {
			IEnumerable<IEquipment> weaponEquips = WeaponManager.ListEquips();
			for (int i = 0; i < weaponEquips.Count(); i++) {
				yield return weaponEquips.ElementAt(i);
			}
			IEnumerable<IEquipment> armorEquips = ArmorManager.ListEquips();
			for (int i = 0; i < armorEquips.Count(); i++) {
				yield return armorEquips.ElementAt(i);
			}
			IEnumerable<IEquipment> clothingEquips = ClothingManager.ListEquips();
			for (int i = 0; i < clothingEquips.Count(); i++) {
				yield return clothingEquips.ElementAt(i);
			}
		}
		#endregion	
    }
}

