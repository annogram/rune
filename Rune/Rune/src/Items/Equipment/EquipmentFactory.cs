﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

using Rune.Util;

namespace Rune.Items.Equipment
{
    public class EquipmentFactory
    {
        #region Properties
		// Empty
		#endregion
		
		#region Constructors
		// Empty
		#endregion
		
		#region Methods
        public static IEquipment GetEquip(string id) {
            foreach (Type v in Assembly.GetExecutingAssembly().GetTypes()
                     .Where(w => w.IsSubclassOf(typeof(Equipment)) && !(w.IsAbstract))) 
            {
                Equipment equip = Activator.CreateInstance(v) as Equipment;
                if (id.Equals(v.GetField("ID").GetValue(equip))) return equip;
            }
            return new NullEquip();
        }
        #endregion
    }
}
