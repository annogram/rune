﻿using System;

namespace Rune.Items.Equipment
{
	/// <summary>
	/// Defines properties required by equippable items.
	/// </summary>
    public interface IEquipment : IItem, ICarry
    {
        #region Properties
        // Empty
        #endregion

        #region Methods
        // Empty
        #endregion
    }
}
