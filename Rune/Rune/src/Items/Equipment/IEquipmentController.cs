﻿using System;
using System.Collections.Generic;
using System.Linq;

using Rune.Util;
using Rune.Items.Equipment.Accessories.Gear;
using Rune.Items.Equipment.Accessories.Gear.Packs;
using Rune.Items.Equipment.Accessories.Gear.Belts;
using Rune.Items.Equipment.Apparel.Clothes;
using Rune.Items.Equipment.Apparel.Armors;
using Rune.Items.Equipment.Weapons;

namespace Rune.Items.Equipment
{
    public interface IEquipmentController
    {
		#region Properties
		int EquippedWeight { get; }
		// Equipment Managers
        IWeaponManager   WeaponManager   { get; }
        IClothingManager ClothingManager { get; }
        IArmorManager    ArmorManager    { get; }
		// Item Storage
		Pack  EquippedPack { get; }
		IBelt EquippedBelt { get; }
		#endregion
		
		#region Methods
        /// <summary>
		/// Finds appropriate equip location and equips the passed equipment.
		/// Will not equip over existing equipment.  If equip fails, check for
		/// and dequip existing equipment before reattempting Equip().
		/// </summary>
		/// <param name="equip"> The equipment to equip </param>
		/// <returns> True if was equipped successfully, else false </returns>
		bool Equip(IEquipment equip);
		
		/// <summary>
		/// Equips the equipment at the specified position.  If the equipment only has one valid
		/// position makes use of equip (ignores position)
		/// </summary>
		/// <param name="equip"> The equipment to equip </param>
		/// <param name="position"> The position to equip the equipment at </param>
		/// <returns> True if was equipped successfully, else false </returns>
		bool Equip(IEquipment equip, string position);
		
		/// <summary>
		/// Equips the equipment at the specified position.  Wrapper for WeaponManager function.
		/// Should only be used for weapons.
		/// </summary>
		/// <param name="equip"> The equipment to equip </param>
		/// <param name="position"> The position to equip the equipment at </param>
		/// <returns> True if was equipped successfully, else false </returns>
		bool Equip(IEquipment equip, WeaponPos position);
		
		/// <summary>
		/// Searches for an identical equipped piece of equipment to passed equip.
		/// If identical equipment is found, Dequips it. (removes and returns).
		/// </summary>
		/// <param name="equip"> The equipment to dequip </param>
		/// <returns> The Equip if dequipped successfully, else NullEquip </returns>
		IEquipment Dequip(IEquipment equip);
		
		/// <summary>
		/// Dequips the item equipped to the position indicated by the passed string.
		/// If a pouch is designated,
		/// removes the first pouch.
		/// </summary>
		/// <param name="position"> The position to dequip equipment from </param>
		/// <returns> The Equip if dequipped successfully, else NullEquip </returns>
		IEquipment Dequip(string position);
		
		/// <summary>
		/// Dequips the weapon equipped at the passed position.
		/// </summary>
		/// <param name="position"> The position to dequip equipment from </param>
		/// <returns> The Equip if dequipped successfully, else NullEquip </returns>
		IEquipment Dequip(WeaponPos position);
		
		/// <summary>
		/// Dequips the armor equipped at the passed position.
		/// </summary>
		/// <param name="position"> The position to dequip equipment from </param>
		/// <returns> The Equip if dequipped successfully, else NullEquip </returns>
		IEquipment Dequip(ArmorPos position);
		
		/// <summary>
		/// Dequips the clothing equipped at the passed position.
		/// </summary>
		/// <param name="position"> The position to dequip equipment from </param>
		/// <returns> The Equip if dequipped successfully, else NullEquip </returns>
		IEquipment Dequip(ClothingPos position);
		
		/// <summary>
		/// Dequips the gear equipped at the passed position. If a pouch is designated,
		/// removes the first pouch.
		/// </summary>
		/// <param name="position"> The position to dequip equipment from </param>
		/// <returns> The Equip if dequipped successfully, else NullEquip </returns>
		IEquipment Dequip(GearPos position);

        /// <summary>
        /// Searches the equipped equipment in the inventory for equipment with the passed name.
        /// </summary>
        /// <param name="name"> The equipment name to search for </param>
        /// <returns> Returns a list of equipment with names matching the input. </returns>
        IEnumerable<IEquipment> FindEquip(string name);
		
		/// <summary>
		/// Searches the equipped equipment in the inventory for equipment with the passed name.
		/// </summary>
		/// <param name="name"> The equipment name to search for </param>
		/// <returns> true if matching equipment is found, else false </returns>
		bool HasEquip(string name);
		
		/// <summary>
		/// Searches the equipped equipment in the inventory for equipment equal to the passed equipment.
		/// </summary>
		/// <param name="equip"> The equipment to search for </param>
		/// <returns> true if equal equipment is found, else false </returns>
		bool HasEquip(IEquipment equip);
		
		/// <summary>
		/// Peeks at the weapon equipped to the given position
		/// </summary>
		/// <param name="position"> The position to peek at </param>
		/// <returns> The weapon equipped to the passed position </returns>
		IWeapon Peek(WeaponPos position);
		
		/// <summary>
		/// Peeks at the armor equipped to the given position
		/// </summary>
		/// <param name="position"> The position to peek at </param>
		/// <returns> The armor equipped to the passed position </returns>
		IArmor Peek(ArmorPos position);
		
		/// <summary>
		/// Peeks at the clothing equipped to the given position
		/// </summary>
		/// <param name="position"> The position to peek at </param>
		/// <returns> The clothing equipped to the passed position </returns>
		IClothing Peek(ClothingPos position);
		
		/// <summary>
		/// Lists all equipped items
		/// </summary>
		/// <returns> A list of all items equipped in the inventory </returns>
		IEnumerable<IEquipment> ListEquips();
		#endregion
    }
}
