﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Rune.Util;

namespace Rune.Items.Equipment {
    public interface IEquipmentManager<T> where T : IEquipment {
        /// <summary>
        /// Equips the equipment by placing it to the relevant slot and 
        /// enabling it for use in relevant rolls.  Will only equip over 
		/// Null<T> equipment.
        /// </summary>
        /// <param name="equipment"> Equipment to equip </param>
		/// <returns> True if equip successful, otherwise false </returns>
        bool Equip(T equipment);

        /// <summary>
        /// "Dequips" or removes a piece of equipment from the specified position.
        /// </summary>
        /// <param name="position"> Position to remove equipment from </param>
        /// <remarks> Position should be a string interpreted by an EquipPosition enum. </remarks>
        /// <returns> The removed IEquip </returns>
        T Dequip(string position);

        /// <summary>
        /// "Dequips" or removes a piece of equipment that is exaclty equal to the passed 
		/// equipment. Equipment should be sourced from the Find() method for best results.
        /// </summary>
        /// <param name="equipment"> Equipment to remove </param>
        /// <returns> The removed IEquip </returns>
        T Dequip(T equipment);

        /// <summary>
        /// Returns the equipment equipped at the specified position.
        /// Does not dequip the equipment.
        /// </summary>
        /// <param name="position"> Position to peek at </param>
        /// <remarks> Position should be a string interpreted by an EquipPosition enum. </remarks>
        /// <returns> Returns the equipment equipped at the specified position. </returns>
        T Peek(string position);

        /// <summary>
        /// Checks if the specified position has something equipped.
        /// </summary>
        /// <param name="position"> Position to check for equipment </param>
        /// <remarks> Position should be a string interpreted by an EquipPosition enum. </remarks>
        /// <returns> True if has equipment, otherwise False </returns>
        bool IsPositionFull(string position);

        /// <summary>
        /// Checks if the given equipment is equipped (looks for identical match)
        /// </summary>
        /// <param name="equipment"> Equipment to check for </param>
        /// <returns> True if is equipped, otherwise false </returns>
        bool HasEquip(T equipment);

        /// <summary>
        /// Returns a list of equipment with the specified name.
        /// </summary>
        /// <param name="name"> Name to check against equipment </param>
        /// <returns> Equipment matching the specified name. </returns>
        IEnumerable<T> Find(string name);
		
		/// <summary>
        /// Returns a list of all current equipment.
        /// </summary>
        /// <returns> Returns a list of all current equipment. </returns>
        IEnumerable<T> ListEquips();

        /// <summary>
        /// Calculates the total weight of all equipped items.
        /// </summary>
        /// <returns> The total weight of all equipped items. </returns>
        int GetTotalWeight();
    }
}
