﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Rune.Items;
using Rune.Items.Equipment;

namespace Rune.Items.Equipment {
    public class NullEquip : Equipment {
        #region Const Defines
        new public static readonly string ID = Equipment.ID + "0";
		#endregion
		
        
        #region Methods
        public override string ToSimpleName() {
            return "[I'M A REAL EQUIPMENT]";
        }
		
		public override string GetDescription() {
            return "This shit be transperant.";
        }
		
        // has no names
        protected override void InitializeCommon() { }

        // has no names
        protected override void InitializeUnique() { }

        protected override CarryPos SetCarryPosition() {
            return CarryPos.CANNOT_CARRY;
        }

        protected override QualityModifier SetQuality() {
            return QualityModifier.BROKEN;
        }

        protected override int SetValue() {
            return 0;
        }

        protected override int SetWeight() {
            return 0;
        }
        #endregion
    }
}
