﻿using System;
using Rune.Util;

namespace Rune.Items.Equipment.Weapons
{
    public abstract class EquipableWeapon : Weapon, IEquipableWeapon
    {
        #region Const Defines
        new protected static readonly string ID = Weapon.ID + "1";
		#endregion
		
        #region Properties
		public WeaponPos EquipPosition { get; protected set; }
		public CarryPos  CarryPosition { get; protected set; }
		public int       Weight        { get; protected set; }
        #endregion 

        #region Constructors
        protected EquipableWeapon() : base() {
            this.EquipPosition = SetEquipPosition();
            this.CarryPosition = SetCarryPosition();
            this.Weight = SetWeight();
        }
        #endregion 

        #region Methods
        protected abstract WeaponPos SetEquipPosition();

        protected abstract CarryPos SetCarryPosition();

        protected abstract int SetWeight();
        #endregion 
    }
}
