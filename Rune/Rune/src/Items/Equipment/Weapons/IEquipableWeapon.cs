﻿using Rune.Util;
using System;

namespace Rune.Items.Equipment.Weapons
{
    /// <summary>
    /// Equipable weapons extend IWeapon and are used for weapons that a character is able to equip.
    /// </summary>
    public interface IEquipableWeapon : IWeapon, IEquipment
    {
		#region Properties
        /// <summary>
        /// The position where the weapon could be equiped.
        /// </summary>
        WeaponPos EquipPosition { get; }
		#endregion
		
		#region Methods
		// Empty
		#endregion
    }
}
