﻿using Rune.Items;
using Rune.Items.Equipment.Weapons;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rune.Items.Equipment.Weapons 
{
    /// <summary>
    /// Defines properties and methods for a weapon (any object used to attack)
    /// </summary>
    public interface IWeapon : IItem 
	{
        #region Properties
        /// <summary> Base damage of the weapon; added to damage rolls </summary>
        int                       DamageBase 	  { get; }
        /// <summary> Max number that may be rolled for in damage rolls </summary>
        int                       DamageRoll      { get; }
        IEnumerable<DamageAffect> DamageAffects   { get; }
        /// <summary> Base chance to hit to be added to attack rolls </summary>
        int                       HitChanceBase   { get; }
        WeaponMaterial            Material        { get; }
        /// <summary> How close the user has to be to the target to attempt a hit roll </summary>
        int                       AttackRange     { get; }
        /// <summary> The base block chance to add to block rolls </summary>
        int                       BlockChanceBase { get; }
        #endregion Properties

        #region Methods
        WeaponMaterial RollMaterial();
		
		WeaponMaterial RollMaterial(WeaponMaterial upper, WeaponMaterial lower);
        #endregion Methods
    }
}
