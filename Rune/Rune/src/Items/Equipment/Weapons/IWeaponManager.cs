﻿using Rune.Items.Equipment;
using Rune.Items.Equipment.Weapons;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rune.Items.Equipment.Weapons 
{
    public interface IWeaponManager : IEquipmentManager<IEquipableWeapon> 
	{
		#region Properties
		// Empty
		#endregion
	
		#region Methods
		/// <summary>
		/// Equips a weapon (one handed) in the specified hand. Will only equip over NullWeapons.
		/// </summary>
		/// <param name="weapon"> the weapon to be equipped </param>
		/// <param name="hand"> the hand in which to equip the weapon </param>
		/// <returns> True if equip successful, otherwise false </returns>
        bool Equip(IEquipableWeapon weapon, WeaponPos hand);

        /// <summary>
        /// "Dequips" or removes a piece of equipment from the specified position.
        /// </summary>
        /// <param name="position"> Position to remove equipment from </param>
        /// <returns> The removed IEquip </returns>
        IEquipableWeapon Dequip(WeaponPos position);
		
		/// <summary>
        /// Returns what weapon is currently in this position.
        /// </summary>
        /// <param name="position">The position to query</param>
        /// <returns>The weapon that was in this position</returns>
        IEquipableWeapon Peek(WeaponPos position);
		
		/// <summary>
        /// Returns whether the given position has an equipped weapon
        /// </summary>
        /// <param name="position"> Position to check for equipped weapon </param>
        /// <returns> True if position has equipped weapon, else false </returns>
        bool IsPositionFull(WeaponPos position);
		#endregion
    }
}
