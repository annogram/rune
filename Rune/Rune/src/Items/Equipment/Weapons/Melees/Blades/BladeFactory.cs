﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace Rune.Items.Equipment.Weapons.Melees.Blades
{
    #region Enums
    public enum BladeType
    {
        NUM_BLADES
    }

    public enum SwordBladeType
    {
        SHORT_BLADE,
        LONG_BLADE,
        NUM_SWORD_BLADES
    }
    #endregion

    public class BladeFactory
    {
        #region Constructors
        // Empty
        #endregion

        #region Methods
        public static Blade GetBlade(string id) {
            foreach (Type v in Assembly.GetExecutingAssembly().GetTypes()
                     .Where(w => w.IsSubclassOf(typeof(Blade)) && !(w.IsAbstract)))
            {
                Blade blade = Activator.CreateInstance(v) as Blade;
                if (id.Equals(v.GetField("ID").GetValue(blade))) return blade;
            }
            return new NullBlade();
        }
        #endregion
    }
}
