﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rune.Items.Equipment.Weapons.Melees.Blades
{
    public abstract class SwordBlade : Blade
    {
        #region Const Defines
        new protected static readonly string ID = Blade.ID + "1";
		#endregion
		
        #region Properties
        // Empty
        #endregion

        #region Constructors
        // Empty
        #endregion

        #region Methods
        protected override void InitializeCommon() {
            base.InitializeCommon();
            List<string> names = new string[] { "swordblade" }.ToList();
            foreach (string s in names) {
                base.Common.Add(s.GetHashCode(), s);
            }
        }
        #endregion
    }
}
