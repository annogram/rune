﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rune.Items.Equipment.Weapons.Melees.Hilts
{
    public abstract class Hilt : WeaponComponent
    {
        #region Const Defines
        new protected static readonly string ID = WeaponComponent.ID + "2";
		#endregion
		
        #region Properties
        // Empty
        #endregion

        #region Constructors
        // Empty
        #endregion

        #region Methods
        protected override void InitializeCommon() {
            List<string> names = new string[] { "hilt" }.ToList();
            foreach (string s in names) {
                base.Common.Add(s.GetHashCode(), s);
            }
        }
        #endregion
    }
}