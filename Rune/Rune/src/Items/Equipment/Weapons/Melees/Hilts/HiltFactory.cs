﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace Rune.Items.Equipment.Weapons.Melees.Hilts
{
    #region Enums
    public enum HiltType
    {
        SIMPLE_HILT,
        BASKET_HILT,
        NUM_HILTS
    }
    #endregion

    public class HiltFactory
    {
        #region Constructors
        // Empty
        #endregion

        #region Methods
        public static Hilt GetHilt(string id) {
            foreach (Type v in Assembly.GetExecutingAssembly().GetTypes()
                     .Where(w => w.IsSubclassOf(typeof(Hilt)) && !(w.IsAbstract))) 
            {
                Hilt hilt = Activator.CreateInstance(v) as Hilt;
                if (id.Equals(v.GetField("ID").GetValue(hilt))) return hilt;
            }
            return new NullHilt();
        }
        #endregion
    }
}