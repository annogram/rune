﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rune.Items.Equipment.Weapons.Melees.Hilts
{
    class NullHilt : Hilt
    {
        #region Const Defines
        new public static readonly string ID = Hilt.ID + "0";
		#endregion
		
        #region Properties
        // Empty
        #endregion

        #region Constructors
        // Empty
        #endregion

        #region Methods
        public override string ToSimpleName() {
            return "[I'M A REAL HILT]";
        }

        protected override void InitializeUnique() { }

        protected override string SetAppendName() {
            return "";
        }

        protected override QualityModifier SetQuality() {
            return QualityModifier.BROKEN;
        }

        protected override int SetValue() {
            return 0;
        }

        protected override int SetAttackRange() {
            return 0;
        }

        protected override IEnumerable<DamageAffect> SetDamageAffects() {
            return new DamageAffect[] { };
        }

        protected override int SetDamageBase() {
            return 0;
        }

        protected override int SetDamageRoll() {
            return 0;
        }

        protected override int SetHitChanceBase() {
            return 0;
        }

        protected override WeaponMaterial SetWeaponMaterial() {
            return WeaponMaterial.WOOD;
        }

        protected override int SetBlockChanceBase() {
            return 0;
        }

        protected override CarryPos SetCarryPosition() {
            return CarryPos.CANNOT_CARRY;
        }

        protected override int SetWeight() {
            return 0;
        }

        public override string GetDescription() {
            return "The very real hilt looks vaguely transparent, as if its very existence in your hands is a woopsie.";
        }
        #endregion
    }
}

