﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rune.Items.Equipment.Weapons.Melees.Hilts
{
    public class SimpleHilt : Hilt
    {
        #region Const Defines
        new public static readonly string ID = Hilt.ID + "A";
        // Named Properties
        private const    string          SIMPLE_NAME       = "Simple Hilt";
        // Component Properties
        private const    string          APPEND_NAME       = "Simple Hilted";
        private const    CarryPos        CARRY_POSITION    = CarryPos.PACK;
        private const    int             WEIGHT            = 4;
        // Item Properties
        private const    int             VALUE             = 5;
        private const    QualityModifier DEFAULT_QUALITY   = QualityModifier.LOW;
        // Weapon Properties
        private const    int             ATTACK_RANGE      = 0;
        private readonly DamageAffect[]  DAMAGE_AFFECTS    = new DamageAffect[] { };
        private const    int             DAMAGE_BASE       = 0;
        private const    int             DAMAGE_ROLL       = 0;
        private const    int             HIT_CHANCE_BASE   = 0;
        private const    WeaponMaterial  DEFAULT_MATERIAL  = WeaponMaterial.IRON;
        private const    int             BLOCK_CHANCE_BASE = 2;
        #endregion

        #region Properties
        // Empty
        #endregion

        #region Constructors
        // Empty
        #endregion

        #region Methods
        public override string ToSimpleName() {
            return SIMPLE_NAME;
        }

        protected override void InitializeUnique() {
            List<string> names = new string[] { "simplehilt" }.ToList();
            foreach (string s in names) {
                base.Common.Add(s.GetHashCode(), s);
            }
        }

        protected override string SetAppendName() {
            return APPEND_NAME;
        }

        protected override QualityModifier SetQuality() {
            return DEFAULT_QUALITY;
        }

        protected override int SetValue() {
            return VALUE;
        }

        protected override int SetAttackRange() {
            return ATTACK_RANGE;
        }

        protected override IEnumerable<DamageAffect> SetDamageAffects() {
            return DAMAGE_AFFECTS;
        }

        protected override int SetDamageBase() {
            return DAMAGE_BASE;
        }

        protected override int SetDamageRoll() {
            return DAMAGE_ROLL;
        }

        protected override int SetHitChanceBase() {
            return HIT_CHANCE_BASE;
        }

        protected override WeaponMaterial SetWeaponMaterial() {
            return DEFAULT_MATERIAL;
        }

        protected override int SetBlockChanceBase() {
            return BLOCK_CHANCE_BASE;
        }

        protected override CarryPos SetCarryPosition() {
            return CARRY_POSITION;
        }

        protected override int SetWeight() {
            return WEIGHT;
        }

                        //TODO
        public override string GetDescription() {
            switch(this.Material) {
				case(WeaponMaterial.WOOD):
					switch(this.Quality) {       
						case(QualityModifier.BROKEN):
							return "";
						case(QualityModifier.LOW):
							return "";
						case(QualityModifier.NORMAL):
							return "";
						case(QualityModifier.HIGH):
							return "";
						case(QualityModifier.MYTHICAL):
							return "";
						default:
							// Quality is set to unsupported value, set to normal and goto appropriate case
							this.Quality = QualityModifier.NORMAL;
							goto case(QualityModifier.NORMAL);
					}
				case(WeaponMaterial.STONE):
					switch(this.Quality) {       
						case(QualityModifier.BROKEN):
							return "";
						case(QualityModifier.LOW):
							return "";
						case(QualityModifier.NORMAL):
							return "";
						case(QualityModifier.HIGH):
							return "";
						case(QualityModifier.MYTHICAL):
							return "";
						default:
							// Quality is set to unsupported value, set to normal and goto appropriate case
							this.Quality = QualityModifier.NORMAL;
							goto case(QualityModifier.NORMAL);
					}
				case(WeaponMaterial.BRONZE):
					switch(this.Quality) {       
						case(QualityModifier.BROKEN):
							return "";
						case(QualityModifier.LOW):
							return "";
						case(QualityModifier.NORMAL):
							return "";
						case(QualityModifier.HIGH):
							return "";
						case(QualityModifier.MYTHICAL):
							return "";
						default:
							// Quality is set to unsupported value, set to normal and goto appropriate case
							this.Quality = QualityModifier.NORMAL;
							goto case(QualityModifier.NORMAL);
					}
				case(WeaponMaterial.IRON):
					switch(this.Quality) {       
						case(QualityModifier.BROKEN):
							return "";
						case(QualityModifier.LOW):
							return "";
						case(QualityModifier.NORMAL):
							return "";
						case(QualityModifier.HIGH):
							return "";
						case(QualityModifier.MYTHICAL):
							return "";
						default:
							// Quality is set to unsupported value, set to normal and goto appropriate case
							this.Quality = QualityModifier.NORMAL;
							goto case(QualityModifier.NORMAL);
					}
				case(WeaponMaterial.STEEL):
					switch(this.Quality) {       
						case(QualityModifier.BROKEN):
							return "";
						case(QualityModifier.LOW):
							return "";
						case(QualityModifier.NORMAL):
							return "";
						case(QualityModifier.HIGH):
							return "";
						case(QualityModifier.MYTHICAL):
							return "";
						default:
							// Quality is set to unsupported value, set to normal and goto appropriate case
							this.Quality = QualityModifier.NORMAL;
							goto case(QualityModifier.NORMAL);
					}
				case(WeaponMaterial.MITHRIL):
					switch(this.Quality) {       
						case(QualityModifier.BROKEN):
							return "";
						case(QualityModifier.LOW):
							return "";
						case(QualityModifier.NORMAL):
							return "";
						case(QualityModifier.HIGH):
							return "";
						case(QualityModifier.MYTHICAL):
							return "";
						default:
							// Quality is set to unsupported value, set to normal and goto appropriate case
							this.Quality = QualityModifier.NORMAL;
							goto case(QualityModifier.NORMAL);
					}
				default:
					this.Material = WeaponMaterial.IRON;
					goto case (WeaponMaterial.IRON);
			}
        }
        #endregion
    }
}
