﻿using System;

namespace Rune.Items.Equipment.Weapons.Melees
{
    /// <summary>
    /// Defines requirements for a melee weapon.
    /// </summary>
    public interface IMelee : IEquipableWeapon
    {
        #region Properties
        // Empty
        #endregion

        #region Methods
        // Empty
        #endregion
    }
}
