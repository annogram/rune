﻿using System;

namespace Rune.Items.Equipment.Weapons.Melees
{
    /// <summary>
    /// Declares properties for a melee weapon.
    /// </summary>
    public abstract class Melee : EquipableWeapon, IMelee
    {
        #region Const Defines
        new protected static readonly string ID = EquipableWeapon.ID + "1";
		#endregion
		
        #region Properties
        // Empty
        #endregion

        #region Constructors
        // Empty
        #endregion

        #region Methods
        // Empty
        #endregion
    }
}
