﻿using System;
using System.Collections.Generic;
using System.Linq;

using Rune.Util;
using Rune.Items.Equipment.Weapons.Melees.Blades;
using Rune.Items.Equipment.Weapons.Melees.Hilts;

// REVIEW new modular weapon creation
namespace Rune.Items.Equipment.Weapons.Melees
{
    /// <summary>
    /// Structural Class to provide identification of swords.
    /// </summary> <remarks>
    /// Swords should share certain characteristics, including medium speed,
    /// bleed affects, medium damage and medium damage multi.
    /// </remarks>
    public class Sword : Melee
    {
        #region Const Defines
        new public static readonly string ID = Melee.ID + "A";
        private const CarryPos  CARRY_POSITION = CarryPos.BELT;
        private const WeaponPos EQUIP_POSITION = WeaponPos.ONE_HAND;

        private readonly IList<string> BLADES = new string[] 
                { ShortBlade.ID, LongBlade.ID };

        private readonly IList<string> HILTS = new string[] 
                { BasketHilt.ID, SimpleHilt.ID };
        #endregion
		
		#region Properties
		public Blade SwordBlade { get; protected set; } = new NullBlade();
        public Hilt  SwordHilt  { get; protected set; } = new NullHilt();
		#endregion

        #region Fields
        private BladeFactory bladeFactory = new BladeFactory();
        private HiltFactory  hiltFactory  = new HiltFactory();
        
        private int   numComponents = 2;
        #endregion

        #region Constructors
        public Sword() : base() {
            SwordHilt = RandomizeHilt();
            SwordBlade = RandomizeBlade();
            ReinitializeItem();
        }

        public Sword(Hilt hilt) : base() {
            SwordHilt = hilt;
            SwordBlade = RandomizeBlade();
            ReinitializeItem();
        }

        public Sword(Blade blade) : base() {
            SwordHilt = RandomizeHilt();
            SwordBlade = blade;
            ReinitializeItem();
        }

        public Sword(Hilt hilt, Blade blade) : base() {
            SwordHilt = hilt;
            SwordBlade = blade;
            ReinitializeItem();
        }
        #endregion

        #region Methods

        public override string ToSimpleName() {
            return SwordHilt.AppendName + " " + SwordBlade.AppendName + " " + "Sword"; ;
        }

        protected override void InitializeCommon() {
            List<string> names = new string[] { "sword" }.ToList();
            foreach (string s in names) {
                base.Common.Add(s.GetHashCode(), s);
            }
        }

        protected override void InitializeUnique() {
            List<string> names = new string[] { (SwordHilt.AppendName + " " + SwordBlade.AppendName + " " + "Sword") }.ToList();
            foreach (string s in names) {
                base.Common.Add(s.GetHashCode(), s);
            }
        }

        protected override QualityModifier SetQuality() {
            int totalQuality = 0;
            totalQuality += (int)SwordBlade.Quality;
            totalQuality += (int)SwordHilt.Quality;

            int averageQuality = (int)Math.Ceiling((double)(totalQuality / numComponents));
            return (QualityModifier)averageQuality;
        }

        protected override int SetValue() {
            int totalValue = 0;
            totalValue += SwordBlade.Value;
            totalValue += SwordHilt.Value;
            return totalValue;
        }

        protected override int SetAttackRange() {
            int totalAttackRange = 0;
            totalAttackRange += SwordBlade.AttackRange;
            totalAttackRange += SwordHilt.AttackRange;
            return totalAttackRange;
        }

        protected override int SetBlockChanceBase() {
            int totalBlockChanceBase = 0;
            totalBlockChanceBase += SwordBlade.BlockChanceBase;
            totalBlockChanceBase += SwordHilt.BlockChanceBase;
            return totalBlockChanceBase;
        }

        protected override CarryPos SetCarryPosition() {
            return CARRY_POSITION;
        }

        protected override IEnumerable<DamageAffect> SetDamageAffects() {
            foreach (DamageAffect a in SwordBlade.DamageAffects) {
                yield return a;
            }
            foreach (DamageAffect a in SwordHilt.DamageAffects) {
                yield return a;
            }
        }

        protected override int SetDamageBase() {
            int totalDamageBase = 0;
            totalDamageBase += SwordBlade.DamageBase;
            totalDamageBase += SwordHilt.DamageBase;
            return totalDamageBase;
        }

        protected override int SetDamageRoll() {
            int totalDamageRoll = 0;
            totalDamageRoll += SwordBlade.DamageRoll;
            totalDamageRoll += SwordHilt.DamageRoll;
            return totalDamageRoll;
        }

        protected override WeaponPos SetEquipPosition() {
            return EQUIP_POSITION;
        }

        protected override int SetHitChanceBase() {
            int totalHitChanceBase = 0;
            totalHitChanceBase += SwordBlade.HitChanceBase;
            totalHitChanceBase += SwordHilt.HitChanceBase;
            return totalHitChanceBase;
        }

        protected override WeaponMaterial SetWeaponMaterial() {
            return SwordBlade.Material;
        }

        protected override int SetWeight() {
            int totalWeight = 0;
            totalWeight += SwordBlade.Weight;
            totalWeight += SwordHilt.Weight;
            return totalWeight;
        }
		
		private Blade RandomizeBlade() {
			// Roll from 0 to max index of arrays (as if appended)
            int bladeRoll = RuneChance.RollDiceUniform(BLADES.Count - 1);
            return BladeFactory.GetBlade(BLADES[bladeRoll]);
		}
		
		private Hilt RandomizeHilt() {
			int hiltRoll = RuneChance.RollDiceUniform(HILTS.Count - 1);
            return HiltFactory.GetHilt(HILTS[hiltRoll]);
		}
		
		private void ReinitializeItem() {
            Quality = SetQuality();
            Value = SetValue();
            AttackRange = SetAttackRange();
            BlockChanceBase = SetBlockChanceBase();
            CarryPosition = SetCarryPosition();
            DamageAffects = SetDamageAffects();
            DamageBase = SetDamageBase();
            DamageRoll = SetDamageRoll();
            EquipPosition = SetEquipPosition();
            HitChanceBase = SetHitChanceBase();
            Material = SetWeaponMaterial();
            Weight = SetWeight();
            base.Common.Clear();
            base.Unique.Clear();
            InitializeCommon();
            InitializeUnique();
        }

        public override string GetDescription() {
            throw new NotImplementedException();
        }
        #endregion
    }
}