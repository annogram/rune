﻿using Rune.Items.Equipment.Weapons;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rune.Items.Equipment.Weapons 
{
    public class NullWeapon : Weapon 
	{
        #region Const Defines
        new protected static readonly string ID = Weapon.ID + "0";
		#endregion
		
        #region Constructors
        // Empty
        #endregion

        #region Methods
        public override string ToSimpleName() {
            return "[I'M A REAL WEAPON]";
        }

        // Null Weapon has no names
        protected override void InitializeCommon() { }

        protected override void InitializeUnique() { }

        protected override int SetAttackRange() {
            return 0;
        }

        protected override int SetBlockChanceBase() {
            return 0;
        }

        protected override IEnumerable<DamageAffect> SetDamageAffects() {
            return new DamageAffect[] { };
        }

        protected override int SetDamageBase() {
            return 0;
        }

        protected override int SetDamageRoll() {
            return 0;
        }

        protected override int SetHitChanceBase() {
            return 0;
        }

        protected override QualityModifier SetQuality() {
            return QualityModifier.BROKEN;
        }

        protected override int SetValue() {
            return 0;
        }

        protected override WeaponMaterial SetWeaponMaterial() {
            return WeaponMaterial.WOOD;
        }
		
		public override string GetDescription() {
            return "The very real weapon looks vaguely transparent, as if its very existence in your hands is a woopsie.";
        }
        #endregion
    }
}
