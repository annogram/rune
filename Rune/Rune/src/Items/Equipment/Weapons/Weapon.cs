﻿using System;
using System.Collections.Generic;
using Rune.Util;

namespace Rune.Items.Equipment.Weapons
{
    public abstract class Weapon : Item, IWeapon
    {
		#region Const Defines
		new protected static readonly string ID = Item.ID + "2";
		#endregion
		
        #region Properties
        /// <summary> Base damage of the weapon; added to damage rolls </summary>
        public int                       DamageBase      { get; protected set; }
        /// <summary> Max number that may be rolled for in damage rolls </summary>
        public int                       DamageRoll      { get; protected set; }
        public IEnumerable<DamageAffect> DamageAffects   { get; protected set; }
        /// <summary> Base chance to hit to be added to attack rolls </summary>
        public int                       HitChanceBase   { get; protected set; }
        public WeaponMaterial            Material        { get; protected set; }
        public int                       AttackRange     { get; protected set; }
        public int                       BlockChanceBase { get; protected set; }
    #endregion Properties

    #region Constructors
    protected Weapon() : base() {
            this.DamageBase = SetDamageBase();
            this.DamageRoll = SetDamageRoll();
            this.DamageAffects = SetDamageAffects();
            this.HitChanceBase = SetHitChanceBase();
            this.Material = SetWeaponMaterial();
            this.AttackRange = SetAttackRange();
            this.BlockChanceBase = SetBlockChanceBase();
        }
        #endregion Constructors

        #region Methods
        protected abstract int SetDamageBase();

        protected abstract int SetDamageRoll();

        protected abstract IEnumerable<DamageAffect> SetDamageAffects();

        protected abstract int SetHitChanceBase();

        protected abstract WeaponMaterial SetWeaponMaterial();

        protected abstract int SetAttackRange();

        protected abstract int SetBlockChanceBase();

        public virtual WeaponMaterial RollMaterial() {
            return RollMaterial(WeaponMaterial.WOOD, WeaponMaterial.MITHRIL);
        }

        public virtual WeaponMaterial RollMaterial(WeaponMaterial lower, WeaponMaterial upper) {
            this.Material = (WeaponMaterial)(RuneChance.RollDiceUniform((int)(upper - lower)) + (int)lower);
            return this.Material;
        }
        #endregion Methods
    }
}
