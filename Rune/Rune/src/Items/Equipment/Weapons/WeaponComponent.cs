﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rune.Items.Equipment.Weapons
{
    public abstract class WeaponComponent : Weapon, IWeaponComponent
    {
		#region Const Defines
		new protected static readonly string ID = Weapon.ID + "2";
		#endregion
		
        #region Properties
        public string   AppendName    { get; protected set; }
        public CarryPos CarryPosition { get; protected set; }
        public int      Weight        { get; protected set; }
        #endregion

        #region Constructors
        protected WeaponComponent() : base() {
            this.AppendName = SetAppendName();
            this.CarryPosition = SetCarryPosition();
            this.Weight = SetWeight();
        }
        #endregion

        #region Methods
        protected abstract string SetAppendName();

        protected abstract CarryPos SetCarryPosition();

        protected abstract int SetWeight();
        #endregion
    }
}
