﻿using System;

namespace Rune.Items.Equipment.Weapons
{
    /// <summary>
    /// Designates where a weapon or tool can be equipped.
    /// </summary>
    public enum WeaponPos
    {
        INVALID_POS = -3,
        TWO_HAND    = -2,
        ONE_HAND    = -1,
        MAIN_HAND   =  0,
        OFF_HAND    =  1,
		NUM_WEAPON_POS
    }

    /// <summary>
    /// Describes potential damage affects that may 
    /// be applied by a weapon to cause character
    /// status affects.
    /// </summary> <remarks>
    /// Should match with a character StatusAffect
    /// </remarks>>
    public enum DamageAffect
    {
        BLEED,
        BREAK,
        STUN,
    }

    public enum WeaponMaterial
    {
        WOOD,
        STONE,
        BRONZE,
        IRON,
        STEEL,
        MITHRIL
    }

    public static class WeaponEnum
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name=""></param>
        /// <param name="position"></param>
        /// <returns></returns>
        public static WeaponPos GetWeaponPosition(this string position) {
            switch (position) {
                case ("MAIN_HAND"):
                    return WeaponPos.MAIN_HAND;
                case ("OFF_HAND"):
                    return WeaponPos.OFF_HAND;
                case ("TWO_HAND"):
                    return WeaponPos.TWO_HAND;
                default:
                    return WeaponPos.INVALID_POS;
            }
        }
    }
}
