﻿using System;
using System.Collections.Generic;
using Rune.Items.Equipment.Weapons.Melees;
using System.Linq;
using System.Reflection;


namespace Rune.Items.Equipment.Weapons
{
    /// <summary>
    /// Factory for creating weapon objects
    /// </summary>
    public abstract class WeaponFactory
    {
	     public static IWeapon GetWeapon(string id) {
            foreach (Type v in Assembly.GetExecutingAssembly().GetTypes()
                     .Where(w => w.IsSubclassOf(typeof(Weapon)) && !(w.IsAbstract))) 
            {
                Weapon weapon = Activator.CreateInstance(v) as Weapon;
                if (id.Equals(v.GetField("ID").GetValue(weapon))) return weapon;
            }
            return new NullWeapon();
        }
    }
}
