﻿using System;
using System.Collections.Generic;
using System.Linq;

using Rune.Util;
using Rune.Items.Equipment.Weapons;

namespace Rune.Items.Equipment.Weapons 
{
    public class WeaponManager : IWeaponManager 
	{
        #region Properties
        // Empty
        #endregion

        #region Fields
		private NullEquipWeapon NullWeaponInstance = new NullEquipWeapon();
        private IEquipableWeapon[] equippedWeapons;
        #endregion

        #region Constructors
        public WeaponManager() {
            equippedWeapons = new IEquipableWeapon[(int)WeaponPos.NUM_WEAPON_POS];
            for (int i = 0; i < equippedWeapons.Length; i++) {
                this.equippedWeapons[i] = NullWeaponInstance;
            }
        }
        #endregion

        #region Methods
		public bool Equip(IEquipableWeapon weapon, WeaponPos hand) {
			if(weapon.EquipPosition == WeaponPos.TWO_HAND) {
				return Equip(weapon);
			}
			
			if ((int)weapon.EquipPosition >= 0 && (int)weapon.EquipPosition < equippedWeapons.Length) {
				equippedWeapons[(int)hand] = weapon;
				return true;
            }
            return false;
		}
		
        public bool Equip(IEquipableWeapon weapon) {
            if(weapon.EquipPosition == WeaponPos.TWO_HAND) {
				if(equippedWeapons[(int)WeaponPos.MAIN_HAND] is NullEquipWeapon && equippedWeapons[(int)WeaponPos.OFF_HAND] is NullEquipWeapon) {
					equippedWeapons[(int)WeaponPos.MAIN_HAND] = weapon;
					equippedWeapons[(int)WeaponPos.OFF_HAND] = weapon;
					return true;
				} else {
					// Slots not empty
					return false;
				}
			} else if(weapon.EquipPosition == WeaponPos.ONE_HAND) {
				if(equippedWeapons[(int)WeaponPos.MAIN_HAND] is NullEquipWeapon) {
					equippedWeapons[(int)WeaponPos.MAIN_HAND] = weapon;
					return true;
				} else if(equippedWeapons[(int)WeaponPos.OFF_HAND] is NullEquipWeapon) {
					equippedWeapons[(int)WeaponPos.OFF_HAND] = weapon;
					return true;
				} else {
					// No empty slot available
					return false;
				}
			} else if((int)weapon.EquipPosition >= 0 && (int)weapon.EquipPosition < equippedWeapons.Length && equippedWeapons[(int)weapon.EquipPosition] is NullEquipWeapon) {
				equippedWeapons[(int)weapon.EquipPosition] = weapon;
				return true;
            }
			// Invalid weapon passed or slots not empty
            return false;
        }

		public IEquipableWeapon Dequip(string position) {
            return Dequip(position.GetWeaponPosition());
        }
		
        public IEquipableWeapon Dequip(WeaponPos position) {
            // Check if the weapon we're removing is two handed
            IEquipableWeapon weapon;
            if (position == WeaponPos.TWO_HAND) {
                weapon = equippedWeapons[(int)WeaponPos.MAIN_HAND];
                // Null both hands
				equippedWeapons[(int)WeaponPos.MAIN_HAND] = NullWeaponInstance;
				equippedWeapons[(int)WeaponPos.OFF_HAND] = NullWeaponInstance;
            } else {
                weapon = equippedWeapons[(int)position];
                if (weapon.EquipPosition == WeaponPos.TWO_HAND) {
					// Null both hands
					equippedWeapons[(int)WeaponPos.MAIN_HAND] = NullWeaponInstance;
					equippedWeapons[(int)WeaponPos.OFF_HAND] = NullWeaponInstance;
				} else {
					equippedWeapons[(int)position] = NullWeaponInstance;
				}
            }
            return weapon;
        }

        public IEquipableWeapon Dequip(IEquipableWeapon weapon) {
			// Checks off hand first as off hand suffers an attack penalty
            if(equippedWeapons[(int)WeaponPos.OFF_HAND].Equals(weapon)) {
				return Dequip(WeaponPos.OFF_HAND);
			} else if(equippedWeapons[(int)WeaponPos.MAIN_HAND].Equals(weapon)) {
				return Dequip(WeaponPos.MAIN_HAND);
			}
            return NullWeaponInstance;
        }

        public IEquipableWeapon Peek(string position) {
            return Peek(position.GetWeaponPosition());
        }
		
		public IEquipableWeapon Peek(WeaponPos position) {
			// Return MAIN_HAND if is TWO_HAND otherwise return weapon for that position
			if(position == WeaponPos.TWO_HAND || position == WeaponPos.ONE_HAND) {
                return equippedWeapons[(int)WeaponPos.MAIN_HAND];
			} else if((int)position >= 0 && (int)position < equippedWeapons.Length) {
				return equippedWeapons[(int)position];
			}
			return NullWeaponInstance;
        }
		
		public bool HasEquip(IEquipableWeapon weapon) {
			WeaponPos position = weapon.EquipPosition;
			if(position == WeaponPos.TWO_HAND) {
				return equippedWeapons[(int)WeaponPos.MAIN_HAND].Equals(weapon);
			} else if (position == WeaponPos.ONE_HAND) {
				return equippedWeapons[(int)WeaponPos.MAIN_HAND].Equals(weapon) 
					   ? equippedWeapons[(int)WeaponPos.MAIN_HAND].Equals(weapon) 
					   : equippedWeapons[(int)WeaponPos.OFF_HAND].Equals(weapon);
			} else if((int)position > 0 && (int)position < equippedWeapons.Length) {
				return equippedWeapons[(int)position].Equals(weapon);
			}
			// Invalid position passed
			return false;
		}

        public bool IsPositionFull(string position) {
			return IsPositionFull(position.GetWeaponPosition());
		}
		
		public bool IsPositionFull(WeaponPos position) {
			if(position == WeaponPos.TWO_HAND) {
				return !(equippedWeapons[(int)WeaponPos.MAIN_HAND] is NullEquipWeapon);
			} else if (position == WeaponPos.ONE_HAND) {
				return !(equippedWeapons[(int)WeaponPos.MAIN_HAND] is NullEquipWeapon || equippedWeapons[(int)WeaponPos.OFF_HAND] is NullEquipWeapon);
			} else if((int)position > 0 && (int)position < equippedWeapons.Length) {
				return !(equippedWeapons[(int)position] is NullEquipWeapon);
			}
			// Invalid position passed
			return true;
        }

        public IEnumerable<IEquipableWeapon> Find(string name) {
            foreach(IEquipableWeapon weapon in equippedWeapons) {
                if (weapon.HasName(name)) {
					yield return weapon;
				}
            }
        }
		
		public IEnumerable<IEquipableWeapon> ListEquips() {
            foreach (IEquipableWeapon weapon in equippedWeapons) {
                if(!(weapon is NullEquipWeapon)) {
					yield return weapon;
				}
            }
        }
		
        public int GetTotalWeight() {
            return equippedWeapons.Select(s => s.Weight).Aggregate((a, b) => a + b);
        }
        #endregion
    }


}

