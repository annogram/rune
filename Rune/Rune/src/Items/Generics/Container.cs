﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Rune.Items.Containers
{
    /// <summary>
    /// Defines methods and properties for items that store items
    /// </summary>
    public abstract class Container : Item, IContainer
    {
        #region Const Defines
        new protected static readonly string ID = Item.ID + "3";
		#endregion
		
        #region Properties
        public CarryPos ContainerType { get; protected set; }
        public int      MaxWeight     { get; protected set; }
        public int      CurrentWeight { get; protected set; }
        public int      MaxItemSlots  { get; protected set; }
        #endregion

        #region Fields
		private NullItem NullItemInstance;
        protected IList<ICarry> Items;
        #endregion

        #region Constructors
        protected Container() : base() {
            this.ContainerType = SetContainerType();
            this.MaxWeight = SetMaxWeight();
            this.MaxItemSlots = SetMaxItemSlots();
            this.CurrentWeight = 0;
            Items = new List<ICarry>(MaxItemSlots);
        }
        #endregion

        #region Methods
        protected abstract CarryPos SetContainerType();

        protected abstract int SetMaxWeight();

        protected abstract int SetMaxItemSlots();

		// TODO this method will have to be updated to handle refilling consumables
        public virtual bool Store(ICarry item) {
            if(!this.IsFull() && ContainerType.CanCarry(item)) {
                Items.Add(item);
				CurrentWeight += item.Weight;
                return true;
            }
            return false;
        }

        public virtual IItem Take(string item) {
            int index = this.IndexOf(item);
            if(index >= 0) {
                ICarry takenItem = Items.ElementAt(index);
                Items.RemoveAt(index);
				CurrentWeight -= takenItem.Weight;
                return takenItem;
            }
            return NullItemInstance;
        }

        public virtual bool CanCarry(IItem item) {
            if (item is ICarry) {
                return ContainerType.CanCarry((ICarry)item);
            }
            return false;
        }

        public virtual bool Contains(IItem item) {
            if (!(item is ICarry)) return false;

            ICarry carryItem = item as ICarry;
            // Check for an identical item first
            if(Items.Contains(carryItem)) {
                return true;
            }

            // Check for a similar item of the same derived type
            for (int i = 0; i < Items.Count; i++) {
                if (Items[i].GetType() == item.GetType()) {
                    return true;
                }
            }
            return false;
        }

        public virtual bool Contains(string name) {
            for (int i = 0; i < Items.Count; i++) {
                if (Items[i].HasName(name)) {
                    return true;
                }
            }
            return false;
        }
		
		public virtual IEnumerable<IItem> Find(string name) {
			for(int i = 0; i < Items.Count; i++) {
				if(Items[i].HasName(name)) {
					yield return Items[i];
				}
			}
		}

        public virtual bool IsFull() {
            if (Items.Count >= MaxItemSlots || CurrentWeight >= MaxWeight) {
                return true;
            }
            return false;
        }
		
		public virtual IEnumerable<IItem> ListItems() {
			for(int i = 0; i < Items.Count; i++) {
				yield return Items[i];
			}
		}

        protected int IndexOf(IItem item) {
            // Check for an identical item first
            for (int i = 0; i < Items.Count; i++) {
                if (Items.Equals(item)) {
                    return i;
                }
            }

            // Check for a similar item of the same derived type
            for (int i = 0; i < Items.Count; i++) {
                if (Items[i].GetType() == item.GetType()) {
                    return i;
                }
            }
            return -1;
        }

        protected int IndexOf(string name) {
            for (int i = 0; i < Items.Count; i++) {
                if (Items[i].HasName(name)) {
                    return i;
                }
            }
            return -1;
        }
        #endregion
    }
}
