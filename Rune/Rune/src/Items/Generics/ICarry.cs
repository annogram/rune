﻿using System;

using Rune.Util;

namespace Rune.Items
{
    /// <summary>
    /// Defines properties for items that can be carried.
    /// </summary>
    public interface ICarry : IItem
    {
        #region Properties
        /// <summary> The weight of the item (kgs or equivalent) </summary>
        int           Weight   { get; }
        CarryPos      CarryPosition { get; }
        #endregion

        #region Methods
		// Empty
        #endregion
    }
}
