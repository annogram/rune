﻿using System;
using System.Collections.Generic;

namespace Rune.Items.Containers
{
    /// <summary>
    /// Defines properties and methods for containers (items that store items, such as chests or packs).
    /// </summary>
    public interface IContainer : IItem
    {
        #region Properties
        CarryPos ContainerType { get; } 
        /// <summary> Max weight that can be carried by the container </summary>
        int      MaxWeight     { get; }
        /// <summary> Current weight being carried by the container </summary>
        int      CurrentWeight { get; }
        /// <summary> Max number of items the container can hold </summary>
        int      MaxItemSlots  { get; }
        #endregion

        #region Methods
        /// <summary>
        /// Places an item into the container.  Will not store if container is full,
		/// or if the item is unable to be stored in that type of container.
        /// </summary>
        /// <param name="item"> The item to be stored </param>
        /// <returns> True if stored, else false </returns>
        bool Store(ICarry item);

        /// <summary>
        /// Takes an item from the container
        /// </summary>
        /// <param name="item"> The name of the item to be taken </param>
        /// <returns> The item taken from the container </returns>
        IItem Take(string item);

        /// <summary>
        /// Checks if the container can carry a given item
        /// </summary>
        /// <param name="item"> The item to be stored/carried </param>
        /// <returns> true if can be carried, otherwise false. </returns>
        bool CanCarry(IItem item);

        /// <summary>
        /// Checks if the container is storing a given item
        /// </summary>
        /// <param name="item"> The item to search for </param>
        /// <returns> true if is holding item, otherwise false </returns>
        bool Contains(IItem item);

        /// <summary>
        /// Checks if the container is storing a given item
        /// </summary>
        /// <param name="item"> The name of the item to search for </param>
        /// <returns> true if is holding item, otherwise false </returns>
        bool Contains(string item);
		
		/// <summary>
        /// Returns a list of items with the specified name.
        /// </summary>
        /// <param name="name"> Name to check against items </param>
        /// <returns> Items matching the specified name. </returns>
		IEnumerable<IItem> Find(string name);

        /// <summary>
        /// Checks if the container is storing MAXITEMSLOTS number of items
        /// </summary>
        /// <returns> true if is full, otherwise false </returns>
        bool IsFull();

        /// <summary>
        /// Lists stored items
        /// </summary>
        /// <returns> A list of the stored items </returns>
        IEnumerable<IItem> ListItems();
        #endregion
    }
}
