﻿using System;
using System.Collections.Generic;

using Rune.Items.Equipment;
using Rune.Items.Equipment.Weapons;
using Rune.Items.Equipment.Apparel.Armors;
using Rune.Items.Equipment.Apparel.Clothes;

namespace Rune.Items 
{
	/// <summary>
	/// The inventory is used to consolidate different methods a character may store items
	/// into one object, and manages where items are stored as defined by the item objects
	/// requirements.
	/// </summary>
    public interface IInventory
	{
        #region Properties
		int StoredWeight   { get; }
		#endregion
		
		#region Methods
		/// <summary>
		/// Stores the given item in the appropriate inventory space.
		/// Priority given to storing the item in the 'smallest' carry
		/// position available that can carry it.  If the default space
		/// is invalid searches for a valid space.
		/// </summary>
		/// <param name="item"> The item to store </param>
		/// <returns> True if item able to be stored, else false </returns>
		bool Store(ICarry item);
		
		/// <summary>
		/// Stores the given item in the given location.  If the location cannot
		/// store the item, returns without searching for another valid location.
		/// </summary> <remarks>
		/// A location may be invalid as the location is full, or the item cannot be carried
		/// there.
		/// </remarks>
		/// <param name="item"> The item to store </param>
		/// <param name="location"> The location to store the item in </param>
		/// <returns> True if item able to be stored, else false </returns>
		bool Store(ICarry item, CarryPos location);
		
		/// <summary>
		/// Searches items STORED in inventory (not equipped) for the item identified by the string.
		/// Returns the item if found.
		/// </summary> <remarks>
		/// If need to remove equipped items from inventory, Dequip() should be used instead.
		/// </remarks>
		/// <param name="item"> The string identifying the item (name) </param>
		/// <returns> The taken item if found, else NullItem </returns>
		IItem Take(string item);
		
		/// <summary>
		/// Searches items STORED in inventory (not equipped) at the specified location.
		/// If found, returns the item, otherwise return NullItem without searching other 
		/// possible locations.
		/// </summary> <remarks>
		/// If need to remove equipped items from inventory, Dequip() should be used instead.
		/// </remarks>
		/// <param name="item"> The string identifying the item (name) </param>
		/// <returns> The taken item if found, else NullItem </returns>
		IItem Take(string item, CarryPos location);
	
		/// <summary>
		/// Checks whether an item equal to the passed item is stored or equipped.
		/// </summary>
		/// <param name="item"> The item to search for </param>
		/// <returns> True if a copy of the item is found, else false </returns>
		bool Contains(IItem item);
		
		/// <summary>
		/// Checks whether an item with the name of the passed string is stored or equipped.
		/// </summary>
		/// <param name="name"> The item name to search for </param>
		/// <returns> True if an item with the passed name is found, else false </returns>
		bool Contains(string name);

		/// <summary>
		/// Searches the stored and equipped items in the inventory for items with the passed name.
		/// </summary>
		/// <param name="name"> The item name to search for </param>
		/// <returns> Returns a list of items with names matching the input. </returns>
		IEnumerable<IItem> Find(string name);
		
		/// <summary>
		/// Searches the stored items in the inventory for items with the passed name.
		/// </summary>
		/// <param name="name"> The item name to search for </param>
		/// <returns> Returns a list of items with names matching the input. </returns>
		IEnumerable<IItem> FindItem(string name);
		
		/// <summary>
		/// Lists all stored and equipped items
		/// </summary>
		/// <returns> A list of all items stored and equipped in the inventory </returns>
		IEnumerable<IItem> List();
		
		/// <summary>
		/// Lists all stored items
		/// </summary>
		/// <returns> A list of all items stored in the inventory </returns>
		IEnumerable<IItem> ListItems();
		
		/// <summary>
		/// Checks if the inventory is full
		/// </summary>
		/// <returns> true if the inventory cannot store anymore items, else false </returns>
		bool IsFull();
		
		/// <summary>
		/// Checks if the given position is full
		/// </summary>
		/// <param name="position"> The position to check is full </param>
		/// <returns> true if the given position cannot store anymore items, else false </returns>
		bool IsFull(CarryPos position);
		#endregion
    }
}
