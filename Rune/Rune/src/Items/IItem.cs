﻿using System;
using Rune.Util;

namespace Rune.Items
{
	/// <summary>
	/// Defines properties of an item
	/// </summary>
    public interface IItem : INamed
    {
        #region Properties
        /// <summary> The gold value of the item </summary>
        int             Value   { get; }
        QualityModifier Quality { get; }
        #endregion

        #region Methods
		QualityModifier RollQuality();
		
        QualityModifier RollQuality(QualityModifier lowestQuality, QualityModifier highestQuality);
        #endregion
    }
}
