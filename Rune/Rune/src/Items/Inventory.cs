﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Rune.Util;
using Rune.Items;
using Rune.Items.Equipment;
using Rune.Items.Equipment.Weapons;
using Rune.Items.Equipment.Apparel.Armors;
using Rune.Items.Equipment.Apparel.Clothes;
using Rune.Items.Equipment.Accessories.Gear;
using Rune.Items.Equipment.Accessories.Gear.Belts;
using Rune.Items.Equipment.Accessories.Gear.Packs;
using Rune.Items.Equipment.Accessories.Gear.Pouches;

// REVIEW Big class with a lot of functionality, should be reviewed for style and bugs.
//		  Undecided whether is too big or OK, equip methods may move back to EquipController which could be used as a property.
// Belt may also be in need of a review, this class shows why pouches were made semi-visible (for simplicity of control, less wrapping of functions)
namespace Rune.Items 
{
    /// <summary>
    /// Manager to handle storing items.
    /// </summary>
    public class Inventory 
	{
		#region Const Defines
		private const int POCKETS_INVENTORY_SPACE = 4;
		private const int HANDS_INVENTORY_SPACE   = 2; // Const define in prep for Day 1 three hands expansion
		#endregion
		
		#region Properties
		public IEquipmentController EquipmentController { get; protected set; }
		public int                  StoredWeight        { get; protected set; }
		#endregion
		
        #region Fields		
		private NullItem NullItemInstance = new NullItem();
		
		private IList<ICarry> pockets = new List<ICarry>(POCKETS_INVENTORY_SPACE);
		private IList<ICarry> hands   = new List<ICarry>(HANDS_INVENTORY_SPACE); // Mutually exclusive with weaponManager
        #endregion

        #region Constructors
        public Inventory() {
			// Init properties
			StoredWeight = 0;
			
            EquipmentController = new EquipmentController();
        }
        #endregion

        #region Methods
		public bool Store(IItem item) {
			if(!(item is ICarry)) return false;

            ICarry carryItem = item as ICarry;
			CarryPos reqCarryPosition = carryItem.CarryPosition;
            foreach(CarryPos pos in Enum.GetValues(typeof(CarryPos))) {
                if (Store(item, pos)) return true;
            }
            return false;
        }
		
		public bool Store(IItem item, CarryPos location) {
			if(!(item is ICarry)) return false;

            ICarry carryItem = item as ICarry;
			switch(location) {
				case(CarryPos.POCKET):
					if(pockets.Count < POCKETS_INVENTORY_SPACE) {
						pockets.Add(carryItem);
						StoredWeight += carryItem.Weight;
						return true;
					}
					return false;
                case (CarryPos.POUCH):
					// No need to check for equippedBelt == null as will be NullBelt if none equipped
					for(int i = 0; i < EquipmentController.EquippedBelt.PouchesCount; i++) {
						// Pouch performs own IsFull and CanCarry checks
						// Check all pouches before return false
						if(EquipmentController.EquippedBelt.GetPouch(i).Store(carryItem)) {
							StoredWeight += carryItem.Weight;
							return true;
						}
					}
					return false;
				case(CarryPos.PACK):
					if(EquipmentController.EquippedPack.Store(carryItem)) {
						StoredWeight += carryItem.Weight;
						return true;
					}
					return false;
				case(CarryPos.BELT):
					if(EquipmentController.EquippedBelt.Store(carryItem)) {
						StoredWeight += carryItem.Weight;
						return true;
					}
					return false;
				case(CarryPos.ONE_HAND):
					if(hands.Count < HANDS_INVENTORY_SPACE) {
						hands.Add(carryItem);
						StoredWeight += carryItem.Weight;
						return true;
					}
					return false;
				case(CarryPos.TWO_HAND):
					if(hands.Count == 0) {
						hands.Add(carryItem);
						hands.Add(NullItemInstance);
						StoredWeight += carryItem.Weight;
						return true;
					}
					return false;
				default:
					return false;
			}
		}

		public IItem Take(string item) {
			IItem takenItem = NullItemInstance;
            foreach (CarryPos pos in Enum.GetValues(typeof(CarryPos))) {
                if (!((takenItem = Take(item, pos)) is NullItem)) return takenItem;
            }
			return takenItem;
		}
				
		public IItem Take(string item, CarryPos location) {
			ICarry takenItem = NullItemInstance;
			
			switch(location) {
				case(CarryPos.POCKET):
					for(int i = 0; i < pockets.Count; i++) {
						if(pockets[i].HasName(item)) {
							takenItem = pockets[i];
                            pockets.RemoveAt(i);
							StoredWeight -= takenItem.Weight;
							return takenItem;
						}
					}
					return NullItemInstance;
				case(CarryPos.POUCH):
					for(int i = 0; i < EquipmentController.EquippedBelt.PouchesCount; i++) {
						if(!((takenItem = (ICarry)EquipmentController.EquippedBelt.GetPouch(i).Take(item)) is NullItem)) {
							StoredWeight -= takenItem.Weight;
							return takenItem;
						}
					}
					return NullItemInstance;
				case(CarryPos.PACK):
					if(!((takenItem = (ICarry)EquipmentController.EquippedPack.Take(item)) is NullItem)) {
						StoredWeight -= takenItem.Weight;
						return takenItem;
					}
					return NullItemInstance;
				case(CarryPos.BELT):
					if(!((takenItem = (ICarry)EquipmentController.EquippedBelt.Take(item)) is NullItem)) {
						StoredWeight -= takenItem.Weight;
						return takenItem;
					}
					return NullItemInstance;
                case (CarryPos.ONE_HAND):
                case (CarryPos.TWO_HAND):
					for(int i = 0; i < hands.Count; i++) {
						if (hands[i].HasName(item)) {
							takenItem = hands[i];
							hands.RemoveAt(i);
							StoredWeight -= takenItem.Weight;
							// Check for TWO_HANDS, if is it will have been detected at 0
							if(takenItem.CarryPosition == CarryPos.TWO_HAND) hands.RemoveAt(1);
							return takenItem;
						}
					}
					return NullItemInstance;
				// Invalid CarryPos, shouldn't reach
				default:
					return NullItemInstance;
			}
		}
		
		public bool Contains(IItem item) {
			for (int i = 0; i < pockets.Count; i++) {
				if(pockets[i].Equals(item)) return true;
			}
			for (int i = 0; i < hands.Count; i++) {
				if(hands[i].Equals(item)) return true;
			}
			if (EquipmentController.EquippedPack.Contains(item) || EquipmentController.EquippedPack.Equals(item)) return true;
			if (EquipmentController.EquippedBelt.Contains(item) || EquipmentController.EquippedBelt.Equals(item)) return true;
			for (int i = 0; i < EquipmentController.EquippedBelt.PouchesCount; i++) {
				if (EquipmentController.EquippedBelt.GetPouch(i).Contains(item) || EquipmentController.EquippedBelt.GetPouch(i).Equals(item)) return true;
			}
			if (item is IWeapon   && EquipmentController.WeaponManager.HasEquip(item as IEquipableWeapon)) return true;
			if (item is IArmor    && EquipmentController.ArmorManager.HasEquip(item as IArmor))            return true;
			if (item is IClothing && EquipmentController.ClothingManager.HasEquip(item as IClothing))      return true;

			return false;
		}
		
		public bool Contains(string name) {
			for(int i = 0; i < pockets.Count; i++) {
				if(pockets[i].HasName(name)) return true;
			}
			for(int i = 0; i < hands.Count; i++) {
				if(hands[i].HasName(name)) return true;
			}
			if (EquipmentController.EquippedPack.Contains(name) || EquipmentController.EquippedPack.HasName(name)) return true;
			if (EquipmentController.EquippedBelt.Contains(name) || EquipmentController.EquippedBelt.HasName(name)) return true;
			for (int i = 0; i < EquipmentController.EquippedBelt.PouchesCount; i++) {
				if (EquipmentController.EquippedBelt.GetPouch(i).Contains(name) || EquipmentController.EquippedBelt.GetPouch(i).HasName(name)) return true;
			}
			IEnumerable<IWeapon> foundWeapons = EquipmentController.WeaponManager.Find(name);
			if (foundWeapons.Count() > 0) return true;
			IEnumerable<IArmor> foundArmors = EquipmentController.ArmorManager.Find(name);
			if (foundArmors.Count() > 0) return true;
			IEnumerable<IClothing> foundClothes = EquipmentController.ClothingManager.Find(name);
			if (foundClothes.Count() > 0) return true;

            return false;
		}
		
		public IEnumerable<IItem> Find(string name) {
			IEnumerable<IItem> foundItems = FindItem(name);
			for (int i = 0; i < foundItems.Count(); i++) {
				yield return foundItems.ElementAt(i);
			}
			IEnumerable<IItem> foundEquips = EquipmentController.FindEquip(name);
			for (int i = 0; i < foundEquips.Count(); i++) {
				yield return foundEquips.ElementAt(i);
			}
		}
		
		public IEnumerable<IItem> FindItem(string name) {
			for (int i = 0; i < pockets.Count; i++) {
				if (pockets[i].HasName(name)) yield return pockets[i];
			} 
			for (int i = 0; i < hands.Count; i++) {
				if (hands[i].HasName(name)) yield return hands[i];
			}
			IEnumerable<IItem> packItems = EquipmentController.EquippedPack.Find(name);
			for (int i = 0; i < packItems.Count(); i++) {
				yield return packItems.ElementAt(i);
			}
			IEnumerable<IItem> beltItems = EquipmentController.EquippedBelt.Find(name);
			for (int i = 0; i < beltItems.Count(); i++) {
				yield return beltItems.ElementAt(i);
			}
			for (int i = 0; i < EquipmentController.EquippedBelt.PouchesCount; i++) {
				IEnumerable<IItem> pouchItems = EquipmentController.EquippedBelt.GetPouch(i).Find(name);
				for (int j = 0; j < pouchItems.Count(); j++) {
					yield return pouchItems.ElementAt(j);
				}
			}
		}
		
		public IEnumerable<IItem> List() {
			IEnumerable<IItem> items = ListItems();
			for (int i = 0; i < items.Count(); i++) {
				yield return items.ElementAt(i);
			}
			IEnumerable<IEquipment> equips = EquipmentController.ListEquips();
			for (int i = 0; i < equips.Count(); i++) {
				yield return equips.ElementAt(i);
			}			
		}
		
		public IEnumerable<IItem> ListItems() {
			for (int i = 0; i < pockets.Count; i++) {
				yield return pockets[i];
			}
			for (int i = 0; i < hands.Count; i++) {
				yield return hands[i];
			}
			IEnumerable<IItem> packItems = EquipmentController.EquippedPack.ListItems();
			for (int i = 0; i < packItems.Count(); i++) {
				yield return packItems.ElementAt(i);
			}
			IEnumerable<IItem> beltItems = EquipmentController.EquippedBelt.ListItems();
			for (int i = 0; i < beltItems.Count(); i++) {
				yield return beltItems.ElementAt(i);
			}
			for (int i = 0; i < EquipmentController.EquippedBelt.PouchesCount; i++) {
				IEnumerable<IItem> pouchItems = EquipmentController.EquippedBelt.GetPouch(i).ListItems();
				for (int j = 0; j < pouchItems.Count(); j++) {
					yield return pouchItems.ElementAt(j);
				}
			}
		}
		
		public bool IsFull() {
            foreach (CarryPos pos in Enum.GetValues(typeof(CarryPos))) {
                if (!(IsFull(pos))) return false;
            }
            return true;
		}
		
		public bool IsFull(CarryPos position) {
            switch (position) {
				case(CarryPos.POCKET):
					if (pockets.Count >= POCKETS_INVENTORY_SPACE) return true;
					return false;
				case(CarryPos.POUCH):
					for (int i = 0; i < EquipmentController.EquippedBelt.PouchesCount; i++) {
						if (!(EquipmentController.EquippedBelt.GetPouch(i).IsFull())) return false;
					}
					return true;
				case(CarryPos.PACK):
					return EquipmentController.EquippedPack.IsFull();
				case(CarryPos.BELT):
					return EquipmentController.EquippedBelt.IsFull();
				case(CarryPos.ONE_HAND):
				case(CarryPos.TWO_HAND):
					if (hands.Count >= HANDS_INVENTORY_SPACE) return true;
					return false;
				// Invalid position
				default:
					return true;
			}
		}
        #endregion
    }
}

