﻿using System;
using System.Collections.Generic;
using Rune.Util;

namespace Rune.Items
{
    /// <summary> 
    /// The "Item" abstract class defines basic behaviours for all items in Rune. 
    /// </summary> <remarks>
    /// The item ID and name should be unique as identifiers 
    /// </remarks>
    public abstract class Item : Named, IItem
    {
        #region Const Defines
        new protected static readonly string ID = Named.ID + "3";
		#endregion
		
        #region Properties
        /// <summary> The gold value of the item </summary>
        public int             Value         { get; protected set; }
        public QualityModifier Quality       { get; protected set; }

        #endregion 

        #region Constructors
        protected Item() {
            this.Value = SetValue();
            this.Quality = SetQuality();
        }
        #endregion

        #region Methods
        protected abstract int SetValue();

        protected abstract QualityModifier SetQuality();

		public virtual QualityModifier RollQuality() {
			return RollQuality(QualityModifier.BROKEN, QualityModifier.MYTHICAL);
		}
		
        /// <summary>
        /// Sets the items quality based on a normally distributed dice roll.
        /// </summary>
        /// <param name="lowestQuality"> Set the lowest possible quality to be rolled </param>
        /// <param name="highestQuality"> Set the highest possible quality to be rolled </param>
        public virtual QualityModifier RollQuality(QualityModifier lowestQuality, QualityModifier highestQuality) {
            this.Quality = (QualityModifier)(RuneChance.RollDiceNormal((int)(highestQuality - lowestQuality)) + (int)lowestQuality);
			return Quality;
		}

        /// <summary>
        /// 
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public override bool Equals(object obj) {
            if (obj.GetType() == this.GetType()) {
                IItem item = (IItem)obj;
                if (item.Quality == this.Quality) {
                    return true;
                }
            }
            return false;
        }

        public override int GetHashCode() {
            return base.GetHashCode();  
        }

        /// <summary>
        /// Item starts out as normal. A random number is rolled. Item has a chance to be every option,
        /// if no option is found it remains normal.
        /// </summary>
        private void setQuality() { 
            int qualityNumber = RuneChance.RollDiceNormal();
            int roll;
            QualityModifier itemQuality = QualityModifier.NORMAL;
            foreach (Items.QualityModifier quality in Enum.GetValues(typeof(Items.QualityModifier))) {
                roll = RuneChance.RollDiceNormal();
                // print roll?
                if (roll == qualityNumber) {
                    itemQuality = quality;
                    break;
                }
            }
            Quality = itemQuality;
        }
        #endregion
    }
}
