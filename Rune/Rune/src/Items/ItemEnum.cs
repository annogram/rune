﻿using System;
using Rune.Items;
using Rune.Items.Equipment.Weapons;

namespace Rune.Items
{
    /// <summary>
    /// Designates where in the inventory an item can be carried.
    /// </summary> <remarks>
    /// Higher value enums are considered specified if a lower enum
    /// is selected, excepting belt (belt cannot hold items from pack,
    /// aside from weapons and pouches; hands can hold items from belt).
    /// </remarks>
	// TODO this should probably be moved.  All enums might need some clean up in where they are, currently lazy implementation.
    public enum CarryPos
    {
        POCKET,
        POUCH,
        PACK,
        BELT,
        ONE_HAND,
        TWO_HAND,
		CANNOT_CARRY // Only for use by NullItems
    }

    /// <summary>
    /// 
    /// </summary>
    public enum QualityModifier
    {
        BROKEN,
        LOW,
        NORMAL,
        HIGH,
        MYTHICAL
    }

    /// <summary>
    /// Used for operations relating to the item enums.
    /// </summary>
    public static class ItemEnum
    {
        /// <summary>
        /// Determines whether this position can carry a given item.
        /// </summary>
        /// <param name="pos"> CarryPosition enum (to carry in) </param>
        /// <param name="item"> Item to be carried </param>
        /// <returns></returns>
        public static bool CanCarry(this CarryPos pos, ICarry item) {
            if( ((pos >= item.CarryPosition) && (pos != CarryPos.BELT))
                || (pos == CarryPos.BELT)
                || ((pos < CarryPos.BELT) && (item is Weapon)) ) {
                return true;
            }

            return false;
        }
    }
}
