﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

using Rune.Util;

namespace Rune.Items
{
    public abstract class ItemFactory
    {
        #region Properties
		// Empty
		#endregion
		
		#region Constructors
		// Empty
		#endregion
		
		#region Methods
        public static IItem GetItem(string id) {
            foreach (Type v in Assembly.GetExecutingAssembly().GetTypes()
                     .Where(w => w.IsSubclassOf(typeof(Item)) && !(w.IsAbstract))) 
            {
                Item item = Activator.CreateInstance(v) as Item;
                if (id.Equals(v.GetField("ID").GetValue(item))) return item;
            }
            return new NullItem();
        }
        #endregion
    }
}
