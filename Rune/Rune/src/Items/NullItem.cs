﻿using System;

namespace Rune.Items
{
    /// <summary>
    /// Defines a null item for use in place of NULL.  eg. an empty slot in the inventory.
    /// </summary>
    public class NullItem : Item, ICarry
    {
        #region Const Defines
        new protected static readonly string ID = Item.ID + "0";
		#endregion
		
		#region Properties
		public int      Weight   { get; protected set; }
		public CarryPos CarryPosition { get; protected set; }
        #endregion

        #region Constructors
        public NullItem() : base() {
			Weight = 0;
			CarryPosition = CarryPos.POCKET;
		}
        #endregion

        #region Methods
        public override string ToSimpleName() {
            return "[I'M A REAL ITEM]";
        }

        // Has no names
        protected override void InitializeCommon() { }

        // Has no names
        protected override void InitializeUnique() { }

        protected override int SetValue() {
            return 0;
        }

        protected override QualityModifier SetQuality() {
            return QualityModifier.BROKEN;
        }

        public override string GetDescription() {
            throw new NotImplementedException();
        }
        #endregion
    }
}
