﻿using System;
using System.Collections.Generic;
using System.Linq;

using Rune.Util;
using Rune.Maps.Tiles;
using Rune.Maps.Scenes;

namespace Rune.Maps
{
    public interface IMap
    {
        #region Properties
		// REVIEW Probs should merge facings somehow
		Direction   PlayerFacing   { get; }
		Direction   PlayerEWFacing { get; }
		Direction   PlayerNSFacing { get; }
        MapPosition PlayerPosition { get; }
		ITile	    PlayerTile     { get; }
		int		    Time   		   { get; }
		#endregion
		
		#region Methods

		string Go(Direction direction);

        string Look(Direction direction);

		void IncrementTime();
		#endregion
    }
}
