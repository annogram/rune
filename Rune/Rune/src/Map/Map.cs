﻿using System;
using System.Collections.Generic;
using System.Linq;

using Rune.Util;
using Rune.Maps.Tiles;
using Rune.Maps.Tiles.Coast;
using Rune.Maps.Tiles.Forest;
using Rune.Maps.Tiles.Marsh;
using Rune.Maps.Tiles.Mountain;
using Rune.Maps.Tiles.Plains;
using Rune.Maps.Tiles.Town;
using Rune.Maps.Tiles.Woodland;
using Rune.Maps.Scenes;

namespace Rune.Maps
{
    public class Map : IMap 
	{
        #region Const Defines
        public static readonly string ID = "1";
        private readonly MapPosition STARTING_POSITION = new MapPosition(1, 3);

        private const int MAP_SIZE_EASTWEST   = 5;
        private const int MAP_SIZE_NORTHSOUTH = 5;
        #endregion

        #region Properties
		public Direction   PlayerFacing   { get; protected set; }
		public Direction   PlayerEWFacing { get; protected set; }
		public Direction   PlayerNSFacing { get; protected set; }
        public MapPosition PlayerPosition { get; protected set; }
        public ITile       PlayerTile     { get; protected set; }
        public int         Time           { get; protected set; }
        #endregion

        #region Fields
        private static Map MapInstance = new Map();

        private string[,] TileMap;
        #endregion

        #region Constructors
        private Map() {
			Time = (int)TimeOfDay.TIME_DAWN;
            PlayerPosition = STARTING_POSITION;

            InitializeMap();
			PlayerTile = TileFactory.GetTile(TileMap[PlayerPosition.Y, PlayerPosition.X]);			
        }
        #endregion

        #region Methods
        public static Map GetMap() {
            return MapInstance;
        }

        public string Go(Direction direction) {
            string actionDescription = "";
            Tile nextTile = new NullTile();
            switch (direction) {
                case (Direction.NORTH):
					PlayerFacing = Direction.NORTH;
					PlayerNSFacing = Direction.NORTH;
					// If cannot enter tile from north, cannot leave by north either
					if (!(PlayerTile.CanEnterBy(direction))) {
						actionDescription = PlayerTile.GetGoDescription(direction);
						break;
					}
					// Check if next tile can be entered from north
                    PlayerPosition.Y++;
					nextTile = TileFactory.GetTile(TileMap[PlayerPosition.Y, PlayerPosition.X]);
                    actionDescription = nextTile.GetGoDescription(direction);
                    if (!(nextTile.CanEnterBy(direction))) {
                        PlayerPosition.Y--;
                    } else {
						PlayerTile = nextTile;
					}
                    break;
                case (Direction.SOUTH):
					PlayerFacing = Direction.SOUTH;
					PlayerNSFacing = Direction.SOUTH;
					if (!(PlayerTile.CanEnterBy(direction))) {
						actionDescription = PlayerTile.GetGoDescription(direction);
						break;
					}
                    PlayerPosition.Y--;
                    nextTile = TileFactory.GetTile(TileMap[PlayerPosition.Y, PlayerPosition.X]);
                    actionDescription = nextTile.GetGoDescription(direction);
                    if (!(nextTile.CanEnterBy(direction))) {
                        PlayerPosition.Y--;
                    } else {
						PlayerTile = nextTile;
					}
                    break;
                case (Direction.EAST):
					PlayerFacing = Direction.EAST;
					PlayerEWFacing = Direction.EAST;
					if (!(PlayerTile.CanEnterBy(direction))) {
						actionDescription = PlayerTile.GetGoDescription(direction);
						break;
					}
                    PlayerPosition.X++;
                    nextTile = TileFactory.GetTile(TileMap[PlayerPosition.Y, PlayerPosition.X]);
                    actionDescription = nextTile.GetGoDescription(direction);
                    if (!(nextTile.CanEnterBy(direction))) {
                        PlayerPosition.Y--;
                    } else {
						PlayerTile = nextTile;
					}
                    break;
                case (Direction.WEST):
					PlayerFacing = Direction.WEST;
					PlayerEWFacing = Direction.WEST;
					if (!(PlayerTile.CanEnterBy(direction))) {
						actionDescription = PlayerTile.GetGoDescription(direction);
						break;
					}
                    PlayerPosition.X--;
                    nextTile = TileFactory.GetTile(TileMap[PlayerPosition.Y, PlayerPosition.X]);
                    actionDescription = nextTile.GetGoDescription(direction);
                    if (!(nextTile.CanEnterBy(direction))) {
                        PlayerPosition.Y--;
                    } else {
						PlayerTile = nextTile;
					}
                    break;
                default:
                    // Will only hit this case with INVALID_DIR
                    break;
            }
            return (actionDescription + "\n" + PlayerTile.GetDescription()
                        + "\n" + PlayerTile.TileScene.GetDescription());
        }
		
		public string Look(Direction direction) {
            Tile nextTile = new NullTile();
            switch (direction) {
                case (Direction.NORTH):
					PlayerFacing = Direction.NORTH;
					PlayerNSFacing = Direction.NORTH;
					nextTile = TileFactory.GetTile(TileMap[PlayerPosition.Y + 1, PlayerPosition.X]);
                    return nextTile.GetDescription(direction);
                case (Direction.SOUTH):
					PlayerFacing = Direction.SOUTH;
					PlayerNSFacing = Direction.SOUTH;
					nextTile = TileFactory.GetTile(TileMap[PlayerPosition.Y - 1, PlayerPosition.X]);
                    return nextTile.GetDescription(direction);
                case (Direction.EAST):
					PlayerFacing = Direction.EAST;
					PlayerEWFacing = Direction.EAST;
					nextTile = TileFactory.GetTile(TileMap[PlayerPosition.Y, PlayerPosition.X + 1]);
                    return nextTile.GetDescription(direction);
                case (Direction.WEST):
					PlayerFacing = Direction.WEST;
					PlayerEWFacing = Direction.WEST;
					nextTile = TileFactory.GetTile(TileMap[PlayerPosition.Y, PlayerPosition.X - 1]);
                    return nextTile.GetDescription(direction);
                default:
                    // Will only hit this case with INVALID_DIR
                    return "";
            }
		}

        public void IncrementTime() {
            if (Time < (int)TimeOfDay.TIME_MIDNIGHT) {
                Time++;
            } else {
                Time = 0;
            }
        }
        #endregion

        #region Map Creation
        // The map should be bordered by NullTiles to simplify passage prevention into invalid areas (ie. out of array bounds)
        private void InitializeMap() {
            // Initializes TileMap with tiles
            TileMap = new string[MAP_SIZE_EASTWEST, MAP_SIZE_NORTHSOUTH]
            { // Central map surrounded by NullTile border
					{ NullTownBuildingsTile.ID,		NullTownBuildingsTile.ID,	NullTownAlleysTile.ID,		NullCoastRocksTile.ID,		NullCoastOceanTile.ID },
													// Map top left											// Map top right
				{ NullTownAlleysTile.ID,			TownSmithyStreetTile.ID,	TownSquareTile.ID,			CoastSandyTile.ID,				NullCoastOceanTile.ID },
				{ NullMountainCliffTile.ID,			WoodlandHillTile.ID,		PlainsCrossroadsTile.ID,	MarshRoadTile.ID,				NullMarshDeepTile.ID  },
				{ NullMountainCliffTile.ID,			MountainSidePathTile.ID,	ForestClearingTile.ID,		ForestBrookTile.ID,				NullMarshDeepTile.ID  },
													// Map bottom left										// Map bottom right
					{ NullMountainCliffTile.ID,		NullMountainRavineTile.ID,	NullForestDenseTile.ID,	    NullForestDenseTile.ID,		NullForestDenseTile.ID }

			
			
			};
        }
		#endregion
    }
}
