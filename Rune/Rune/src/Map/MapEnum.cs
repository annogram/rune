﻿using System;
using System.Collections.Generic;
using System.Linq;

using Rune.Util;

namespace Rune.Maps
{
    public class MapPosition
    {
        public int X { get; set; }
        public int Y { get; set; }
		
		public MapPosition() {
			this.X = 0;
			this.Y = 0;
		}
		
		public MapPosition(int x, int y) {
			this.X = x;
			this.Y = y;
		}
    }
	
	public enum Direction 
	{
		INVALID_DIR = -1,
		NORTH,
		SOUTH,
		EAST,
		WEST,
		NUM_DIRECTIONS
	}
	
	public enum TimeOfDay
	{
		TIME_MIDNIGHT = 24,
		TIME_DAWN = 6,
		TIME_MIDDAY = 12,
		TIME_DUSK = 18,
	}

    public static class MapEnum
    {
		#region Methods
		public static Direction ToDirection(this string str) {
			str.Replace(@"\s+", "");
			str.Replace("-", "");
			str = str.ToUpper();
			foreach (Direction dir in Enum.GetValues(typeof(Direction))) {
                if (str.Equals(dir.ToString())) return dir;
            }
            return Direction.INVALID_DIR;
		}
		#endregion
    }
}
