﻿using System;
using System.Collections.Generic;
using System.Linq;

using Rune.Util;
using Rune.Items;

namespace Rune.Maps.Scenes
{
	/// <summary>
	/// Scene to be used when nothing is happening in a general tile.  Can be used in all regions.
	/// </summary>
    public class EmptyScene : Scene
    {
        #region Const Defines
        new public static readonly string ID = Scene.ID + "0";
		#endregion
		
        #region Properties
		// Empty
		#endregion
		
		#region Constructors
		// Empty
		#endregion
		
		#region Methods
		public override string GetDescription() {
			// Nothing is happening in an empty scene
			return "There appears to be nothing of interest here.";
		}
        #endregion
    }
}
