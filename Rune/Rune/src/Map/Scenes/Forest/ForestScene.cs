﻿using System;
using System.Collections.Generic;
using System.Linq;

using Rune.Util;
using Rune.Items;
using Rune.Items.Equipment.Weapons;

namespace Rune.Maps.Scenes.Forest
{
    public abstract class ForestScene : Scene
    {
        #region Const Defines
		new protected static readonly string ID = Scene.ID + "1";
		#endregion
		
        #region Properties
		// Empty
		#endregion
		
		#region Fields
		// Empty
        #endregion

        #region Constructors
        // Empty
        #endregion

        #region Methods
        // Empty
        #endregion
    }
}
