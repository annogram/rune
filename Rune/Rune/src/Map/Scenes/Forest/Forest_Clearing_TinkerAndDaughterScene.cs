﻿using System;
using System.Collections.Generic;
using System.Linq;

using Rune.Util;
using Rune.Items;
using Rune.Items.Equipment.Weapons;

namespace Rune.Maps.Scenes.Forest
{
	// REVIEW we really need a good naming convention for scenes, that indicate which tiles/regions the scene is appropriate for.
	/// <summary>
	/// A scene to be used in forest clearings and groves.  Consists of an old travelling 'tinker' (random merchant)
	/// and a girl young enough to be his daughter (mid-teens).
	/// </summary>
    public class Forest_Clearing_TinkerAndDaughterScene : ForestScene
    {
        #region Const Defines
        new public static readonly string ID = ForestScene.ID + "1";
		// Sets how many items are in the scene.
		private const int NUM_ITEMS = 1;
		// You could have a list of potential ItemTypes to create as a private const here
		#endregion
		
        #region Properties
		// Empty
		#endregion

        #region Constructors
        // Empty
        #endregion

        #region Methods
        protected override IItem[] SetItems() {
            // You could roll a dice to select an ItemType to add to the scene here in a for loop
            IItem[] items = new IItem[NUM_ITEMS] { WeaponFactory.GetWeapon(Rune.Items.Equipment.Weapons.Melees.Sword.ID) };
			return items;
		}
		
		public override string GetDescription() {
			string sceneText = "";
			int roll = RuneChance.RollDiceUniform(2);
			switch(roll) {
				case(0):
					sceneText = "As you look around the clearing you see an old man and a girl beneath the trees, a crackling fire between them.";
                    break;
                case (1):
					sceneText = "Moving further into the clearing, you can see a man and a girl have made camp, having selected a spot where the sun is shining through the break in the trees.";
                    break;
                case(2):
					sceneText = "At the clearings centre you observe a man and young girl have made camp next to the large old tree; camp gear is strewn around them.";
                    break;
                default:
                    goto case (2);
			}
            // Iterate through original items, index indicates where in the scene the item was placed
			for (int i = 0; i < NUM_ITEMS; i++) {
				// TODO check all NullItemTypes inherit nullitem, they should
				if(!(Items[i] is NullItem)) {
					switch(i) {
						// Number of cases should be equal to NUM_ITEMS
						case(0):
                            if (!(Items[i] is NullItem)) {
                                IItem item = Items[i] as IItem;
                                sceneText = sceneText + "/n" + String.Format("You see {0} on the ground next to the man.", item.ToSimpleName(GrammarPoints.SINGULAR));
                            }
                            break;
                        default:
                            // should not hit here, do nothing
                            break;
					}
				}
			}
            // Iterate through any new items (items dropped into the scene)
            for (int i = NUM_ITEMS; i < Items.Count; i++) {
                IItem item = Items[i] as IItem;
                sceneText = sceneText + "\n" + String.Format("There is an {0} on the ground.", item.ToSimpleName(GrammarPoints.SINGULAR));
            }
			return sceneText;
		}
        #endregion
    }
}
