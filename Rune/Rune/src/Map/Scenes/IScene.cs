﻿using System;
using System.Collections;
using System.Linq;

using Rune.Util;
using Rune.Items;

namespace Rune.Maps.Scenes
{
    public interface IScene
    {
        #region Properties
		ArrayList Items { get; }
        // Char list here
        #endregion

        #region Methods

        string GetDescription();

		IScene NextScene();
		
		IItem FindItem(string name);
		
		bool StoreItem(IItem item);
		
		IItem TakeItem(string name);
		
		IItem TakeItem(IItem item);

		// FindChar() method
				
		#endregion
    }
}
