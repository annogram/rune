﻿using System;
using System.Collections.Generic;
using System.Linq;

using Rune.Util;

namespace Rune.Maps.Scenes.Plains
{
    public abstract class PlainsScene : Scene
    {
        #region Const Defines
        new protected static readonly string ID = Scene.ID + "2";
        #endregion

        #region Properties
        // Empty
        #endregion

        #region Fields
        // Empty
        #endregion

        #region Constructors
        // Empty
        #endregion

        #region Methods
        // Empty
        #endregion
    }
}
