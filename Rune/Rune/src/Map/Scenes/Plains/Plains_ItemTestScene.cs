﻿using System;
using System.Collections.Generic;
using System.Linq;

using Rune.Util;
using Rune.Items;
using Rune.Items.Equipment.Weapons;
using Rune.Items.Equipment.Weapons.Melees;
using Rune.Items.Equipment.Apparel.Armors;
using Rune.Items.Equipment.Apparel.Armors.Helms;
using Rune.Items.Equipment.Apparel.Armors.Cuirasses;
using Rune.Items.Equipment.Apparel.Clothes;
using Rune.Items.Equipment.Apparel.Clothes.Boots;
using Rune.Items.Equipment.Weapons.Melees.Hilts;
using Rune.Items.Equipment.Accessories.Gear.Packs;
using Rune.Items.Equipment.Accessories.Gear.Pouches;
using Rune.Items.Equipment.Accessories.Gear.Belts;

namespace Rune.Maps.Scenes.Plains
{
	/// <summary>
	/// </summary>
    public class Plains_ItemTestScene : PlainsScene
    {
        #region Const Defines
        new public static readonly string ID = PlainsScene.ID + "1";
		// Sets how many items are in the scene.
		private const int NUM_ITEMS = 1;
        private readonly string[] ITEM_LIST = new string[7] { Sword.ID, DrawstringPouch.ID, SalletHelm.ID, Jackboot.ID, HalfPlate.ID, SimpleHilt.ID, BasketHilt.ID };
        #endregion 

        #region Properties
        // Empty
        #endregion

        #region Fields
		// Empty
        #endregion

        #region Constructors
        // Empty
        #endregion

        #region Methods
        protected override IItem[] SetItems() {
            // You could roll a dice to select an ItemType to add to the scene here in a for loop
			// NUM_ITEMS not used as this is testing "on ground" part of print
            IItem[] items = new IItem[12];
			for (int i = 0; i < 12; i++) {
				int roll = RuneChance.RollDiceExponential(ITEM_LIST.Length - 1);
				items[i] = ItemFactory.GetItem(ITEM_LIST[roll]);
			}
			return items;
		}
		
		public override string GetDescription() {
			string sceneText = "You are in a plain with shit all over the place";
            // Iterate through original items, index indicates where in the scene the item was placed
			for (int i = 0; i < NUM_ITEMS; i++) {
				// TODO check all NullItemTypes inherit nullitem, they should
				if(!(Items[i] is NullItem)) {
					switch(i) {
						// Number of cases should be equal to NUM_ITEMS
						case(0):
                            if (!(Items[i] is NullItem)) {
                                IItem item = Items[i] as IItem;
                                sceneText = sceneText + "/n" + String.Format("You see a {0} leaning against a well; a note is appended to it, reading 'Shit be dangerous.  Take this.'.", item.ToSimpleName(GrammarPoints.SINGULAR));
                            }
                            break;
                        default:
                            // should not hit here, do nothing
                            break;
					}
				}
			}
            // Iterate through any new items (items dropped into the scene)
            for (int i = NUM_ITEMS; i < Items.Count; i++) {
                IItem item = Items[i] as IItem;
                sceneText = sceneText + "\n" + String.Format("There is an {0} on the ground.", item.ToSimpleName(GrammarPoints.SINGULAR));
            }
			return sceneText;
		}
        #endregion
    }
}
