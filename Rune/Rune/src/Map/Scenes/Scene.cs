﻿using System;
using System.Collections;
using System.Linq;

using Rune.Util;
using Rune.Items;

namespace Rune.Maps.Scenes
{
    public abstract class Scene : IScene
    {
		// Battle scenes will be made as a new abstract class with a 2d array(?) detailing a mini "battle map"
		#region Const Defines
		protected static readonly string ID = Rune.Maps.Map.ID + "2";
		#endregion
				
        #region Properties
		public ArrayList Items { get; protected set; }
        // Char list here
        #endregion

        #region Fields
		private IItem NullItemInstance = new NullItem();
		#endregion
		
		#region Constructors
		protected Scene() : base() {
			this.Items = new ArrayList(SetItems());
		}
        #endregion

        #region Methods
        public abstract string GetDescription();

		protected virtual IItem[] SetItems() {
			return new IItem[] {};
		}

		public virtual IScene NextScene() {
			return this;
		}
		
		public virtual IItem FindItem(string name) {
			for (int i = 0; i < Items.Count; i++) {
                IItem item = Items[i] as IItem;
				if (item.HasName(name)) {
					return item;
				}
			}
            return NullItemInstance;
		}
		
		public bool StoreItem(IItem item) {
			Items.Add(item);
            return true;
		}
		
		public virtual IItem TakeItem(string name) {
			for (int i = 0; i < Items.Count; i++) {
                IItem item = Items[i] as IItem;
				if (item.HasName(name)) {
					IItem takenItem;
					takenItem = item;
                    Items[i] = NullItemInstance; ;
					return takenItem;
				}
			}
			return NullItemInstance;
		}
		
		public virtual IItem TakeItem(IItem item) {
			for (int i = 0; i < Items.Count; i++) {
				if (Items[i].Equals(item)) {
					IItem takenItem;
					takenItem = Items[i] as IItem;
					Items[i] = NullItemInstance;
					return takenItem;
				}
			}
			return NullItemInstance;
		}	
		#endregion
    }
}
