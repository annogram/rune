﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

using Rune.Util;

namespace Rune.Maps.Scenes
{
    public abstract class SceneFactory
    {
        #region Properties
		// Empty
		#endregion
		
		#region Constructors
		// Empty
		#endregion
		
		#region Methods
		public static Scene GetScene(string id) {
            foreach (Type v in Assembly.GetExecutingAssembly().GetTypes()
                     .Where(w => w.IsSubclassOf(typeof(Scene)) && !(w.IsAbstract))) 
            {
                Scene scene = Activator.CreateInstance(v) as Scene;
                if (id.Equals(v.GetField("ID").GetValue(scene))) return scene;
            }
            return new EmptyScene();
        }	
		#endregion
    }
}
