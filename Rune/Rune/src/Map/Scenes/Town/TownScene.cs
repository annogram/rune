﻿using System;
using System.Collections.Generic;
using System.Linq;

using Rune.Util;

namespace Rune.Maps.Scenes.Town
{
    public abstract class TownScene : Scene
    {
        #region Const Defines
		new protected static readonly string ID = Scene.ID + "3";
		#endregion
		
        #region Properties
		// Empty
		#endregion
		
		#region Fields
		// Empty
        #endregion

        #region Constructors
        // Empty
        #endregion

        #region Methods
        // Empty
        #endregion
    }
}
