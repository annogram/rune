﻿using System;
using System.Collections.Generic;
using System.Linq;

using Rune.Util;
using Rune.Items;
using Rune.Items.Equipment.Weapons;
using Rune.Items.Equipment.Weapons.Melees;

namespace Rune.Maps.Scenes.Town
{
	/// <summary>
	/// </summary>
    public class Town_Square_TakeThisScene : TownScene
    {
        #region Const Defines
		new public static readonly string ID = TownScene.ID + "1";
		// Sets how many items are in the scene.
		private const int NUM_ITEMS = 1;
		// You could have a list of potential ItemTypes to create as a private const here
		#endregion
		
        #region Properties
		// Empty
		#endregion

        #region Constructors
        // Empty
        #endregion

        #region Methods
        protected override IItem[] SetItems() {
            // You could roll a dice to select an ItemType to add to the scene here in a for loop
			// REVIEW Referencing the actual class sword reduces ability to alter code, detrimental to purpose of factory pattern
            IItem[] items = new IItem[NUM_ITEMS] { WeaponFactory.GetWeapon(Sword.ID) };
			return items;
		}
		
		public override string GetDescription() {
			string sceneText = "You are in a bustling town square.";
            // Iterate through original items, index indicates where in the scene the item was placed
			for (int i = 0; i < NUM_ITEMS; i++) {
				// TODO check all NullItemTypes inherit nullitem, they should
				if(!(Items[i] is NullItem)) {
					switch(i) {
						// Number of cases should be equal to NUM_ITEMS
						case(0):
                            if (!(Items[i] is NullItem)) {
                                IItem item = Items[i] as IItem;
                                sceneText = sceneText + "/n" + String.Format("You see a {0} leaning against a well; a note is appended to it, reading 'Shit be dangerous.  Take this.'.", item.ToSimpleName(GrammarPoints.SINGULAR));
                            }
                            break;
                        default:
                            // should not hit here, do nothing
                            break;
					}
				}
			}
            // Iterate through any new items (items dropped into the scene)
            for (int i = NUM_ITEMS; i < Items.Count; i++) {
                IItem item = Items[i] as IItem;
                sceneText = sceneText + "\n" + String.Format("There is an {0} on the ground.", item.ToSimpleName(GrammarPoints.SINGULAR));
            }
			return sceneText;
		}
        #endregion
    }
}
