﻿using System;
using System.Collections.Generic;
using System.Linq;

using Rune.Util;
using Rune.Maps.Scenes;

namespace Rune.Maps.Tiles.Coast
{
	
    public class CoastSandyTile : CoastTile
    {
        #region Const Defines
        new public static readonly string ID = CoastTile.ID + "1";
        private readonly IList<string> SCENE_LIST = new List<string> { EmptyScene.ID };
        #endregion
		
        #region Properties
		// Empty
        #endregion

        #region Constructors
        // Empty
        #endregion

        #region Methods
		protected override IScene SetScene() {
			// Scene list should be ordered appropriately so that the correct scenes appear more often.
			int roll = RuneChance.RollDiceNormal(SCENE_LIST.Count() - 1);
			return SceneFactory.GetScene(SCENE_LIST.ElementAt(roll));
		}
		
		public override string GetDescription() {
			return "You are standing on a small, thin beach that runs along the coast.";
		}
		
		public override string GetGoDescription(Direction direction) {
			return String.Format("As you walk on the beach, you can feel the shifting of the sand under you.", direction.ToString());
		}
		
		public override string GetDescription(Direction direction) {
			return String.Format("You can see a small spit of sand {0} of you.", direction.ToString());
		}
        #endregion
    }
}
