﻿using System;
using System.Collections.Generic;
using System.Linq;

using Rune.Util;
using Rune.Maps.Scenes;

namespace Rune.Maps.Tiles.Coast
{
    public class NullCoastOceanTile : NullTile
    {
        #region Const Defines
        new public static readonly string ID = NullTile.ID + CoastTile.ID + "1";
        #endregion

        #region Properties
        // Empty
        #endregion

        #region Fields
        // Empty
        #endregion

        #region Constructors
        // Empty
        #endregion

        #region Methods
        protected override IScene SetScene() {
			return new EmptyScene();
		}
		
		public override string GetGoDescription(Direction direction) {
			return String.Format("As you try to move {0}, the waves roll past your ankles to your knees... waist... chest... you begin to hurry back to dry land, lest you drown for no good reason.", direction.ToString());
		}
		
        public override string GetDescription() {
			return "[I'M AN IMPASSABLE COAST. WHOT ARE YOU DOING HERE?]";
		}
		
		public override string GetDescription(Direction direction) {
			return String.Format("The coast to the {0} is not coast at all, but ocean.  It seems apparent that {0} is not a potential direction of travel.", direction.ToString());
		}
		#endregion
    }
}
