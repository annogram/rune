﻿using System;
using System.Collections.Generic;
using System.Linq;

using Rune.Util;
using Rune.Maps.Scenes;

namespace Rune.Maps.Tiles.Coast
{
    public class NullCoastRocksTile : NullTile
    {
        #region Const Defines
        new public static readonly string ID = NullTile.ID + CoastTile.ID + "2";
        #endregion

        #region Properties
        // Empty
        #endregion

        #region Fields
        // Empty
        #endregion

        #region Constructors
        // Empty
        #endregion

        #region Methods

        protected override IScene SetScene() {
			throw new NotImplementedException();
		}
		
		public override string GetGoDescription(Direction direction) {
			return String.Format("While you try to climb the {0}ern rocks, you are unable to make any real progress.", direction.ToString());
		}
		
        public override string GetDescription() {
			return "[I'M AN IMPASSABLE COAST. WHOT ARE YOU DOING HERE?]";
		}
		
		public override string GetDescription(Direction direction) {
			return String.Format("To your {0} are craggy coastal rocks, waves constantly crashing over them.", direction.ToString());
		}
		#endregion
    }
}
