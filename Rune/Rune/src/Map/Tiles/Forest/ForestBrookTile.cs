﻿using System;
using System.Collections.Generic;
using System.Linq;

using Rune.Util;
using Rune.Maps.Scenes;
using Rune.Maps.Scenes.Forest;

namespace Rune.Maps.Tiles.Forest
{
	
    public class ForestBrookTile : ForestTile
    {
        #region Const Defines
        new public static readonly string ID = ForestTile.ID + "1";
        private readonly IList<string> SCENE_LIST = new List<string> { EmptyScene.ID };
        #endregion
		
        #region Properties
		// Empty
        #endregion

        #region Constructors
        // Empty
        #endregion

        #region Methods
		
		protected override IScene SetScene() {
			// Scene list should be ordered appropriately so that the correct scenes appear more often.
			int roll = RuneChance.RollDiceNormal(SCENE_LIST.Count() - 1);
			return SceneFactory.GetScene(SCENE_LIST.ElementAt(roll));
		}
		
		public override string GetDescription() {
			// Creek could be a water resource that allows actions such as fishing/bathing/taking water.
			return "You are in the middle of a forest.  A small creek is flowing in a small riverbed in front of you.";
		}
		
		public override string GetGoDescription(Direction direction) {
			// Direction ignored so brook/river/etc. tiles can be placed in lines to create large rivers without continuity errors
			return "As you make your way through the trees, you hear the trickling of running water, only to find a small brook running along the forest floor in front of you.";
		}
		
		public override string GetDescription(Direction direction) {
			return String.Format("You cannot see much through the trees to the {0}, but can hear a noise, alike to flowing water...", direction.ToString());
		}
        #endregion
    }
}
