﻿using System;
using System.Collections.Generic;
using System.Linq;

using Rune.Util;
using Rune.Maps.Scenes;
using Rune.Maps.Scenes.Forest;

namespace Rune.Maps.Tiles.Forest
{
	
    public class ForestClearingTile : ForestTile
    {
        #region Const Defines
        // A list of enums titling different scenes. A scene is randomly selected upon entering the tile and created with
        // a SceneFactory using the appropriate enum.
        new public static readonly string ID = ForestTile.ID + "1";
        private readonly IList<string> SCENE_LIST = new List<string> { EmptyScene.ID, Forest_Clearing_TinkerAndDaughterScene.ID };
        #endregion
		
        #region Properties
		// Empty
        #endregion

        #region Constructors
        // Empty
        #endregion

        #region Methods
		
		protected override IScene SetScene() {
			// Scene list should be ordered appropriately so that the correct scenes appear more often.
			int roll = RuneChance.RollDiceNormal(SCENE_LIST.Count() - 1);
			return SceneFactory.GetScene(SCENE_LIST.ElementAt(roll));
		}
		
		public override string GetDescription() {
			return "You are in a small forest clearing, a break in the forest canopy allowing sunlight to shine down.  A single large tree is apparent at the clearing's centre.";
		}
		
		public override string GetGoDescription(Direction direction) {
			return "As you push your way through the underbrush, you find yourself emerging into a some sort of clearing.";
		}

        public override string GetDescription(Direction direction) {
            // Tile is 'circular', direction ignored
            return String.Format("There seems to be a small break in the trees ahead to the {0}, as you perceive sunlight filtering through the tree trunks.", direction.ToString());
        }
        #endregion
    }
}
