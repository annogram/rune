﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Rune.Util;

namespace Rune.Maps.Tiles.Forest
{
    public abstract class ForestTile : Tile
    {
        #region Const Defines
        new public static readonly string ID = Tile.ID + "2";
        #endregion
    }
}
