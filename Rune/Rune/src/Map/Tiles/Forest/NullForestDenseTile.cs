﻿using System;
using System.Collections.Generic;
using System.Linq;

using Rune.Util;
using Rune.Maps.Scenes;

namespace Rune.Maps.Tiles.Forest
{
    public class NullForestDenseTile : NullTile
    {
        #region Const Defines
        new public static readonly string ID = NullTile.ID + ForestTile.ID + "1";
        #endregion

        #region Properties
        // Empty
        #endregion

        #region Fields
        // Empty
        #endregion

        #region Constructors
        // Empty
        #endregion

        #region Methods

        protected override IScene SetScene() {
			throw new NotImplementedException();
		}
		
		public override string GetGoDescription(Direction direction) {
			return String.Format("You try to go {0}, but as you force your way through the forest's dense interior, you burst out of the brush... back at where you had started.", direction.ToString());
		}
		
        public override string GetDescription() {
			return "[I'M AN IMPASSABLE FOREST. WHOT ARE YOU DOING HERE?]";
		}
		
		public override string GetDescription(Direction direction) {
			return String.Format("The forest to the the {0} looks impossibly dense and dark... it looks unlikely that you would be able to force your way further that way.", direction.ToString());
		}
        #endregion
    }
}
