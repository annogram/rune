﻿using System;
using System.Collections.Generic;
using System.Linq;

using Rune.Util;
using Rune.Maps.Scenes;

namespace Rune.Maps.Tiles
{
    public interface ITile
    {
        #region Properties
		IScene 		TileScene  { get; }
        #endregion

        #region Methods
        string GetDescription();

		/// <summary>
		/// Describes what the player sees as they enter the tile.
		/// </summary>
		string GetGoDescription(Direction direction);

		/// <summary>
		/// Describes what the player sees when looking at the given side of the tile (eg. NORTH is from north of the tile, looking south).
		/// </summary>
		string GetDescription(Direction direction);
		
		bool CanEnterBy(Direction direction);
		#endregion
    }
}
