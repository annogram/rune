﻿using System;
using System.Collections.Generic;
using System.Linq;

using Rune.Util;
using Rune.Maps.Scenes;

namespace Rune.Maps.Tiles.Marsh
{
	
    public class MarshRoadTile : MarshTile
    {
        #region Const Defines
        new public static readonly string ID = MarshTile.ID + "1";
        private readonly IList<string> SCENE_LIST = new List<string> { EmptyScene.ID };
        #endregion
		
        #region Properties
		// Empty
        #endregion

        #region Constructors
        // Empty
        #endregion

        #region Methods
		
		protected override IScene SetScene() {
			// Scene list should be ordered appropriately so that the correct scenes appear more often.
			int roll = RuneChance.RollDiceNormal(SCENE_LIST.Count() - 1);
			return SceneFactory.GetScene(SCENE_LIST.ElementAt(roll));
		}
		
		public override string GetDescription() {
			return "You are standing on a small cobbled road - or a large path - that winds through the marsh.";
		}
		
		public override string GetGoDescription(Direction direction) {
			return String.Format("You walk {0}, the solid stones of the winding road beneath your feet.", direction.ToString());
		}
		
		public override string GetDescription(Direction direction) {
			// "the marsh" only really makes sense if the player is already in the marsh.  Maybe have "transition" tiles
			// designed to be placed along the borders between regions?
			return String.Format("To the {0} is a small road winding through the marsh.", direction.ToString());
		}
        #endregion
    }
}
