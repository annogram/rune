﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Rune.Util;

namespace Rune.Maps.Tiles.Marsh
{
    public abstract class MarshTile : Tile
    {
        #region Const Defines
        new public static readonly string ID = Tile.ID + "3";
        #endregion
    }
}
