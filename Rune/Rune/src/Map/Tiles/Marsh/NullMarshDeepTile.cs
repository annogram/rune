﻿using System;
using System.Collections.Generic;
using System.Linq;

using Rune.Util;
using Rune.Maps.Scenes;

namespace Rune.Maps.Tiles.Marsh
{
    public class NullMarshDeepTile : NullTile
    {
        #region Const Defines
        new public static readonly string ID = NullTile.ID + MarshTile.ID + "1";
        #endregion

        #region Properties
        // Empty
        #endregion

        #region Fields
        // Empty
        #endregion

        #region Constructors
        // Empty
        #endregion

        #region Methods

        protected override IScene SetScene() {
			// Should not be called
			return new EmptyScene();
		}
		
		public override string GetGoDescription(Direction direction) {
			return String.Format("You try to go {0}, but as you take a few steps, you find the waters of the marsh less solid than you would have thought, quickly giving away beneath you.  You hurry back to solid ground.", direction.ToString());
		}
		
        public override string GetDescription() {
			return "[I'M AN IMPASSABLE MARSH. WHOT ARE YOU DOING HERE?]";
		}
		
		public override string GetDescription(Direction direction) {
			return String.Format("There seems to be a section of deep marshy water to the {0}.", direction.ToString());
		}
		#endregion
    }
}
