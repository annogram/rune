﻿using System;
using System.Collections.Generic;
using System.Linq;

using Rune.Util;
using Rune.Maps.Scenes;

namespace Rune.Maps.Tiles.Mountain
{
	
    public class MountainSidePathTile : MountainTile
    {
        #region Const Defines
        new public static readonly string ID = MountainTile.ID + "1";
        private readonly IList<string> SCENE_LIST = new List<string> { EmptyScene.ID };
        #endregion
		
        #region Properties
		// Empty
        #endregion

        #region Constructors
        // Empty
        #endregion

        #region Methods
		
		protected override IScene SetScene() {
			// Scene list should be ordered appropriately so that the correct scenes appear more often.
			int roll = RuneChance.RollDiceNormal(SCENE_LIST.Count() - 1);
			return SceneFactory.GetScene(SCENE_LIST.ElementAt(roll));
		}
		
		public override string GetDescription() {
			return "You stand on a small dirt mountain path, with a long fall on one side and a steep slope on the other.";
		}
		
		public override string GetGoDescription(Direction direction) {
			if (direction == Direction.EAST || direction == Direction.WEST) {
				return String.Format("While you attempt to go {0}, the steep cliffs of the mountain rebuke you.", direction.ToString());
			}
			return String.Format("You walk along a path {0} following the mountainside.", direction.ToString());
		}
		
		public override string GetDescription(Direction direction) {
			return String.Format("You can see a small path up the mountain to the {0}.", direction.ToString());
		}
		
		public override bool CanEnterBy(Direction direction) {
			if (direction == Direction.EAST || direction == Direction.WEST) {
				return false;
			}
			return true;
		}
        #endregion
    }
}
