﻿using System;
using System.Collections.Generic;
using System.Linq;

using Rune.Util;
using Rune.Maps.Scenes;

namespace Rune.Maps.Tiles.Mountain
{
    public class NullMountainRavineTile : NullTile
    {
        #region Const Defines
        new public static readonly string ID = NullTile.ID + MountainTile.ID + "1";
        #endregion

        #region Properties
        // Empty
        #endregion

        #region Fields
        // Empty
        #endregion

        #region Constructors
        // Empty
        #endregion

        #region Methods

        protected override IScene SetScene() {
			return new EmptyScene();
		}
		
		public override string GetGoDescription(Direction direction) {
			return String.Format("You try to go {0}, but the steep downward slope to a bottomless ravine makes you rethink your decision - it would be awful hard to stop going in that direction.", direction.ToString());
		}
		
        public override string GetDescription() {
			return "[I'M AN IMPASSABLE MOUNTAIN. WHOT ARE YOU DOING HERE?]";
		}
		
		public override string GetDescription(Direction direction) {
			return String.Format("A steep slope leads down to a deep ravine to the {0}.", direction.ToString());
		}
		#endregion
    }
}
