﻿using System;
using System.Collections.Generic;
using System.Linq;

using Rune.Util;
using Rune.Maps.Scenes;

namespace Rune.Maps.Tiles
{
	
    public class NullTile : Tile
    {
        #region Const Defines
        new protected static readonly string ID = Tile.ID + "0";
        #endregion

        #region Properties
        // Empty
        #endregion

        #region Constructors
        // Empty
        #endregion

        #region Methods
        public override bool CanEnterBy(Direction direction) {
			return false;
		}

        public override string GetDescription() {
            return "[Well, you buggered the map.]";
        }

        public override string GetDescription(Direction direction) {
            return String.Format("There appears to be nothing of interest to the {0}.", direction.ToString());
        }

        public override string GetGoDescription(Direction direction) {
            return "[It doesn't go well.]";
        }

        protected override IScene SetScene() {
            return new EmptyScene();
        }
        #endregion
    }
}
