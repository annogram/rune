﻿using System;
using System.Collections.Generic;
using System.Linq;

using Rune.Util;
using Rune.Maps.Scenes;
using Rune.Maps.Scenes.Plains;

namespace Rune.Maps.Tiles.Plains
{
	
    public class PlainsCrossroadsTile : PlainsTile
    {
        #region Const Defines
        new public static readonly string ID = PlainsTile.ID + "1";
        private readonly IList<string> SCENE_LIST = new List<string> { Plains_ItemTestScene.ID };
        #endregion
		
        #region Properties
		// Empty
        #endregion

        #region Constructors
        // Empty
        #endregion

        #region Methods
		
		protected override IScene SetScene() {
			// Scene list should be ordered appropriately so that the correct scenes appear more often.
			int roll = RuneChance.RollDiceNormal(SCENE_LIST.Count() - 1);
			return SceneFactory.GetScene(SCENE_LIST.ElementAt(roll));
		}
		
		public override string GetDescription() {
			return "You are in the middle of a crossroad, with flat plains stretching out around you.";
		}
		
		public override string GetGoDescription(Direction direction) {
			return String.Format("As you walk along the large road, you come to a crossroad in the central plains.", direction.ToString());
		}
		
		public override string GetDescription(Direction direction) {
			return String.Format("You can see a crossroad in the middle of the plains to the {0}.", direction.ToString());
		}
        #endregion
    }
}
