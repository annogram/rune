﻿using System;
using System.Collections.Generic;
using System.Linq;

using Rune.Util;
using Rune.Maps.Scenes;

namespace Rune.Maps.Tiles
{
	
    public abstract class Tile : ITile
    {
        #region Const Defines
        protected static readonly string ID = Rune.Maps.Map.ID + "1";
        #endregion

        #region Properties
        // Should have some sort of "Resource" list for interactable resources like rivers
        // Resources would be tile based and as such should be stored in tile
		public IScene     TileScene  { get; protected set; }
		#endregion
		
		#region Fields
		protected bool isFirstVisit = true;
        #endregion

        #region Constructors
        protected Tile() {
            this.TileScene = SetScene();
		}
        #endregion

        #region Methods
		
		protected abstract IScene SetScene();

        public abstract string GetDescription();
		
		public abstract string GetGoDescription(Direction direction);
		
		public abstract string GetDescription(Direction direction);
		
		public virtual bool CanEnterBy(Direction direction) {
			return true;
		}
		#endregion
    }
}
