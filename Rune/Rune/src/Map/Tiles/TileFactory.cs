﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

using Rune.Util;

namespace Rune.Maps.Tiles
{
    public abstract class TileFactory
    {
        #region Properties
		// Empty
		#endregion
		
		#region Constructors
		// Empty
		#endregion
		
		#region Methods
        public static Tile GetTile(string id) {
            foreach (Type v in Assembly.GetExecutingAssembly().GetTypes()
                     .Where(w => w.IsSubclassOf(typeof(Tile)) && !(w.IsAbstract))) 
            {
                Tile tile = Activator.CreateInstance(v) as Tile;
                if (id.Equals(v.GetField("ID").GetValue(tile))) return tile;
            }
            return new NullTile();
        }
        #endregion
    }
}
