﻿using System;
using System.Collections.Generic;
using System.Linq;

using Rune.Util;
using Rune.Maps.Scenes;

namespace Rune.Maps.Tiles.Town
{
    public class NullTownAlleysTile : NullTile
    {
        #region Const Defines
        new public static readonly string ID = NullTile.ID + TownTile.ID + "1";
        #endregion

        #region Properties
        // Empty
        #endregion

        #region Fields
        // Empty
        #endregion

        #region Constructors
        // Empty
        #endregion

        #region Methods

        protected override IScene SetScene() {
			return new EmptyScene();
		}
		
		public override string GetGoDescription(Direction direction) {
			return String.Format("You try to go {0}, but you quickly lose yourself in the small alleys and sidestreets, forcing you to return to where you had come from.", direction.ToString());
		}
		
        public override string GetDescription() {
			return "[I'M AN IMPASSABLE TOWN. WHOT ARE YOU DOING HERE?]";
		}
		
		public override string GetDescription(Direction direction) {
			return String.Format("To the {0} is an incomprehensible set of small alleys and sidestreets.", direction.ToString());
		}
		#endregion
    }
}
