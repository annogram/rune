﻿using System;
using System.Collections.Generic;
using System.Linq;

using Rune.Util;
using Rune.Maps.Scenes;

namespace Rune.Maps.Tiles.Town
{
    public class NullTownBuildingsTile : NullTile
    {
        #region Const Defines
        new public static readonly string ID = NullTile.ID + TownTile.ID + "1";
        #endregion

        #region Properties
        // Empty
        #endregion

        #region Fields
        // Empty
        #endregion

        #region Constructors
        // Empty
        #endregion

        #region Methods

        protected override IScene SetScene() {
			return new EmptyScene();
		}
		
		public override string GetGoDescription(Direction direction) {
			return String.Format("You try to go {0}, but find a large building blocking your way.", direction.ToString());
		}
		
        public override string GetDescription() {
			return "[I'M AN IMPASSABLE TOWN. WHOT ARE YOU DOING HERE?]";
		}
		
		public override string GetDescription(Direction direction) {
			return String.Format("The {0} has nothing but buildings of non-descript nature, their doors closed.", direction.ToString());
		}
		#endregion
    }
}
