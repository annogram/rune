﻿using System;
using System.Collections.Generic;
using System.Linq;

using Rune.Util;
using Rune.Maps.Scenes;

namespace Rune.Maps.Tiles.Town
{
	
    public class TownSmithyStreetTile : TownTile
    {
        #region Const Defines
        new public static readonly string ID = TownTile.ID + "2";
        private readonly IList<string> SCENE_LIST = new List<string> { EmptyScene.ID };
        #endregion
		
        #region Properties
		// Empty
        #endregion

        #region Constructors
        // Empty
        #endregion

        #region Methods
		
		protected override IScene SetScene() {
			// Scene list should be ordered appropriately so that the correct scenes appear more often.
			int roll = RuneChance.RollDiceNormal(SCENE_LIST.Count() - 1);
			return SceneFactory.GetScene(SCENE_LIST.ElementAt(roll));
		}
		
		public override string GetDescription() {
			// Street is of medium quality as befitting a trade area
			return "You are standing on a well-maintained road.  The sign of an anvil hangs above one of the nearby doors, with an open-air forge evident.";
		}
		
		public override string GetGoDescription(Direction direction) {
			if(direction == Direction.NORTH || direction == Direction.SOUTH) {
				return String.Format("Unfortunately it appears you cannot reach the part of the town to your {0} due to the rather immovable buildings in your way.", direction.ToString());
			}
			// Account for run/walk/other kinds of movement?
			// Smithy might be a tile (would mess up map, need to size tiles to each building) or special char (basically a smith which can be referred to by smithy)
			return "As you wander into the next street you see that this is where the towns blacksmith lives - as evidenced by his smithy, the sign of the anvil hanging from ropes above its door.";
		}
		
		public override string GetDescription(Direction direction) {
			if(direction == Direction.NORTH || direction == Direction.SOUTH) {
				return String.Format("To the {0} is only dour-faced buildings.", direction.ToString());
			}
			return String.Format("Some sort of sign hangs above a door to your {0}... perhaps a trader of some kind?", direction.ToString());
		}
		
		public override bool CanEnterBy(Direction direction) {
			// Cannot enter this tile from north or south
			if(direction == Direction.NORTH || direction == Direction.SOUTH) {
				return false;
			}
			
			return true;
		}
        #endregion
    }
}
