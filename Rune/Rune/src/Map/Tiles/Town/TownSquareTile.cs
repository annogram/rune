﻿using System;
using System.Collections.Generic;
using System.Linq;

using Rune.Util;
using Rune.Maps.Scenes;
using Rune.Maps.Scenes.Town;

namespace Rune.Maps.Tiles.Town
{
	
    public class TownSquareTile : TownTile
    {
        #region Const Defines
        new public static readonly string ID = TownTile.ID + "1";
        private readonly IList<string> SCENE_LIST = new List<string> { EmptyScene.ID };
        #endregion
		
        #region Properties
		// Empty
        #endregion

        #region Constructors
        // Empty
        #endregion

        #region Methods
		
		protected override IScene SetScene() {
			if (isFirstVisit) {
                isFirstVisit = false;
                return SceneFactory.GetScene(Town_Square_TakeThisScene.ID);
			}
			// Scenes could be filtered based on time as well
			// Scene list should be ordered appropriately so that the correct scenes appear more often.
			int roll = RuneChance.RollDiceNormal(SCENE_LIST.Count() - 1);
			return SceneFactory.GetScene(SCENE_LIST.ElementAt(roll));
		}
		
		public override string GetDescription() {
			// Townspeople and merchants omitted to be included in scenes
			return "You are in a busy town square.  You can see a large well in the square's center.";
		}
		
		public override string GetGoDescription(Direction direction) {
			return String.Format("You enter the main town square from the {0}.", direction.ToString());
		}
		
		public override string GetDescription(Direction direction) {
			return String.Format("You can see that {0} of you is a large town square, like to contain merchants.", direction.ToString());
		}
        #endregion
    }
}
