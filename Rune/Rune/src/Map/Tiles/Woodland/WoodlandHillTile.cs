﻿using System;
using System.Collections.Generic;
using System.Linq;

using Rune.Util;
using Rune.Maps.Scenes;

namespace Rune.Maps.Tiles.Woodland
{
	
    public class WoodlandHillTile : WoodlandTile
    {
        #region Const Defines
        new public static readonly string ID = WoodlandTile.ID + "1";
        private readonly IList<string> SCENE_LIST = new List<string> { EmptyScene.ID };
        #endregion
		
        #region Properties
		// Empty
        #endregion

        #region Constructors
        // Empty
        #endregion

        #region Methods
		
		protected override IScene SetScene() {
			// Scene list should be ordered appropriately so that the correct scenes appear more often.
			int roll = RuneChance.RollDiceNormal(SCENE_LIST.Count() - 1);
			return SceneFactory.GetScene(SCENE_LIST.ElementAt(roll));
		}
		
		public override string GetDescription() {
			return "You are standing on top of a hill, dotted with large trees; the light is dappled by the sparse leaves and branches.";
		}
		
		public override string GetGoDescription(Direction direction) {
			return String.Format("You ascend a hill, with tall trees scattered about, albeit sparsely.", direction.ToString());
		}
		
		public override string GetDescription(Direction direction) {
			return String.Format("There is a wooded hill to the {0}.", direction.ToString());
		}
        #endregion
    }
}
