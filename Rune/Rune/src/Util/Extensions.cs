﻿using System;

namespace Rune.Util
{
    /// <summary>
    /// This Class will be used to make extension methods.
    /// </summary>
    public static class Extensions
    {
        /// <summary>
        /// Mathematically accurate mod function which correctly handles negative numbers.
        /// </summary>
        /// <param name="x">The integer which we will be doing the modulus operation on</param>
        /// <param name="m">The divisor which we will be applying to the integer</param>
        /// <returns>The absolute remainder of the division operation</returns>
        public static int Mod(this int x, int m) {
            int r = x % m;
            return r < 0 ? r + m : r;
        }

        /// <summary>
        /// Clamps the number between the input numbers.
        /// </summary>
        /// <param name="x"> Number to clamp </param>
        /// <param name="low"> Lowest number to return </param>
        /// <param name="high"> Highest number to return </param>
        /// <returns> Returns this clamped between low and high </returns>
        public static double Clamp(this double x, int low, int high) {
            return x > high ? high : x < low ? low : x;
        }

        /// <summary>
        /// Appends the input number to the extended number.
        /// </summary>
        /// <param name="baseNum"> Number to append to </param>
        /// <param name="concatNum"> Number to append </param>
        /// <returns> Returns the concatenated number </returns>
        public static double Concat(this double baseNum, double concatNum) {
            return Double.Parse(String.Format("{0}{1}", baseNum, concatNum));
        }
    }
}

