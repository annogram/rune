﻿using System;
using System.Collections.Generic;
using System.Linq;

using Rune.Items;

namespace Rune.Util
{
    /// <summary>
    /// Defines Properties and methods for an object with names (items, chars, actions).
    /// </summary>
    public interface INamed
    {
        #region Properties
        // Empty
        #endregion

        #region Methods
        /// <summary>
        /// Returns the simple (to print to screen) name of the object
        /// </summary>
        /// <returns> The name of the object as a string </returns>
        string ToSimpleName();

        /// <summary>
        /// Returns a simple name in context.
        /// </summary>
        /// <param name="context">The context the word will be used in.</param>
        /// <returns>The name of the object as a string, with context.</returns>
        string ToSimpleName(GrammarPoints context);
		
        /// <summary>
        /// Generates a description of the object.  May use a switch case with various params
		/// (such as item material, or just a random roll) to generate different descriptions
		/// to prevent "static" descriptions - ie. the player gets bored reading the same thing
		/// over and over.
        /// </summary>
        /// <returns> The description of the object as a string. </returns>
		string GetDescription();

        /// <summary>
        /// Checks the dictionaries for the given name
        /// </summary>
        /// <param name="str"> The name to look for </param>
        /// <returns> true if is the name of the item, otherwise false </returns>
        bool HasName(string str);

        /// <summary> 
        /// Checks the dictionary of common names against str for a match
        /// </summary>
        /// <param name="str"> A string to check against the dictionary </param>
        /// <returns> TRUE if match found, else false </returns>
        bool HasCommonName(string str);

        /// <summary> 
        /// Checks the dictionary of unique names against str for a match
        /// </summary>
        /// <param name="str"> A string to check against the dictionary </param>
        /// <returns> TRUE if match found, else false </returns>
        bool HasUniqueName(string str);
		#endregion
    }
}
