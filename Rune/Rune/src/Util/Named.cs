﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Rune.Util
{
    public abstract class Named : INamed
    {
		#region Const Defines
		protected static readonly string ID = "0";
		#endregion
		
		#region Properties
		// Dictionaries as to what names can be accepted in user input
        /// <summary> Common names that may apply to several items </summary>
        protected Dictionary<int, string> Common { get; set; } = new Dictionary<int, string>();
        /// <summary> Unique names that apply to this item only </summary>
        protected Dictionary<int, string> Unique { get; set; } = new Dictionary<int, string>();
		#endregion
		
		#region Constructors
		protected Named() {
			InitializeCommon();
			InitializeUnique();
		}
		#endregion
		
		#region Methods
		/// <summary>
        /// 
        /// </summary>
        protected virtual void InitializeCommon() {}

        /// <summary>
        /// 
        /// </summary>
        protected abstract void InitializeUnique();
		
		/// <summary>
        /// Returns the name, as seen in game, of the item
        /// </summary>
        /// <returns>The name of the item as a string</returns>
        public abstract string ToSimpleName();
		
		public abstract string GetDescription();

        /// <summary>
        /// 
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public bool HasName(string str) {
            if (HasUniqueName(str)) {
                return true;
            }
            return HasCommonName(str);
        }

                /// <summary>
        /// Returns a contextual name of the object
        /// </summary>
        /// <param name="context">The context in which the word is going to be used</param>
        /// <returns>The name modified to fit in this context</returns>
        public virtual string ToSimpleName(GrammarPoints context) {
            switch (context) {
                case GrammarPoints.ARTICLES:
                    if (checkStartingVowels(ToSimpleName())) {
                        return "an " + ToSimpleName();
                    } else {
                        return "a " + ToSimpleName();
                    }
                case GrammarPoints.PLURAL:
                    if (ToSimpleName().EndsWith("s")) {
                        return ToSimpleName();
                    }
                    return ToSimpleName() + "s";
                default:
                    return ToSimpleName();
            }
        }

        /// <summary> 
        /// Checks the dictionary of common names against str for a match
        /// </summary>
        /// <param name="str"> A string to check against the dictionary </param>
        /// <returns> TRUE if match found, else false </returns>
        public bool HasCommonName(string str) {
            str.Replace(@"\s+", "");
            str = str.ToLower();
            return this.Common.ContainsKey(str.GetHashCode());
        }

        /// <summary> 
        /// Checks the dictionary of unique names against str for a match
        /// </summary>
        /// <param name="str"> A string to check against the dictionary </param>
        /// <returns> TRUE if match found, else false </returns>
        public bool HasUniqueName(string str) {
            str.Replace(@"\s+", "");
            str = str.ToLower();
            return this.Unique.ContainsKey(str.GetHashCode());
        }

        private bool checkStartingVowels(string noun) {
            char[] vowles = new char[] { 'a', 'e', 'i', 'o', 'u' };
            bool returnValue = false;
            foreach (char c in vowles) {
                if (noun.StartsWith(c + "")) {
                    returnValue = true;
                    break;
                }
            }
            return returnValue;
        }
        #endregion


    }
}
