﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rune.Util {
    #region Enums
    public enum GrammarPoints
    {
        SINGULAR,
        PLURAL,
        ARTICLES
    }
    #endregion
}
