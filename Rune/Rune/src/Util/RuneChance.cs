﻿using System;
using System.Collections.Generic;

namespace Rune.Util
{
    public static class RuneChance
    {
        #region Public Constants
        /// <summary> Represents the max number that may be rolled </summary>
        /// <remarks> Roll chance should be floor(MAX * multi), not hard numbers </remarks>
        public const int DEFAULT_DICE_ROLL = 20;

        /// <summary> A baseline number of cases (soft limit) </summary>
        public const int DEFAULT_NUM_CASES = 5;
        #endregion

        #region Fields
        /// <summary> Random number generator for rolling die. </summary>
        private static Random random = new Random();
        #endregion

        #region Methods
        /// <summary>
        /// Rolls a die from 0 to DEFAULT_DICE_ROLL with a uniform distribution.
        /// </summary>
        /// <returns> Returns a number between 0 and DEFAULT_DICE_ROLL. </returns>
        public static int RollDiceUniform() {
            return RollDiceUniform(DEFAULT_DICE_ROLL);
        }

        /// <summary>
        /// Rolls a die from 0 to diceSize with a uniform distribution.
        /// </summary>
        /// <returns> Returns a number between 0 and diceSize. </returns>
        public static int RollDiceUniform(int diceSize) {
            return random.Next(0, diceSize);
        }

        /// <summary>
        /// Rolls a dice from 0 to DEFAULT_DICE_ROLL following a normal distribution with medium sigma.
        /// </summary>
        /// <returns> A randomly generated dice roll following a normal distribution. </returns>
        public static int RollDiceNormal() {
            return RollDiceNormal(DEFAULT_DICE_ROLL, 0, DEFAULT_DICE_ROLL/10);
        }

        /// <summary>
        /// Rolls a dice from 0 to dicesize following a normal distribution
        /// </summary>
        /// <returns> A randomly generated dice roll following a normal distribution. </returns>
        public static int RollDiceNormal(int diceSize) {
            return RollDiceNormal(diceSize, 0, diceSize/10);
        }

        /// <summary>
        /// Rolls a dice from 0 to DEFAULT_DICE_ROLL following a normal distribution with specified sigma.  Median is DEFAULT_DICE_ROLL / 2 + diff.
        /// </summary>
        /// <returns> A randomly generated dice roll following a normal distribution. </returns>
        public static int RollDiceNormal(int medianDiff, double sigma) {
            return RollDiceNormal(DEFAULT_DICE_ROLL, medianDiff, sigma);
        }

        /// <summary>
        /// Rolls a dice from 0 to diceSize following a normal distribution with specified sigma.  Median is diceSize / 2 + diff.
        /// </summary>
        /// <remarks>
        /// Appropriate sigma:
        /// dice / 20 for steep median, few outlying rolls.
        /// dice / 10 for frequent central rolls, some outlying rolls.
        /// dice / 5 for good number of all rolls.  Central rolls still more common.
        /// dice / 4 maximum recommended, dice / 20 minimum.
        /// </remarks>
        /// <returns> A randomly generated dice roll following a normal distribution. </returns>
        public static int RollDiceNormal(int diceSize, double medianDiff, double sigma) {
            double median = (diceSize / 2) + medianDiff;

            double cauchyRand = median + sigma * Math.Tan(Math.PI * (random.NextDouble() - 0.5));
            int diceRoll = (int)Math.Round(cauchyRand.Clamp(-1, diceSize + 1));

            return (diceRoll <= diceSize && diceRoll >= 0) ? diceRoll : RollDiceNormal(diceSize, median, sigma);
        }

        /// <summary>
        /// Rolls a dice from 0 to DEFAULT_DICE_ROLL following a exponential distibution.
        /// </summary>
        /// <returns> A randomly generated dice roll following a exponential distribution. </returns>
        public static int RollDiceExponential() {
            return RollDiceExponential(DEFAULT_DICE_ROLL, DEFAULT_DICE_ROLL/400);
        }

        /// <summary>
        /// Rolls a dice from 0 to diceSize following a exponential distibution.
        /// </summary>
        /// <returns> A randomly generated dice roll following a exponential distribution. </returns>
        public static int RollDiceExponential(int diceSize) {
            return RollDiceExponential(diceSize, DEFAULT_DICE_ROLL/400);
        }

        /// <summary>
        /// Rolls a dice from 0 to diceSize following a exponential distibution.
        /// </summary>
        /// <remarks>
        /// Appropriate rates:
        /// dice / 200 for steep median, few outlying rolls
        /// dice / 400 for frequent lower rolls, some outlying rolls
        /// dice / 667 for good number of all rolls.  Lower rolls still more common.
        /// Do not recommend rates outside of (higher / lower) than min/max suggestions.
        /// </remarks>
        /// <returns> A randomly generated dice roll following a exponential distribution. </returns>
        public static int RollDiceExponential(int diceSize, double rate) {
            double exponentialRand = Math.Log(1 - random.NextDouble()) / (-rate);
            int diceRoll = (int)Math.Round(exponentialRand.Clamp(-1, diceSize + 1));

            return (diceRoll <= diceSize && diceRoll >= 0) ? diceRoll : RollDiceExponential(diceSize, rate);
        }

        /// <summary>
        /// Generates a set of cases according to DEFAULT_DICE_ROLL and DEFAULT_NUM_CASES.
        /// </summary>
        /// <returns> A set of cases to be used in switch/cases using dicerolls. </returns>
        public static IEnumerable<int> GetChanceCases() {
            return GetChanceCases(DEFAULT_DICE_ROLL, DEFAULT_NUM_CASES);
        }

        /// <summary>
        /// Generates a set of cases according to DEFAULT_DICE_ROLL for the specified number of cases.
        /// </summary>
        /// <returns> A set of cases to be used in switch/cases using dicerolls. </returns>
        public static IEnumerable<int> GetChanceCases(int numCases) {
            return GetChanceCases(DEFAULT_DICE_ROLL, numCases);
        }

        /// <summary>
        /// Generates a set of cases according to specified diceSize and number of cases.
        /// </summary>
        /// <remarks>
        /// Suggested switch case syntax is: is roll less than case[0], is roll less than case[1], etc.
        /// </remarks>
        /// <returns> A set of cases to be used in switch/cases using dicerolls. </returns>
        public static IEnumerable<int> GetChanceCases(int diceSize, int numCases) {
            if (numCases <= diceSize && numCases != 0) {
                int caseDiff = (int)Math.Floor((double)(diceSize / numCases));
                int caseChance = 0;
                for (int i = 0; i <= numCases; i++) {
                    caseChance += caseDiff;
                    yield return caseChance;
                }
            } else if (numCases > diceSize) {
                int caseDiff = 0;
                for (int i = 0; i <= numCases; i++) {
                    yield return caseDiff++;
                }
            } else {
                // Invalid numCases
                yield return diceSize;
            }
        }
        #endregion
    }
}
