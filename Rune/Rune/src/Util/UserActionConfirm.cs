﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rune.Util 
{
	// REVIEW This could be restricted by creating different types of actions that return different types of UAC structs
    // Eg. UACMap (would need description, title), UACAction (would need action, description, rolls), UACPassiveAction (needs description, rolls)
	public struct UserActionConfirm 
	{
        #region Properties
		public bool WasSuccessful;
		
        public bool HasCallback;
		public Action<string> Callback;
		
		// REVIEW The following allows the controller to account for changes to View API 
		//        without affecting the actions.  Checking bools could be replaced with null
		//        checks but nulls scare me.
		public bool HasTitle;
		public string TitleText;
		
		public bool HasDescription;
        public string DescriptiveText;
		
		public bool HasAction;
		public string ActionText;
		//public Rune.View.DisplayTextColor[] EmphasisText;
		
		public bool HasRolls;
		public string[] Rolls;
        #endregion
		
		#region Constructors
		// Can use this ctor or default.  Default bool = false, string+ = null
		public UserActionConfirm(bool wasSuccessful,
								 bool hasCallback, 
								 Action<String> callback, 
								 bool hasTitle, 
								 string titleText,
								 bool hasDescription,
								 string descriptiveText,
								 bool hasAction,
								 string actionText,
								 //Rune.View.DisplayTextColor[] emphasisText,
								 bool hasRolls,
								 string[] rolls) 
		{
			WasSuccessful = wasSuccessful;
			HasCallback = hasCallback;
			Callback = callback;
			
			HasTitle = hasTitle;
			TitleText = titleText;
			
			HasDescription = hasDescription;
			DescriptiveText = descriptiveText;
			
			HasAction = hasAction;
			ActionText = actionText;
			//EmphasisText = emphasisText;
			
			HasRolls = hasRolls;
			Rolls = rolls;
		}
		#endregion
    }
}
