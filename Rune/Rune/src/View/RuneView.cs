﻿using Rune.View;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Rune.View 
{
    public partial class RuneView : Form
    {

        #region Constants
        private const string LINE_START = "> ";
        private const string INITIAL_INPUT_TEXT = LINE_START + "Type Commands here";
        private readonly Font DEFAULT_FONT = new Font("Consolas", 11F);
        #endregion

        #region Fields
        //boolean parameters for conveying machine state
        private bool userConfirm = false;
        AboutRune aboutRune = new AboutRune();
        #endregion

        #region Constructors
        public RuneView() {
            InitializeComponent();
            //Initilize text boxes
            textBoxDisplay.Font = DEFAULT_FONT;
            textBoxInput.Font = DEFAULT_FONT;
            statsFeedBox.Font = DEFAULT_FONT;
            WriteToDisplayTitle("Welcome To Rune");
            WriteToDisplay(Application.ProductVersion);

            #region DummyText
            WriteToDisplayScene("You awake in your humble bedroom. It is the end of spring and the " 
                + "first chills of winter breeze through your open window. You get up and look outside "
                + "at your village and fill your lungs with the morning air, the scent of labor and mud " 
                + "clings to your nostrils even this early in the morning.");
            writeActionResults(true, "You spot a friend", "You");
            writeActionResults(false, "The guards close in on you, an pathetic orc and a troll attack.", "guards", "pathetic orc", "troll");
            #endregion

            textBoxInput.Text = INITIAL_INPUT_TEXT;
            //TODO Initilize controllers
        }
        #endregion

        #region Methods
        /// <summary>
        /// This function must be used to write any text to the display.
        /// Any text must be parsed to the games standard.
        /// </summary>
        public void WriteToDisplay(string text) {
            string parsedText = LINE_START;
            textBoxDisplay.AppendText(parsedText + text + Environment.NewLine);
            //Move the cursor to the end of the text
            textBoxDisplay.Select(textBoxDisplay.TextLength, textBoxDisplay.TextLength);
            // Scroll to bottom
            textBoxDisplay.ScrollToCaret();
        }

        /// <summary>
        /// This function must be used to write any text that will be used in describing scenes.
        /// </summary>
        /// <param name="text">Text that will be written to the display</param>
        public void WriteToDisplayScene(string text) {
            // add pointer
            string parsedText = LINE_START;
            textBoxDisplay.AppendText(parsedText);
            // get the start of this append
            int insertionStart = textBoxDisplay.TextLength;
            textBoxDisplay.AppendText(text + Environment.NewLine);
            textBoxDisplay.Select(insertionStart, insertionStart + text.Length);
            textBoxDisplay.SelectionFont = new Font(textBoxDisplay.Font, FontStyle.Italic);
            // Move the cursor to the end of the text
            textBoxDisplay.Select(insertionStart + text.Length + 1, insertionStart + text.Length + 1);
            // Scroll to bottom
            textBoxDisplay.ScrollToCaret();
        }

        /// <summary>
        /// This method formats strings into titile format and adds them
        /// to the display. text formatted thusly is bordered by "+" characters
        /// </summary>
        /// <param name="title">Title text</param>
        public void WriteToDisplayTitle(string title) {
            StringBuilder sb = new StringBuilder();
            string titlePrefix = "";
            for (int i = 0; i < title.Length * 4; i++) {
                if (i < title.Length/2) {
                    titlePrefix = string.Concat(titlePrefix, " ");
                }
                if (i == (title.Length*2)) {
                    sb.Append(Environment.NewLine);
                    sb.Append(Environment.NewLine);
                    sb.AppendLine(titlePrefix + title.ToUpper());
                    sb.Append(Environment.NewLine);
                }
                sb.Append('+');
            }
            sb.Append(Environment.NewLine);
            textBoxDisplay.Text = sb.ToString();
            // Scroll to bottom
            textBoxDisplay.ScrollToCaret();
        }

        /// <summary>
        /// This method will format user input to emphasize key aspects of the string.
        /// </summary>
        /// <param name="friendly">Determines the color of the emphasis, red if false, green if true</param>
        /// <param name="context">The string to parse</param>
        /// <param name="emphasize">The substrings within the context to emphasize</param>
        private void writeActionResults(bool friendly, string context, params string[] emphasize) {
            Font embolden = new Font(DEFAULT_FONT, FontStyle.Bold);
            Color friendlyColor = Color.Green;
            Color enemyColor = Color.Red;
            // Add pointer
            textBoxDisplay.AppendText(LINE_START);
            // get the start of this appendition
            int insertionStart = textBoxDisplay.TextLength;
            string[] split = context.Split(' ');
            // **You** attack for x Damage
            // **You** and **Friendly** Attack for x Damage
            textBoxDisplay.AppendText(context);
            foreach (string s in emphasize) {
                if (!context.Contains(s)) continue;
                int loc = context.IndexOf(s);
                textBoxDisplay.Select(insertionStart + loc, s.Length);
                textBoxDisplay.SelectionFont = embolden;
                textBoxDisplay.SelectionColor = (friendly) ? friendlyColor : enemyColor;
            }
            textBoxDisplay.AppendText(Environment.NewLine);
        }
        #endregion

        #region Event Listeners
        /// <summary>
        /// This event listener will ensure that text displayed meets
        /// quality requirements
        /// </summary>
        private void textBoxInput_TextChanged(object sender, EventArgs e) {
            var actionable = sender as TextBox;
            int textLength = actionable.TextLength;

            if (textLength <= 2) {
                actionable.Text = LINE_START;
                actionable.Select(2, 2);
            } else if (textLength > 2 && actionable.Text.Substring(0, 2) != LINE_START) {
                char[] parse = actionable.Text.ToCharArray();
                string outString = "";
                if (parse[1] != ' ') {
                    outString = LINE_START + actionable.Text.Substring(1, textLength - 1);
                }
                actionable.Text = outString;
                actionable.Select(2, 0);
            }

        }

        /// <summary>
        /// Any event that should happen during the default timer tick should
        /// be written here.
        /// <remarks>Defualt tick rate is 1000ms</remarks>
        /// </summary>
        private void timerGame_Tick(object sender, EventArgs e) {
            //TODO timer events
        }

        /// <summary>
        /// This listener handles all shortuct keys.
        /// </summary>
        /// <returns>returns true if input parsed sucessfully otherwise false.</returns>
        protected override bool ProcessCmdKey(ref Message msg, Keys keyData) {
            if (keyData == (Keys.Control | Keys.A)) {
                // Ctrl + A
                textBoxInput.SelectAll();
                return true;
            } else if (userConfirm && keyData == (Keys.Right)) {
                //TODO implement functionality to select an option "y/n"
            } else if (userConfirm && keyData == (Keys.Left)) {
                //TODO implement functionality to select an option "y/n"
            } else if (keyData == (Keys.Control | Keys.Back)) {
                // Ctrl + Bcksp
                textBoxInput.Clear();
            }
            return base.ProcessCmdKey(ref msg, keyData);
        }

        /// <summary>
        /// Listener on textBoxInput that deals with mouse clicks.
        /// </summary>
        private void textBoxInput_MouseClick(object sender, MouseEventArgs e) {
            var actionable = sender as TextBox;
            // If the text is the initial text then we remove it when it is clicked.
            if (actionable.Text == INITIAL_INPUT_TEXT) {
                actionable.Text = "";
            }
        }

        /// <summary>
        /// What to do when enter is pressed. 
        /// Main delegator for passing info to controllers.
        /// </summary>
        private void textBoxInput_KeyDown(object sender, KeyEventArgs e) {
            var actionable = sender as TextBox;
            string textAction;
            if (e.KeyCode == Keys.Enter) {
                textAction = actionable.Text.Substring(2);
                //TODO Designate which controller to input
                if (textAction.ToLower().Equals("quit rune")) {
                    Application.Exit();
                } else {
                    WriteToDisplay("I don't know how to: " + textAction);
                }
                actionable.Clear();
            }
        }

        private void textBoxDisplay_TextChanged(object sender, EventArgs e) {
        }

        private void aboutToolStripMenuItem_Click(object sender, EventArgs e) {
            aboutRune.Show();
        }
        #endregion



    }
}
