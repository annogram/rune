﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Rune.Maps.Tiles;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Rune.Maps.Tiles.Forest;

namespace Rune.Maps.Tiles.Tests
{
    [TestClass()]
    public class TileFactoryTests
    {
        [TestMethod()]
        public void UnitTest_GetTile() {
            Tile actual = TileFactory.GetTile(ForestBrookTile.ID);

            Assert.IsInstanceOfType(actual, typeof(ForestBrookTile));
        }
    }
}